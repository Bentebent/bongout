#include <OIS/OISInputManager.h>
#include <OIS/OISException.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>
#include <OIS/OISJoyStick.h>
#include <OIS/OISEvents.h>

struct InputDevices
{
	OIS::InputManager*	g_InputManager;
	OIS::Keyboard*		g_kb;
	OIS::Mouse*			g_m;
	OIS::JoyStick*		g_joys[4];
};
