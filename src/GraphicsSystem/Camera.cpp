#include "Camera.hpp"
#include "glm/ext.hpp"

Camera::Camera()
{
	position = glm::vec3(0.0f);
	
	forward = glm::vec3(0.0f,0.0f,1.0f);
	up		= glm::vec3(0.0f,1.0f,0.0f);

	
	m_viewMatrix = glm::mat4(1.0f);
	m_projMatrix = glm::mat4(1.0f);
	
	// init fov to 90 degrees
	m_fov = 45.0f;
}

Camera::Camera( const Camera& cam )
{
}

Camera::~Camera()
{
}

void Camera::SetPosition( float x, float y, float z )
{
	position.x = x;
	position.y = y;
	position.z = z;
}

void Camera::SetRotation( float x, float y, float z )
{
	float pitch = x;
	float yaw   = y;
	float roll  = z;

	glm::vec3 right;

	this->forward = glm::vec3(
            -cos(pitch) * sin(yaw),
            sin(pitch),
            cos(pitch) * cos(yaw)
    );
	
	right = glm::normalize( glm::cross( forward, glm::vec3( 0.0f, 1.0f, 0.0f ) ) );
	right = glm::rotate(right, glm::degrees(roll), forward);
	

	this->up = glm::cross( right, forward);
}

void Camera::SetFov( float fov )
{
	m_fov = fov;
}

glm::vec3 Camera::GetPosition()
{
	return position;
}

glm::vec3 Camera::GetRotation()
{
	return glm::vec3( 
		glm::dot( forward, glm::vec3(1.0f, 0.0f, 0.0f) ),
		glm::dot( forward, glm::vec3(0.0f, 1.0f, 0.0f) ),
		glm::dot( forward, glm::vec3(0.0f, 0.0f, 1.0f) )
		);
}

void Camera::UpdateProjection( const float &width, const float &height, const float &near, const float &far )
{
	m_projMatrix = glm::perspective<float>( m_fov, width/height, near, far );
}

void Camera::UpdateView( glm::vec3 &pos, glm::vec3 &lookAt )
{
	glm::vec3 position;
	m_viewMatrix = glm::lookAt( position, lookAt, glm::vec3( 0.000001f, 1.0f, 0.000001f ) );
}

void Camera::UpdateView( glm::vec3 &lookAt )
{
	m_viewMatrix = glm::lookAt( position, lookAt, glm::vec3( 0.000001f, 1.0f, 0.000001f ) );
}

void Camera::UpdateView()
{
	// Generate view matrix
	m_viewMatrix = glm::lookAt( position, position + forward, up );

}

void Camera::GetViewMatrix( glm::mat4& view )
{
	view = m_viewMatrix;
}

void Camera::GetProjectionMatrix( glm::mat4& proj )
{
	proj = m_projMatrix;
}