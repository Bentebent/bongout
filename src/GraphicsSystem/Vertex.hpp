#ifndef VERTEX_H
#define VERTEX_H

struct Vertex
{

	Vertex()
	{
	}

	Vertex(float px, float py, float pz, float nx, float ny, float nz, float UVx, float UVy)
	{
		mPosition[0] = px;
		mPosition[1] = py;
		mPosition[2] = pz;

		mNormal[0] = nx;
		mNormal[1] = ny;
		mNormal[2] = nz;

		mUV[0] = UVx;
		mUV[1] = UVy;
	}

	bool operator== (Vertex &v)
	{
		return 
			mPosition[0] == v.mPosition[0] &&
			mPosition[1] == v.mPosition[1] &&
			mPosition[2] == v.mPosition[2] &&
			mNormal[0] == v.mNormal[0] &&
			mNormal[1] == v.mNormal[1] &&
			mNormal[2] == v.mNormal[2] &&
			mUV[0] == v.mUV[0] &&
			mUV[1] == v.mUV[1];
	}

	float mPosition[3];
	float mNormal[3];
	float mUV[2];
};

#endif