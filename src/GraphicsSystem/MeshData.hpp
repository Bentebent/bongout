#ifndef MESHDATA_H
#define MESHDATA_H

#include "Vertex.hpp"
#include <vector>

struct MeshData
{
	std::vector<Vertex> mVertices;
	std::vector<unsigned int> mIndices;
};

#endif