#ifndef LOADINTERFACE_HPP
#define LOADINTERFACE_HPP
#include "MeshType.hpp"
#include <string>

class LoadInterface
{
public:

	void virtual LoadObj( std::string path, unsigned int mode, MeshType type ) = 0;
	
	void LoadContent( unsigned int mode );

private:
	


};
#endif
