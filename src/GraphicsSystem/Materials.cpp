#include "Materials.hpp"


PaletteMaterials::PaletteMaterials()
{
	paddleMaterial = MeshMaterial( 
		1, 0, 0, 
		0, 1, 0, 1, 
		1, 1, 1, 0 );

	//ballMaterial = MeshMaterial( 
	//	0, 1, 1, 
	//	0, 1, 1, 1, 
	//	1, 1, 1, 0 );

	ballMaterial = MeshMaterial( 
		0, 1, 1, 
		0, 1, 1, 1, 
		1, 0, 0, 0 );

	brickMaterialA = MeshMaterial( 
		23 / 255.f, 41 / 255.f, 176 / 255.f,
		23 / 255.f, 41 / 255.f, 176 / 255.f, 1,
		1, 1, 1, 0 );

	brickMaterialB = MeshMaterial( 
		75 / 255.f, 92 / 255.f, 215 / 255.f, 
		23 / 255.f, 41 / 255.f, 176 / 255.f, 1,
		1, 1, 1, 0 );

	fieldWallMaterialA = MeshMaterial( 
		0, 1, 1, 
		0, 1, 1, 200, 
		1, 1, 1, 0 );

	backgroundMaterialA = MeshMaterial(
		0.1f, 0.1f, 0.1f, 
		0.1f, 0.1f, 0.1f, 0.1f, 
		0.1f, 0.1f, 0.1f, 0.0f );
}



LightPalette::LightPalette()
{
	ballLightMaterial = LightMaterial( 
		30, 0.4, 
		255 / 255.f, 232 / 255.f, 64 / 255.f, 
		0, 0, 2 );

	backroundLightMaterial = LightMaterial( 
		60, 0.1f, 
		255 / 255.f, 232 / 255.f, 64 / 255.f, 
		0, 0, 2 );
}