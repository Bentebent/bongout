#ifndef GRAPHICSMATERIAL_H
#define GRAPHICSMATERIAL_H

#include "MeshType.hpp"


enum LightType
{
	NoLight,
	PointLight,
	DirectionalLight,
	COUNT_LIGHTTYPES,
};

struct MeshMaterial
{
	MeshMaterial();
	MeshMaterial(
		float diffuse_red, float diffuse_green, float diffuse_blue,
		float specular_red, float specular_green, float specular_blue, float specularIntensity,
		float glow_red, float glow_green, float glow_blue, float glowIntensity );

	float specularIntensity;
	float glowIntensity;
	float diffuse_red, diffuse_green, diffuse_blue;
	float specular_red, specular_green, specular_blue;
	float glow_red, glow_green, glow_blue;
};

struct LightMaterial
{
	LightMaterial();
	LightMaterial( float size, float intensity, float red, float green, float blue, float offsetX_, float offsetY_, float offsetZ_ );

	float offsetX, offsetY, offsetZ;
	float red, green, blue;
	float size, intensity;
};

#endif
