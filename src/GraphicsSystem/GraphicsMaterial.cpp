#include "GraphicsMaterial.hpp"

MeshMaterial::MeshMaterial()
{
	specularIntensity	= 1;
	glowIntensity		= 0;

	diffuse_red			= 1;
	diffuse_green		= 1;
	diffuse_blue		= 1;

	specular_red		= 1;
	specular_green		= 1;
	specular_blue		= 1;
		
	glow_red			= 1;
	glow_green			= 1;
	glow_blue			= 1;
}

MeshMaterial::MeshMaterial(
		float diffuse_red_, float diffuse_green_, float diffuse_blue_,
		float specular_red_, float specular_green_, float specular_blue_, float specularIntensity_,
		float glow_red_, float glow_green_, float glow_blue_, float glowIntensity_ )
{
	specularIntensity	= specularIntensity_;
	glowIntensity		= glowIntensity_;

	diffuse_red			= diffuse_red_;
	diffuse_green		= diffuse_green_;
	diffuse_blue		= diffuse_blue_;

	specular_red		= specular_red_;
	specular_green		= specular_green_;
	specular_blue		= specular_blue_;
		
	glow_red			= glow_red_;
	glow_green			= glow_green_;
	glow_blue			= glow_blue_;
}


LightMaterial::LightMaterial()
{
	offsetX = 0;
	offsetY = 0;
	offsetZ = 0;
	red = 1;
	green = 1;
	blue = 1;
	intensity = 1;
	size = 10;
}

LightMaterial::LightMaterial( float size_, float intensity_, float red_, float green_, float blue_, float offsetX_, float offsetY_, float offsetZ_ )
{
	offsetX = offsetX_;
	offsetY = offsetY_;
	offsetZ = offsetZ_;
	red = red_;
	green = green_;
	blue = blue_;
	size = size_;
	intensity = intensity_;
}
