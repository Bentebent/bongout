#ifndef SRC_GRAPHICSSYSTEM_MESHTYPE_H
#define SRC_GRAPHICSSYSTEM_MESHTYPE_H

enum MeshType
{
	NoMesh = 0,
	BongOutText,
	StartGameText,
	QuitText,
	GameOverText,
	Player1WinsText,
	Player2WinsText,
	RestartGameText,
	ExitGameText,
	MainMenuText,
	PrepareToFightText,

	Paddle,
	Ball,
	Brick,
	PlayerWall,
	PlayerFloor,
	FieldThinWall,
	FieldThinDelimiter,
	ComplexBackground,

	Num0, Num1, Num2, Num3, Num4, Num5, Num6, Num7, Num8, Num9,
	COUNT_MESHTYPES,
};

#endif
