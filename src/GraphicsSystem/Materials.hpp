#ifndef MATERIALS_HPP
#define MATERIALS_HPP

#include <GraphicsSystem/GraphicsMaterial.hpp>

static struct PaletteMaterials
{
	PaletteMaterials();
	
	MeshMaterial paddleMaterial;
	MeshMaterial ballMaterial;
	MeshMaterial brickMaterialA;
	MeshMaterial brickMaterialB;
	MeshMaterial fieldWallMaterialA;
	MeshMaterial backgroundMaterialA;

} meshPalette;



static struct LightPalette
{
	LightPalette();

	LightMaterial ballLightMaterial;
	LightMaterial backroundLightMaterial;
} lightPalette;


#endif