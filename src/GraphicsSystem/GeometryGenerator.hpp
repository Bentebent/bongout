#ifndef GEOMETRYGENERATOR_H
#define GEOMETRYGENERATOR_H

#define _USE_MATH_DEFINES

#include <string>
#include <vector>
#include <string>
#include <math.h>
#include "MeshData.hpp"

class GeometryGenerator
{
private:
	struct FaceDef
	{
		int vertIndex[3];
		int uvIndex[3];
		int normalIndex[3];
	};

public:
	static void CreateBox(float width, float height, float depth, unsigned int numSubdivisions, MeshData& meshData);
	static void CreateSphere(float radius, unsigned int sliceCount, unsigned int stackCount, MeshData& meshData);
	static void CreateGrid(float width, float depth, unsigned int m, unsigned int n, MeshData& meshData);
	static void CreateFromObjFile(std::string filePath, MeshData& meshData);

private:
	static void Subdivide(MeshData& meshData);
	static void NormalizeVector3(float* vec3);
	static void NormalizeVector3(float* inVec3, float* outVec3);
	static void LerpVec2(const float* vec1, const float* vec2, float t, float* outVec);
	static void LerpVec3(const float* vec1, const float* vec2, float t, float* outVec);
	static float StringToFloat( std::string s );
	static int StringToInt( std::string s );
};

#endif
