#ifndef GLGRAPHICS_HPP
#define GLGRAPHICS_HPP

#include <GraphicsSystem/OpenGL/GLGeobufferPainterSystem.hpp>
#include <GraphicsSystem/OpenGL/GLPointLightPainterSystem.hpp>
#include <GraphicsSystem/OpenGL/GLDirectionalLightPainterSystem.hpp>
#include <GraphicsSystem/OpenGL/GLAmbientPainter.hpp>
#include <GraphicsSystem/OpenGL/GLGlowPainter.hpp>
#include <GraphicsSystem/LoadInterface.hpp>
#include <GraphicsSystem/OpenGL/FBOTexture.hpp>


class ParticleSystem;


class GLgraphics : public LoadInterface
{
private:
	void InitDummyVAO();
	void InitDeferredSystem();
	void ResizeDeferredSystem();

public:

	void Init( int windowSizeX, int windowSizeY, ParticleSystem* particleSystem);
	void Update( World& world );

	void SetNewWindowLimits( int x, int y );
	glm::vec2 GetWindowSize() { return this->windowSize; }

	void SetAmbienceLevel( float level );
	void SetAmbienceColor( glm::vec3 color );
	glm::vec3 GetAmbienceColor() { return this->ambienceColor; }
	float GetAmbienceLevel() { return this->ambienceLevel; }

	static GLgraphics& Get();
	GLuint GetDummyVAO() { return this->dummyVAO; }

	void ActivateDeferredFBO();
	void SetDeferredSystemForGeobufferOutput();
	void DeActivateFBO();

	int GetDiffuseTextureHandle() { return this->diffuseTexture.GetTextureHandle(); }
	int GetPositionTextureHandle() { return this->positionTexture.GetTextureHandle(); }
	int GetNormalTextureHandle() { return this->normalTexture.GetTextureHandle(); }
	int GetSpecularTextureHandle() { return this->specularTexture.GetTextureHandle(); }
	int GetGlowTextureHandle() { return this->glowTexture.GetTextureHandle(); }

	void virtual LoadObj( std::string path, unsigned int mode, MeshType type ) override;

private:

	GLgraphics();
	~GLgraphics();

	bool newWindowSize;
	glm::vec2 windowSize;
	float ambienceLevel;
	glm::vec3 ambienceColor;

	ParticleSystem* particleSystem;
	GLGeobufferPainterSystem geoPainter;
	GLPointLightPainterSystem pointLightPainter;
	GLDirectionalLightPainterSystem directionalPainter;
	GLAmbientPainter ambientPainter;
	GLGlowPainter glowPainter;

	GLuint dummyVAO;

	FBOTexture deferredDepthTexture;
	FBOTexture diffuseTexture;
	FBOTexture positionTexture;
	FBOTexture normalTexture;
	FBOTexture specularTexture;
	FBOTexture glowTexture;
	GLuint deferredFBO;
};

#endif

