#ifndef GLGEOBUFFERPAINTERSYSTEM_HPP
#define GLGEOBUFFERPAINTERSYSTEM_HPP

#include <GraphicsSystem/OpenGL/GLShader.hpp>
#include <GraphicsSystem/OpenGL/GLMesh.hpp>
#include <ComponentFramework/System.hpp>
#include <GraphicsSystem/GraphicsMaterial.hpp>

class Camera;
class World;

class GLGeobufferPainterSystem : public System
{
public:
	GLGeobufferPainterSystem();
	~GLGeobufferPainterSystem();

	void Init();
	virtual void Update(World &world) override;

	void LoadObj( std::string path, GLenum mode, MeshType type );

private:

	GLShader shader;

	int modelMatrixPosition;
	int viewMatrixPosition;
	int projectionMatrixPosition;
	int colorPos;
	int specularPos;
	int glowPos;


	struct Setting {
		GLMesh mesh;
		MeshType meshType;
		GLenum mode;
	};

	std::vector< Setting > settings;


};
#endif