#include "GLGeobufferPainterSystem.hpp"

#include <GLFW/glew.h>
#include "GLgraphics.hpp"
#include "GLMesh.hpp"
#include <GraphicsSystem/GeometryGenerator.hpp>
#include <GraphicsSystem/Camera.hpp>
#include "glm/ext.hpp"

#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>

#include <iostream>

GLGeobufferPainterSystem::GLGeobufferPainterSystem()
{
}

GLGeobufferPainterSystem::~GLGeobufferPainterSystem()
{
}

void GLGeobufferPainterSystem::LoadObj( std::string path, GLenum mode, MeshType type )
{
	MeshData data;
	GeometryGenerator::CreateFromObjFile( path, data );

	this->settings.push_back( Setting() );
	this->settings.back().mesh.Load( data );
	this->settings.back().meshType = type; 
	this->settings.back().mode = mode; 
}


void GLGeobufferPainterSystem::Init()
{
	this->shader.LoadShader( "assets/OpenglShaders/StaticMeshShader.vertex", "assets/OpenglShaders/StaticMeshShader.fragment" );

	this->Asp.AddInclusiveAspect( WORLD_POSITION_COMPONENT );
	this->Asp.AddInclusiveAspect( GRAPHICS_COMPONENT );
	this->Asp.AddInclusiveAspect( SCALE_COMPONENT );

	this->modelMatrixPosition = this->shader.GetPosition( "modelMatrix" );
	this->viewMatrixPosition = this->shader.GetPosition( "viewMatrix" );
	this->projectionMatrixPosition = this->shader.GetPosition( "projMatrix" );

	this->colorPos = this->shader.GetPosition( "color" );
	this->specularPos = this->shader.GetPosition( "specular" );
	this->glowPos = this->shader.GetPosition( "glow" );
}

void GLGeobufferPainterSystem::Update( World& world )
{
	
	switch (world.mCurrentState)
	{
		case GameState::GAMEPLAY:
			Asp = world.mGameplayAspect;
			break;
		case GameState::MAIN_MENU:
			Asp = world.mMainMenuAspect;
			break;
		case GameState::END_GAME_MENU:
			Asp = world.mEndMenuAspect;
			break;
		case GameState::READY_MENU:
			Asp = world.mReadyMenuAspect;
			break;
		case GameState::ROUND_OVER_MENU:
			Asp = world.mRoundOverMenuAspect;
			break;
		default:
			return;
	}

	std::vector<unsigned int> entities = this->getEntities( world );

	glm::mat4 viewMat;
	glm::mat4 projmat;
	world.mCamera.GetViewMatrix( viewMat );
	world.mCamera.GetProjectionMatrix( projmat );

	this->shader.Activate();
	this->shader.SetUniform( 1, viewMat, this->viewMatrixPosition );
	this->shader.SetUniform( 1, projmat, this->projectionMatrixPosition );

	for( int i = 0; i < this->settings.size(); i++ )
	{
		Setting& setting = this->settings[ i ];
		setting.mesh.Bind();

		for( auto it : entities )
		{
			WorldPositionComponent* wpc = (WorldPositionComponent*)world.mEntityHandler->GetComponent( it, WORLD_POSITION_COMPONENT );
			GraphicsComponent* grc = (GraphicsComponent*)world.mEntityHandler->GetComponent( it, GRAPHICS_COMPONENT );
			ScaleComponent* scc = (ScaleComponent*)world.mEntityHandler->GetComponent( it, SCALE_COMPONENT );

			if( grc->meshType != setting.meshType || !grc->renderMe)
				continue;

			this->shader.SetUniform( 1, glm::translate( glm::mat4(1), glm::vec3( static_cast<float>(wpc->x), static_cast<float>(wpc->y), static_cast<float>(wpc->z) ) ), this->modelMatrixPosition );
			
			// set materials...
			this->shader.SetUniform( 1, glm::vec3( grc->meshMaterial.diffuse_red, grc->meshMaterial.diffuse_green, grc->meshMaterial.diffuse_blue ), this->colorPos );
			
			this->shader.SetUniform( 1, glm::vec4( 
				grc->meshMaterial.specular_red, grc->meshMaterial.specular_green, grc->meshMaterial.specular_blue, grc->meshMaterial.specularIntensity ), 
				this->specularPos );
			
			this->shader.SetUniform( 1, glm::vec4( 
				grc->meshMaterial.glow_red, grc->meshMaterial.glow_green, grc->meshMaterial.glow_blue, grc->meshMaterial.glowIntensity ), 
				this->glowPos );

			// set model matrix...
			this->shader.SetUniform( 1, glm::translate( glm::mat4(1), glm::vec3( static_cast<float>(wpc->x), static_cast<float>(wpc->y), static_cast<float>(wpc->z) ) ) * glm::scale(glm::mat4(1.0f), glm::vec3(scc->x, scc->y, scc->z)), this->modelMatrixPosition );

			glDrawElements( setting.mode, setting.mesh.GetIndices().size(), GL_UNSIGNED_INT, setting.mesh.GetIndices().data() ); 			
		}
	}

	this->shader.DeActivate();
	//glBindVertexArray( 0 );
}


