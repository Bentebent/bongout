#ifndef GLGLOWPAINTER_HPP
#define GLGLOWPAINTER_HPP

#include <GraphicsSystem/OpenGL/GLShader.hpp>
#include <GraphicsSystem/OpenGL/GLMesh.hpp>
#include <ComponentFramework/System.hpp>

#include <GraphicsSystem/OpenGL/FBOTexture.hpp>

class Camera;
class World;

class GLGlowPainter : public System
{
private:
	void ResizeGlowTextures();

	void SetFBOforGlowPass( FBOTexture& texture );

public:
	GLGlowPainter();
	~GLGlowPainter();

	void Init();
	virtual void Update(World &world) override;
	void ApplyGlow();

private:

	glm::vec2 windowSize;

	GLShader blurrShader;
	GLShader glowShader;

	std::vector< int > glowSamplerPos;
	int deferredGlowSamplerPos;
	int blurrSamplerPos;

	struct Setting {
		GLMesh mesh;
		int meshId;
	};

	std::vector< FBOTexture > fboTextures;
};
#endif