#include "GLDirectionalLightPainterSystem.hpp"

#include <GLFW/glew.h>
#include "GLgraphics.hpp"
#include "GLMesh.hpp"
#include <GraphicsSystem/GeometryGenerator.hpp>
#include <GraphicsSystem/Camera.hpp>
#include "glm/ext.hpp"

#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <GraphicsSystem/GraphicsMaterial.hpp>

GLDirectionalLightPainterSystem::GLDirectionalLightPainterSystem()
{
}

GLDirectionalLightPainterSystem::~GLDirectionalLightPainterSystem()
{
}


void GLDirectionalLightPainterSystem::Init()
{
	this->shader.LoadShader( "assets/OpenglShaders/ScreenQuad.vertex", "assets/OpenglShaders/ScreenQuad.geometry", "assets/OpenglShaders/DirectionalLight.fragment" );

	this->LightDirectionPos = this->shader.GetPosition( "lightDirection" );
	this->diffuseSamplerPos = this->shader.GetPosition( "diffuseSampler" );
	this->normalSamplerPos = this->shader.GetPosition( "normalSampler" );
	this->positionSamplerPos = this->shader.GetPosition( "positionSampler" );
	this->specularSamperPos = this->shader.GetPosition( "specularSampler" );
	this->colourPos = this->shader.GetPosition( "colour" );
	this->cameraPos = this->shader.GetPosition( "cameraPos" );
}


void GLDirectionalLightPainterSystem::Update( World& world )
{
	Aspect gameplayAspect;
	gameplayAspect.AddExclusiveAspect(MENU_COMPONENT);
	gameplayAspect.AddInclusiveAspect(PHYSICS_COMPONENT);
	gameplayAspect.AddInclusiveAspect(LIGHT_COMPONENT);

	Aspect mainMenuAspect;
	mainMenuAspect.AddInclusiveAspect(MAIN_MENU_COMPONENT);
	mainMenuAspect.AddInclusiveAspect(LIGHT_COMPONENT);

	Aspect endMenuAspect;
	endMenuAspect.AddInclusiveAspect(END_MENU_COMPONENT);
	endMenuAspect.AddInclusiveAspect(LIGHT_COMPONENT);

	Aspect readyMenuAspect;
	readyMenuAspect.AddInclusiveAspect(READY_MENU_COMPONENT);
	readyMenuAspect.AddInclusiveAspect(LIGHT_COMPONENT);

	Aspect roundOverMenuAspect;
	roundOverMenuAspect.AddInclusiveAspect(ROUND_OVER_MENU_COMPONENT);
	roundOverMenuAspect.AddInclusiveAspect(LIGHT_COMPONENT);

	switch (world.mCurrentState)
	{
		case GameState::GAMEPLAY:
			Asp = gameplayAspect;
			break;

		case GameState::MAIN_MENU:
			Asp = mainMenuAspect;
			break;
		case GameState::END_GAME_MENU:
			Asp = endMenuAspect;
			break;
		case GameState::READY_MENU:
			Asp = readyMenuAspect;
			break;
		case GameState::ROUND_OVER_MENU:
			Asp = gameplayAspect;
			break;
		default: // load_state goes here...
			return;
	}

	std::vector<unsigned int> entities = this->getEntities( world );

	this->shader.Activate();
	glBindVertexArray( GLgraphics::Get().GetDummyVAO() );

	this->shader.SetUniform( GLgraphics::Get().GetDiffuseTextureHandle(), this->diffuseSamplerPos );
	this->shader.SetUniform( GLgraphics::Get().GetPositionTextureHandle(), this->positionSamplerPos );
	this->shader.SetUniform( GLgraphics::Get().GetNormalTextureHandle(), this->normalSamplerPos );
	this->shader.SetUniform( GLgraphics::Get().GetSpecularTextureHandle(), this->specularSamperPos );

	this->shader.SetUniform( 1, world.mCamera.GetPosition(), cameraPos );

	for( auto it : entities )
	{
		LightComponent* lightComp = (LightComponent*)world.mEntityHandler->GetComponent( it, LIGHT_COMPONENT );

		if( lightComp->lightType != LightType::DirectionalLight )
			continue;

		this->shader.SetUniform( 1, 
			glm::normalize( glm::vec3( lightComp->lightMaterial.offsetX, lightComp->lightMaterial.offsetY, lightComp->lightMaterial.offsetZ  ) ), 
			this->LightDirectionPos );

		this->shader.SetUniform( 1, world.mCamera.GetPosition(), this->cameraPos );

		this->shader.SetUniform( 1,
			glm::vec3( 
				lightComp->lightMaterial.red * lightComp->lightMaterial.intensity, 
				lightComp->lightMaterial.green * lightComp->lightMaterial.intensity, 
				lightComp->lightMaterial.blue * lightComp->lightMaterial.intensity ), 
				this->colourPos );

		glDrawArrays( GL_POINTS, 0, 1 ); 
	}

	this->shader.DeActivate();
	glBindVertexArray( 0 );

}


