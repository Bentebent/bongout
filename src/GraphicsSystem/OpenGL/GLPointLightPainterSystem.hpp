#ifndef GLPOINTLIGHTPAINTERSYSTEM_HPP
#define GLPOINTLIGHTPAINTERSYSTEM_HPP

#include <GraphicsSystem/OpenGL/GLShader.hpp>
#include <GraphicsSystem/OpenGL/GLMesh.hpp>
#include <ComponentFramework/System.hpp>

class Camera;
class World;

class GLPointLightPainterSystem : public System
{
public:
	GLPointLightPainterSystem();
	~GLPointLightPainterSystem();

	void Init();
	virtual void Update(World &world) override;

private:

	GLShader shader;

	int viewMatrixPosition;
	int projectionMatrixPosition;

	int sizePos;
	int lightPositionPos;
	int sceenSizePos;
	int cameraPos;
	int colourPos;
	int diffuseSamplerPos;
	int normalSamplerPos;
	int positionSamplerPos;
	int specularSamperPos;

	struct Setting {
		GLMesh mesh;
		int meshId;
	};
	std::vector< Setting > settings;


};
#endif