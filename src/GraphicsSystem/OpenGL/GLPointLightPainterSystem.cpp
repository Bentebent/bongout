#include "GLPointLightPainterSystem.hpp"

#include <GLFW/glew.h>
#include "GLgraphics.hpp"
#include "GLMesh.hpp"
#include <GraphicsSystem/GeometryGenerator.hpp>
#include <GraphicsSystem/Camera.hpp>
#include "glm/ext.hpp"

#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <GraphicsSystem/GraphicsMaterial.hpp>

GLPointLightPainterSystem::GLPointLightPainterSystem()
{
}

GLPointLightPainterSystem::~GLPointLightPainterSystem()
{
}


void GLPointLightPainterSystem::Init()
{
	this->shader.LoadShader( "assets/OpenglShaders/lightBox.vertex", "assets/OpenglShaders/lightBox.geometry", "assets/OpenglShaders/lightBox.fragment" );

	this->viewMatrixPosition = this->shader.GetPosition( "viewMatrix" );
	this->projectionMatrixPosition = this->shader.GetPosition( "projMatrix" );
	this->sizePos = this->shader.GetPosition( "size" );
	this->lightPositionPos = this->shader.GetPosition( "boxPosition" );
	this->cameraPos = this->shader.GetPosition( "cameraPos" );
	this->sceenSizePos = this->shader.GetPosition( "screenSize" );
	this->diffuseSamplerPos = this->shader.GetPosition( "diffuseSampler" );
	this->normalSamplerPos = this->shader.GetPosition( "normalSampler" );
	this->positionSamplerPos = this->shader.GetPosition( "positionSampler" );
	this->specularSamperPos = this->shader.GetPosition( "specularSampler" );
	this->colourPos = this->shader.GetPosition( "colour" );
}


void GLPointLightPainterSystem::Update( World& world )
{
	Aspect gameplayAspect;
	gameplayAspect.AddInclusiveAspect(PHYSICS_COMPONENT);
	gameplayAspect.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	gameplayAspect.AddInclusiveAspect(LIGHT_COMPONENT);

	Aspect mainMenuAspect;
	mainMenuAspect.AddInclusiveAspect(MAIN_MENU_COMPONENT);
	mainMenuAspect.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	mainMenuAspect.AddInclusiveAspect(LIGHT_COMPONENT);

	Aspect readyMenuAspect;
	readyMenuAspect.AddInclusiveAspect(READY_MENU_COMPONENT);
	readyMenuAspect.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	readyMenuAspect.AddInclusiveAspect(LIGHT_COMPONENT);

	Aspect roundOverMenuAspect;
	roundOverMenuAspect.AddInclusiveAspect(READY_MENU_COMPONENT);
	roundOverMenuAspect.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	roundOverMenuAspect.AddInclusiveAspect(LIGHT_COMPONENT);


	switch (world.mCurrentState)
	{
		case GameState::GAMEPLAY:
			Asp = gameplayAspect;
			break;

		case GameState::MAIN_MENU:
			Asp = mainMenuAspect;
			break;
		case GameState::READY_MENU:
			Asp = gameplayAspect;
			break;
		case GameState::ROUND_OVER_MENU:
			Asp = gameplayAspect;
			break;
		default: // load_state goes here...
			return;
	}

	std::vector<unsigned int> entities = this->getEntities( world );

	glm::mat4 view;
	glm::mat4 proj;
	world.mCamera.GetViewMatrix( view );
	world.mCamera.GetProjectionMatrix( proj );

	this->shader.Activate();
	this->shader.SetUniform( 1, view, this->viewMatrixPosition );
	this->shader.SetUniform( 1, proj, this->projectionMatrixPosition );
	this->shader.SetUniform( 1, GLgraphics::Get().GetWindowSize(), this->sceenSizePos );

	glBindTexture( GL_TEXTURE_2D, GLgraphics::Get().GetDiffuseTextureHandle() );
	glBindTexture( GL_TEXTURE_2D, GLgraphics::Get().GetPositionTextureHandle() );
	glBindTexture( GL_TEXTURE_2D, GLgraphics::Get().GetNormalTextureHandle() );
	glBindTexture( GL_TEXTURE_2D, GLgraphics::Get().GetSpecularTextureHandle() );

	this->shader.SetUniform( GLgraphics::Get().GetDiffuseTextureHandle(), this->diffuseSamplerPos );
	this->shader.SetUniform( GLgraphics::Get().GetPositionTextureHandle(), this->positionSamplerPos );
	this->shader.SetUniform( GLgraphics::Get().GetNormalTextureHandle(), this->normalSamplerPos );
	this->shader.SetUniform( GLgraphics::Get().GetSpecularTextureHandle(), this->specularSamperPos );
	
	glBindVertexArray( GLgraphics::Get().GetDummyVAO() );

	for( auto it : entities )
	{
		WorldPositionComponent* wpc = (WorldPositionComponent*)world.mEntityHandler->GetComponent( it, WORLD_POSITION_COMPONENT );
		LightComponent* lightComp = (LightComponent*)world.mEntityHandler->GetComponent( it, LIGHT_COMPONENT );

		if( lightComp->lightType != LightType::PointLight )
			continue;

		this->shader.SetUniform( 1, 
			glm::vec3( wpc->x + lightComp->lightMaterial.offsetX, wpc->y + lightComp->lightMaterial.offsetY, wpc->z + lightComp->lightMaterial.offsetZ  ), 
			this->lightPositionPos );

		this->shader.SetUniform( 1, world.mCamera.GetPosition(), this->cameraPos );

		this->shader.SetUniform( lightComp->lightMaterial.size, this->sizePos );
		this->shader.SetUniform( 1, 
			glm::vec3( 
				lightComp->lightMaterial.red * lightComp->lightMaterial.intensity, 
				lightComp->lightMaterial.green * lightComp->lightMaterial.intensity, 
				lightComp->lightMaterial.blue * lightComp->lightMaterial.intensity ), 
				this->colourPos );

		glDrawArrays( GL_POINTS, 0, 1 ); 
	}

	this->shader.DeActivate();
	glBindVertexArray( 0 );

}


