#ifndef GLAMBIENTPAINTER_HPP
#define GLAMBIENTPAINTER_HPP

#include <GraphicsSystem/OpenGL/GLShader.hpp>
#include <GraphicsSystem/OpenGL/GLMesh.hpp>
#include <ComponentFramework/System.hpp>

class Camera;
class World;

class GLAmbientPainter : public System
{
public:
	GLAmbientPainter();
	~GLAmbientPainter();

	void Init();
	virtual void Update(World &world) override;

private:

	GLShader shader;

	int ambienceLevelPos;
	int colourPos;
	int diffuseSamplerPos;

	struct Setting {
		GLMesh mesh;
		int meshId;
	};
	std::vector< Setting > settings;


};
#endif