#include "GLMesh.hpp"

#include <GraphicsSystem/Vertex.hpp>


GLMesh::GLMesh()
{
	this->VAO = 0;
	this->VBO = 0;
	this->elementBuffer = 0;
}

GLMesh::~GLMesh()
{
	if( this->VAO > 0 )
		glDeleteBuffers( 1, &VBO );
}

void GLMesh::Bind()
{
	glBindVertexArray( this->VAO );
}

void GLMesh::unBind()
{
	glBindVertexArray( 0 );
}

void GLMesh::Load( MeshData& data )
{
	glGenBuffers( 1, &this->VBO );

	glBindBuffer( GL_ARRAY_BUFFER, this->VBO );
	glBufferData( GL_ARRAY_BUFFER, data.mVertices.size() * sizeof( Vertex ), data.mVertices.data(), GL_STATIC_DRAW );

	glGenVertexArrays( 1, &this->VAO );
	glBindVertexArray( this->VAO );

	// position data
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), (void*)0 );
	
	// normal data
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), (void*)(3 * sizeof( float )) );

	// uv data
	glEnableVertexAttribArray( 2 );
	glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, sizeof( Vertex ), (void*)(6 * sizeof( float )) );

	glBindVertexArray(0);

	// fix indicies...
	/* // potential optimisation-thingy, low-prio...
	glGenBuffers( 1, &this->elementBuffer );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->elementBuffer );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, data.mIndices.size() * sizeof( int ), data.mIndices.data(), GL_STATIC_DRAW );
	*/

	this->meshData = data;
}

