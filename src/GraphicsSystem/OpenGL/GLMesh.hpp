#ifndef GLMESH_HPP
#define GLMESH_HPP

#include <string>
#include <GLFW/glew.h>
#include <GraphicsSystem/MeshData.hpp>
#include <vector>

class GLMesh
{
public:
	GLMesh();
	~GLMesh();

	void Bind();
	void unBind();
	std::vector< unsigned int >& GetIndices() { return this->meshData.mIndices; }

	void Load( MeshData& data );

private:

	GLuint VAO;
	GLuint VBO;
	GLuint elementBuffer;

	MeshData meshData;
};

#endif