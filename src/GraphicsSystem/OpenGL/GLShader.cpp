#pragma once
#include "GLShader.hpp"
#include <Utility/ErrorHandler.hpp>
#include <fstream>
#include <iostream>
#include "GLgraphics.hpp"

GLShader::GLShader()
{
	this->programHandle = 0;
}

GLShader::~GLShader()
{
	if( this->programHandle > 0 )
		glDeleteShader( this->programHandle );
}

void GLShader::LoadShader( std::string vertexShader, std::string fragmentShader )
{
	GLuint vertexHandle = LoadShader( vertexShader, GL_VERTEX_SHADER );
	GLuint fragmentHandle = LoadShader( fragmentShader, GL_FRAGMENT_SHADER );

	GLuint programHandle = glCreateProgram();
	if ( programHandle == 0 ) {
		ErrorHandler::Get().Throw( "Error creating shader program. " );
		return;
	}

	glAttachShader( programHandle, vertexHandle );
	glAttachShader( programHandle, fragmentHandle );

	glLinkProgram( programHandle );

	GLint status;
	glGetProgramiv( programHandle, GL_LINK_STATUS, &status );
	if ( status == GL_FALSE ) {
		int length = 0;
		glGetProgramiv( programHandle, GL_INFO_LOG_LENGTH, &length );
		if ( length > 0 )
		{
			char* log = new char[ length ];
			int written = 0;
			glGetProgramInfoLog( programHandle, sizeof(log), &length, log );
			std::cout << log << std::endl << std::endl;
			delete[] log;
		}	
		ErrorHandler::Get().Throw( "error linking shader program: " + vertexShader );
		return;
	}

	this->programHandle = programHandle;
}

void GLShader::LoadShader( std::string vertexShader, std::string geometryShader, std::string fragmentShader )
{
	GLuint vertexHandle = LoadShader( vertexShader, GL_VERTEX_SHADER );
	GLuint geometryHandle = LoadShader( geometryShader, GL_GEOMETRY_SHADER );
	GLuint fragmentHandle = LoadShader( fragmentShader, GL_FRAGMENT_SHADER );

	GLuint programHandle = glCreateProgram();
	if ( programHandle == 0 ) {
		ErrorHandler::Get().Throw( "Error creating shader program. " );
		return;
	}

	glAttachShader( programHandle, vertexHandle );
	glAttachShader( programHandle, geometryHandle );
	glAttachShader( programHandle, fragmentHandle );

	glLinkProgram( programHandle );

	GLint status;
	glGetProgramiv( programHandle, GL_LINK_STATUS, &status );
	if ( status == GL_FALSE ) {
		int length = 0;
		glGetProgramiv( programHandle, GL_INFO_LOG_LENGTH, &length );
		if ( length > 0 )
		{
			char* log = new char[ length ];
			int written = 0;
			glGetProgramInfoLog( programHandle, length, &length, log );
			std::cout << log << std::endl << std::endl;
			delete[] log;
		}	
		ErrorHandler::Get().Throw( "error linking shader program: " + vertexShader );
		return;
	}

    std::cout << "Succesfully created shader: " << vertexShader << std::endl << geometryShader << std::endl << fragmentShader << std::endl;

	this->programHandle = programHandle;
}

GLuint GLShader::LoadShader( std::string path, GLuint shaderType )
{
	std::fstream ff;
	ff.open( path );
	if ( !ff ) {
		ErrorHandler::Get().Throw( "error opening shader file : " + path );
		return -1;
	}
	
	std::string text;

	char tmpLine[500];
	while( !ff.eof() ) {
		ff.getline( tmpLine, 500 );
		text += tmpLine;
		text += "\n";
	}
	ff.close();
	
	GLuint handle = glCreateShader( shaderType );
	if ( handle == 0 ) {
		ErrorHandler::Get().Throw( "Error creating shader" );
		return -1;
	}

	const char* ptr = text.c_str();
	glShaderSource( handle, 1, &ptr, NULL );
	glCompileShader( handle );

	// check for errors...
	int result = 0;
	glGetShaderiv( handle, GL_COMPILE_STATUS, &result );
	if ( result == GL_FALSE )
	{
		int length = 0;
		glGetShaderiv( handle, GL_INFO_LOG_LENGTH, &length );
		if ( length > 0 )
		{
			char* log = new char[ length ];
			int written = 0;
			glGetShaderInfoLog( handle, length, &written, log );
			std::cout << log << std::endl << std::endl;
			delete[] log;
			ErrorHandler::Get().Throw( "Error compiling shader: " + path );
			return -1;
		}
	}

    std::cout << "Succesfully created shader: "<< path << std::endl;
	
	return handle;
}

void GLShader::Activate() {
	glUseProgram( this->programHandle );
}

void GLShader::DeActivate() {
	glUseProgram( 0 );
}


GLuint GLShader::GetPosition( std::string name ) {
	return glGetUniformLocation( this->programHandle, name.c_str() );
}


/// //////////////////////////////////// uniform setters
// single floats...
void GLShader::SetUniform( std::string name, GLfloat v1 ) {
	glUniform1f( this->GetPosition( name ), v1 );
}

void GLShader::SetUniform( std::string name, GLfloat v1, GLfloat v2 ) {
	glUniform2f( this->GetPosition( name ), v1, v2 );
}

void GLShader::SetUniform( std::string name, GLfloat v1, GLfloat v2, GLfloat v3 ) {
	glUniform3f( this->GetPosition( name ), v1, v2, v3 );
}

void GLShader::SetUniform( std::string name, GLfloat v1, GLfloat v2, GLfloat v3, GLfloat v4 ) {
	glUniform4f( this->GetPosition( name ), v1, v2, v3, v4 );
}

void GLShader::SetUniform( GLfloat v1, GLuint shaderVariableIndex )
{
	glUniform1f( shaderVariableIndex, v1 );
}

void GLShader::SetUniform( GLfloat v1, GLfloat v2, GLuint shaderVariableIndex )
{
	glUniform2f( shaderVariableIndex, v1, v2 );
}

void GLShader::SetUniform( GLfloat v1, GLfloat v2, GLfloat v3, GLuint shaderVariableIndex )
{
	glUniform3f( shaderVariableIndex, v1, v2, v3 );
}

void GLShader::SetUniform( GLfloat v1, GLfloat v2, GLfloat v3, GLfloat v4, GLuint shaderVariableIndex )	
{
	glUniform4f( shaderVariableIndex, v1, v2, v3, v4 );
}


// single ints...
void GLShader::SetUniform( std::string name, GLint v1 ) {
	glUniform1i( this->GetPosition( name ), v1 );
}

void GLShader::SetUniform( std::string name, GLint v1, GLint v2 ) {
	glUniform2i( this->GetPosition( name ), v1, v2 );
}

void GLShader::SetUniform( std::string name, GLint v1, GLint v2, GLint v3 ) {
	glUniform3i( this->GetPosition( name ), v1, v2, v3 );
}

void GLShader::SetUniform( std::string name, GLint v1, GLint v2, GLint v3, GLint v4 ) {
	glUniform4i( this->GetPosition( name ), v1, v2, v3, v4 );
}

void GLShader::SetUniform( GLint v1, GLuint shaderVariableIndex ) {
	glUniform1i( shaderVariableIndex, v1 );
}

void GLShader::SetUniform( GLint v1, GLint v2, GLuint shaderVariableIndex ) {
	glUniform2i( shaderVariableIndex, v1, v2 );
}

void GLShader::SetUniform( GLint v1, GLint v2, GLint v3, GLuint shaderVariableIndex ) {
	glUniform3i( shaderVariableIndex, v1, v2, v3 );
}

void GLShader::SetUniform( GLint v1, GLint v2, GLint v3, GLint v4, GLuint shaderVariableIndex ) {
	glUniform4i( shaderVariableIndex, v1, v2, v3, v4 );
}

// vectors and matrices...
void GLShader::SetUniform( std::string name, glm::vec3 v1 ){
	glUniform3fv( this->GetPosition( name ), 1, &v1[0] );
}

void GLShader::SetUniform( std::string name, int count, glm::vec3 v1[] )
{
	glUniform3fv( this->GetPosition( name ), count, &v1[0][0] );
}

void GLShader::SetUniform( std::string name, int count, glm::mat2 v1 ){
	glUniformMatrix2fv( this->GetPosition( name ), count, GL_FALSE, &v1[0][0] );
}

void GLShader::SetUniform( std::string name, int count, glm::mat3 v1 ) {
	glUniformMatrix3fv( this->GetPosition( name ), count, GL_FALSE, &v1[0][0] );
}

void GLShader::SetUniform( std::string name, int count, glm::mat4 v1 ) {
	glUniformMatrix4fv( this->GetPosition( name ), count, GL_FALSE, &v1[0][0] );
}

void GLShader::SetUniform( int count, glm::vec2 v1, GLuint shaderVariableIndex ){
	glUniform2fv( shaderVariableIndex, count, &v1[0] );
}

void GLShader::SetUniform( int count, glm::vec3 v1, GLuint shaderVariableIndex ){
	glUniform3fv( shaderVariableIndex, count, &v1[0] );
}

void GLShader::SetUniform( int count, glm::vec4 v1, GLuint shaderVariableIndex ){
	glUniform4fv( shaderVariableIndex, count, &v1[0] );
}

void GLShader::SetUniform( int count, glm::mat2 v1, GLuint shaderVariableIndex ){
	glUniformMatrix2fv( shaderVariableIndex, count, GL_FALSE, &v1[0][0] );
}

void GLShader::SetUniform( int count, glm::mat3 v1, GLuint shaderVariableIndex ) {
	glUniformMatrix3fv( shaderVariableIndex, count, GL_FALSE, &v1[0][0] );
}

void GLShader::SetUniform( int count, glm::mat4 v1, GLuint shaderVariableIndex ) {
	glUniformMatrix4fv( shaderVariableIndex, count, GL_FALSE, &v1[0][0] );
}

