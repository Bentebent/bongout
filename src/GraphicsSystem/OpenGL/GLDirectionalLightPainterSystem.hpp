#ifndef GLDIRECTIONALLIGHTPAINTERSYSTEM_HPP
#define GLDIRECTIONALLIGHTPAINTERSYSTEM_HPP

#include <GraphicsSystem/OpenGL/GLShader.hpp>
#include <GraphicsSystem/OpenGL/GLMesh.hpp>
#include <ComponentFramework/System.hpp>

class Camera;
class World;

class GLDirectionalLightPainterSystem : public System
{
public:
	GLDirectionalLightPainterSystem();
	~GLDirectionalLightPainterSystem();

	void Init();
	virtual void Update(World &world) override;

private:

	GLShader shader;

	int LightDirectionPos;
	int colourPos;
	int cameraPos;
	int diffuseSamplerPos;
	int normalSamplerPos;
	int positionSamplerPos;
	int specularSamperPos;
	int glowSamplerPos;

	struct Setting {
		GLMesh mesh;
		int meshId;
	};
	std::vector< Setting > settings;


};
#endif