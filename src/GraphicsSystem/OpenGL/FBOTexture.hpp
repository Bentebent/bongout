#ifndef RENDERTEXTURE_HPP
#define RENDERTEXTURE_HPP

#include <GLFW/glew.h>


class FBOTexture
{
private:

	void GenerateTexture();

public:
	FBOTexture();
	~FBOTexture();

	void Init( GLenum textureTarget, GLint magFilter, GLint minFilter, GLint wrapModeS, GLint wrapModeT, GLint internalFormat, GLint format );
	void UpdateResolution( int x, int y );
	GLuint GetTextureHandle();

public:
	
private:

	GLuint handle;
	GLenum textureTarget;

	GLint magFilter, minFilter;
	GLint wrapS, wrapT;

	GLint internalFormat, format;
};


#endif