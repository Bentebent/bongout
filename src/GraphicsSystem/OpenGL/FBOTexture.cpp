#include "FBOTexture.hpp"

#include <GraphicsSystem/Vertex.hpp>


FBOTexture::FBOTexture()
{
	this->handle = 0;
	this->textureTarget = 0;

	this->magFilter = 0;
	this->minFilter = 0;
	this->wrapS = 0;
	this->wrapT = 0;

	this->internalFormat = 0;
	this->format = 0;
}

FBOTexture::~FBOTexture()
{
}


void FBOTexture::Init( GLenum textureTarget_, GLint magFilter_, GLint minFilter_, GLint wrapModeS_, GLint wrapModeT_, GLint internalFormat_, GLint format_ )
{
	this->handle = 0;
	this->textureTarget = textureTarget_;

	this->magFilter = magFilter_;
	this->minFilter = minFilter_;
	this->wrapS = wrapModeS_;
	this->wrapT = wrapModeT_;

	this->internalFormat = internalFormat_;
	this->format = format_;

	this->GenerateTexture();
}


void FBOTexture::GenerateTexture()
{
	glGenTextures( 1, &this->handle );
	glActiveTexture( GL_TEXTURE0 + this->handle );
	glBindTexture(  this->textureTarget, this->handle );

	glTexParameterf(this->textureTarget, GL_TEXTURE_MAG_FILTER, this->magFilter );
	glTexParameterf(this->textureTarget, GL_TEXTURE_MIN_FILTER, this->minFilter );
	glTexParameteri(this->textureTarget, GL_TEXTURE_WRAP_S, this->wrapS );
	glTexParameteri(this->textureTarget, GL_TEXTURE_WRAP_T, this->wrapT );
}


void FBOTexture::UpdateResolution( int x, int y )
{
	glActiveTexture( GL_TEXTURE0 + handle );
	glBindTexture( this->textureTarget, this->handle );
	glTexImage2D( this->textureTarget, 0, this->internalFormat, x, y, 0, this->format, GL_UNSIGNED_BYTE, nullptr );
}

GLuint FBOTexture::GetTextureHandle()
{
	return this->handle;
}




