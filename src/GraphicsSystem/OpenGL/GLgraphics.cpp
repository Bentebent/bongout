
#include "GLgraphics.hpp"

#include <GLFW/glew.h>
#include "GLFW/glfw3.h"

#include <sstream>
#include <Utility/ErrorHandler.hpp>
#include <ComponentFramework/Systems/ParticleSystem.hpp>
#include <ComponentFramework/World.hpp>



GLgraphics& GLgraphics::Get()
{
	static GLgraphics g;
	return g;
}


GLgraphics::GLgraphics()
{
	this->dummyVAO = 0;
}

GLgraphics::~GLgraphics()
{
}


void GLgraphics::Init( int windowSizeX, int windowSizeY, ParticleSystem* particleSystem)
{
	this->windowSize = glm::vec2( windowSizeX, windowSizeY );
	
	// init painters...
	this->geoPainter.Init();
	this->pointLightPainter.Init();
	this->directionalPainter.Init();
	this->ambientPainter.Init();
	this->glowPainter.Init();

	// utility...
	this->InitDummyVAO();
	this->deferredFBO = 0;
	this->newWindowSize = false;

	this->particleSystem = particleSystem;

	this->ambienceColor = glm::vec3(0);
	this->ambienceLevel = 0;

	this->InitDeferredSystem();
}

void GLgraphics::SetNewWindowLimits( int x, int y )
{
	this->windowSize = glm::vec2( x, y );
	this->newWindowSize = true;
}

void GLgraphics::SetDeferredSystemForGeobufferOutput()
{
	// attatch textures to fbo...
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this->deferredDepthTexture.GetTextureHandle(),	0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->diffuseTexture.GetTextureHandle(),		0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, this->positionTexture.GetTextureHandle(),		0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, this->normalTexture.GetTextureHandle(),		0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, this->specularTexture.GetTextureHandle(),		0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, this->glowTexture.GetTextureHandle(),			0 );

	// define outputs...
	GLenum drawBuffers[] = { GL_NONE, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4 };
	glDrawBuffers( 6, drawBuffers );
}

void GLgraphics::InitDeferredSystem()
{
	// generate fbo...
	glGenFramebuffers( 1, &this->deferredFBO );
	glBindFramebuffer( GL_FRAMEBUFFER, this->deferredFBO );

	// init texture targets...
	this->deferredDepthTexture.Init( GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT );
	this->diffuseTexture.Init( GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA );
	this->positionTexture.Init( GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA );
	this->normalTexture.Init( GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA );
	this->specularTexture.Init( GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA );
	this->glowTexture.Init( GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA );

	// resize texture targets...
	this->ResizeDeferredSystem();

	this->SetDeferredSystemForGeobufferOutput();

	GLenum status;
    status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
	if ( status != GL_FRAMEBUFFER_COMPLETE )
		ErrorHandler::Get().Throw( "Error initialising the deferred system" );

	this->DeActivateFBO();
}

void GLgraphics::ResizeDeferredSystem()
{
	this->deferredDepthTexture.UpdateResolution( this->windowSize.x, this->windowSize.y );
	this->diffuseTexture.UpdateResolution( this->windowSize.x, this->windowSize.y );
	this->positionTexture.UpdateResolution( this->windowSize.x, this->windowSize.y );
	this->normalTexture.UpdateResolution( this->windowSize.x, this->windowSize.y );
	this->specularTexture.UpdateResolution( this->windowSize.x, this->windowSize.y );
	this->glowTexture.UpdateResolution( this->windowSize.x, this->windowSize.y );
}

void GLgraphics::ActivateDeferredFBO()
{
	glBindFramebuffer( GL_FRAMEBUFFER, this->deferredFBO );
}

void GLgraphics::DeActivateFBO()
{
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
}

void GLgraphics::InitDummyVAO()
{
	float dummy = 1;

	GLuint VBO;
	glGenBuffers( 1, &VBO );
	glBindBuffer( GL_ARRAY_BUFFER, VBO );
	glBufferData( GL_ARRAY_BUFFER, sizeof(float), &dummy, GL_STATIC_DRAW );

	glGenVertexArrays( 1, &this->dummyVAO );
	glBindVertexArray( this->dummyVAO );

	glEnableVertexAttribArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, VBO );
	glVertexAttribPointer( 0, 1, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL );

	glBindVertexArray(0);
}

void GLgraphics::SetAmbienceLevel( float level )
{
	this->ambienceLevel = level;
}

void GLgraphics::SetAmbienceColor( glm::vec3 color )
{
	this->ambienceColor = color;
}

void GLgraphics::LoadObj( std::string path, unsigned int mode, MeshType type )
{
	this->geoPainter.LoadObj( path, mode, type );
}

void GLgraphics::Update( World& world )
{
	if( this->newWindowSize ) {
		this->newWindowSize = false;
		glViewport( 0, 0, this->windowSize.x, this->windowSize.y );
		this->ResizeDeferredSystem();

		world.mCamera.UpdateProjection( this->windowSize.x, this->windowSize.y, 1, 500 );
	}

	this->ActivateDeferredFBO();
	this->SetDeferredSystemForGeobufferOutput();
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glDisable( GL_BLEND );
	glEnable( GL_DEPTH_TEST );

	this->geoPainter.Update( world );
	this->particleSystem->Render( world );
	
	glDisable( GL_DEPTH_TEST );
	this->glowPainter.Update( world );

	this->DeActivateFBO();
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glEnable( GL_BLEND );

	this->ambientPainter.Update( world );
	this->pointLightPainter.Update( world );
	this->directionalPainter.Update( world );

	this->glowPainter.ApplyGlow();
}
