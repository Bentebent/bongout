#include "GLAmbientPainter.hpp"

#include <GLFW/glew.h>
#include "GLgraphics.hpp"
#include "GLMesh.hpp"
#include <GraphicsSystem/GeometryGenerator.hpp>
#include <GraphicsSystem/Camera.hpp>
#include "glm/ext.hpp"

#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <GraphicsSystem/GraphicsMaterial.hpp>

GLAmbientPainter::GLAmbientPainter()
{
}

GLAmbientPainter::~GLAmbientPainter()
{
}


void GLAmbientPainter::Init()
{
	this->shader.LoadShader( "assets/OpenglShaders/ScreenQuad.vertex", "assets/OpenglShaders/ScreenQuad.geometry", "assets/OpenglShaders/AmbientShader.fragment" );

	this->colourPos = this->shader.GetPosition( "colour" );
	this->diffuseSamplerPos = this->shader.GetPosition( "diffuseSampler" );
}


void GLAmbientPainter::Update( World& world )
{
	this->shader.Activate();
	glBindVertexArray( GLgraphics::Get().GetDummyVAO() );

	this->shader.SetUniform( GLgraphics::Get().GetDiffuseTextureHandle(), this->diffuseSamplerPos );

	this->shader.SetUniform( 1, 
		glm::vec4( GLgraphics::Get().GetAmbienceColor(), GLgraphics::Get().GetAmbienceLevel() ), this->colourPos );

	glDrawArrays( GL_POINTS, 0, 1 ); 

	this->shader.DeActivate();
	glBindVertexArray( 0 );

}


