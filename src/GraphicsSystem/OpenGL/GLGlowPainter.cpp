#include "GLGlowPainter.hpp"

#include <GLFW/glew.h>
#include "GLgraphics.hpp"
#include "GLMesh.hpp"
#include <GraphicsSystem/GeometryGenerator.hpp>
#include <GraphicsSystem/Camera.hpp>
#include "glm/ext.hpp"

#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <GraphicsSystem/GraphicsMaterial.hpp>

GLGlowPainter::GLGlowPainter()
{
}

GLGlowPainter::~GLGlowPainter()
{
}

void GLGlowPainter::SetFBOforGlowPass( FBOTexture& texture )
{
	// attatch textures to fbo...
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0,												0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.GetTextureHandle(),		0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, 0,												0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, 0,												0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, 0,												0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, 0,												0 );

	// define outputs...
	GLenum drawBuffers[] = { GL_NONE, GL_COLOR_ATTACHMENT0, GL_NONE, GL_NONE, GL_NONE, GL_NONE };
	glDrawBuffers( 6, drawBuffers );
}


void GLGlowPainter::Init()
{
	this->windowSize = glm::vec2( 0 );

	this->blurrShader.LoadShader( "assets/OpenglShaders/ScreenQuad.vertex", "assets/OpenglShaders/ScreenQuad.geometry", "assets/OpenglShaders/blurrShader.fragment" );
	this->glowShader.LoadShader( "assets/OpenglShaders/ScreenQuad.vertex", "assets/OpenglShaders/ScreenQuad.geometry", "assets/OpenglShaders/GlowShader.fragment" );

	int nrTextures = 8;

	for( int i = 0; i < nrTextures; i++ )
		this->glowSamplerPos.push_back( this->glowShader.GetPosition( "glowSampler[" + std::to_string( i ) + "]" ));
	this->deferredGlowSamplerPos = this->glowShader.GetPosition( "deferredDlowSampler" );

	this->blurrSamplerPos = this->blurrShader.GetPosition( "glowSampler" );
	

	FBOTexture texture1;
	FBOTexture texture2;
	FBOTexture texture3;
	FBOTexture texture4;
	FBOTexture texture5;
	for( int i = 0; i < nrTextures; i++ )
		this->fboTextures.push_back( FBOTexture() );

	for( int i = 0; i < this->fboTextures.size(); i++ )
		this->fboTextures[i].Init( GL_TEXTURE_2D, GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA );
		//this->fboTextures[i].Init( GL_TEXTURE_2D, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_RGBA32F, GL_RGBA );
}

void GLGlowPainter::ResizeGlowTextures()
{
	glm::vec2 size = this->windowSize = GLgraphics::Get().GetWindowSize();

	for( int i = 0; i < this->fboTextures.size(); i++ )
	{
		size *= 0.5f;
		this->fboTextures[i].UpdateResolution( size.x, size.y );
	}
}


void GLGlowPainter::Update( World& world )
{
	glm::vec2 size = GLgraphics::Get().GetWindowSize();
	if( this->windowSize.x != size.x || this->windowSize.y != size.y )
		this->ResizeGlowTextures();

	glBindVertexArray( GLgraphics::Get().GetDummyVAO() );


	this->blurrShader.Activate();
	this->blurrShader.SetUniform( GLgraphics::Get().GetGlowTextureHandle(), this->blurrSamplerPos );
	glBindTexture( GL_TEXTURE_2D, GLgraphics::Get().GetGlowTextureHandle() );

	for( int i = 0; i < this->fboTextures.size(); i++ )
	{
		size *= 0.5f;
		glViewport( 0, 0, size.x, size.y );

		this->SetFBOforGlowPass( this->fboTextures[i] );
		glClear( GL_COLOR_BUFFER_BIT );

		glDrawArrays( GL_POINTS, 0, 1 ); 

		this->blurrShader.SetUniform( (GLint)this->fboTextures[i].GetTextureHandle(), this->blurrSamplerPos );
		glBindTexture( GL_TEXTURE_2D, this->fboTextures[i].GetTextureHandle() );
	}

	this->blurrShader.DeActivate();

	glViewport( 0, 0, windowSize.x, windowSize.y );
	glBindVertexArray( 0 );

}

void GLGlowPainter::ApplyGlow()
{
	glBindVertexArray( GLgraphics::Get().GetDummyVAO() );
	this->glowShader.Activate();

	this->glowShader.SetUniform( (GLint)GLgraphics::Get().GetGlowTextureHandle(), this->deferredGlowSamplerPos );
	glBindTexture( GL_TEXTURE_2D, GLgraphics::Get().GetGlowTextureHandle() );
	
	for( int i = 0; i < this->fboTextures.size(); i++ )	{
		this->glowShader.SetUniform( (GLint)this->fboTextures[i].GetTextureHandle(), this->glowSamplerPos[i] );
		glBindTexture( GL_TEXTURE_2D, this->fboTextures[i].GetTextureHandle() );
	}

	glDrawArrays( GL_POINTS, 0, 1 ); 

	this->glowShader.DeActivate();
	glBindVertexArray( 0 );
}

// glUniform2i( -1, 0, 0 );