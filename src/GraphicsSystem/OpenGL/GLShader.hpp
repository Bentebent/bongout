#ifndef GLSHADER_HPP
#define GLSHADER_HPP

#include <GLFW/glew.h>
#include <string>
#include "glm/glm.hpp"

class GLShader
{
private:


public:

	GLShader();
	~GLShader();

	void LoadShader( std::string vertexShader, std::string fragmentShader );
	void LoadShader( std::string vertexShader, std::string geometryShader, std::string fragmentShader );
	GLuint LoadShader( std::string path, GLuint shaderType );

	void Activate(); 
	void DeActivate();
	GLuint GetHandle() { return this->programHandle; }
	
	void IncreaseReference();
	void DecreaseReference();

	GLuint GetPosition( std::string name );

	// find location and set value...
	void SetUniform( std::string name, GLfloat v1 );
	void SetUniform( std::string name, GLfloat v1, GLfloat v2 );
	void SetUniform( std::string name, GLfloat v1, GLfloat v2, GLfloat v4 );
	void SetUniform( std::string name, GLfloat v1, GLfloat v2, GLfloat v3, GLfloat v4 );

	// set value at location...
	void SetUniform( GLfloat v1, GLuint shaderVariableIndex );
	void SetUniform( GLfloat v1, GLfloat v2, GLuint shaderVariableIndex );
	void SetUniform( GLfloat v1, GLfloat v2, GLfloat v4, GLuint shaderVariableIndex );
	void SetUniform( GLfloat v1, GLfloat v2, GLfloat v3, GLfloat v4, GLuint shaderVariableIndex );

	// find location and set value...
	void SetUniform( std::string name, GLint v1 );
	void SetUniform( std::string name, GLint v1, GLint v2 );
	void SetUniform( std::string name, GLint v1, GLint v2, GLint v3 );
	void SetUniform( std::string name, GLint v1, GLint v2, GLint v3, GLint v4 );
	
	// set value at location...
	void SetUniform( GLint v1, GLuint shaderVariableIndex );
	void SetUniform( GLint v1, GLint v2, GLuint shaderVariableIndex );
	void SetUniform( GLint v1, GLint v2, GLint v4, GLuint shaderVariableIndex );
	void SetUniform( GLint v1, GLint v2, GLint v3, GLint v4, GLuint shaderVariableIndex );

	// find location and set value...
	void SetUniform( std::string name, glm::vec3 v1 );
	void SetUniform( std::string name, int count, glm::vec3 v1[] );
	void SetUniform( std::string name, int count, glm::mat2 v1 );
	void SetUniform( std::string name, int count, glm::mat3 v1 );
	void SetUniform( std::string name, int count, glm::mat4 v1 );

	// set value at location...
	void SetUniform( int count, glm::vec2 v1, GLuint shaderVariableIndex );
	void SetUniform( int count, glm::vec3 v1, GLuint shaderVariableIndex );
	void SetUniform( int count, glm::vec4 v1, GLuint shaderVariableIndex );
	void SetUniform( int count, glm::mat2 v1, GLuint shaderVariableIndex );
	void SetUniform( int count, glm::mat3 v1, GLuint shaderVariableIndex );
	void SetUniform( int count, glm::mat4 v1, GLuint shaderVariableIndex );
	
private:
	GLuint programHandle;
};

#endif