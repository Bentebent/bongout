#include "GeometryGenerator.hpp"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

void GeometryGenerator::CreateBox(float width, float height, float depth, unsigned int numSubdivisions, MeshData& meshData)
{
	meshData.mVertices.clear();
	meshData.mIndices.clear();

	Vertex v[24];

	float w2 = 0.5f * width;
	float h2 = 0.5f * height;
	float d2 = 0.5f * depth;

	// Fill in the front face vertex data.
	v[0] = Vertex(-w2, -h2, -d2, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
	v[1] = Vertex(-w2, +h2, -d2, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
	v[2] = Vertex(+w2, +h2, -d2, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
	v[3] = Vertex(+w2, -h2, -d2, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

	// Fill in the back face vertex data.
	v[4] = Vertex(-w2, -h2, +d2, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);
	v[5] = Vertex(+w2, -h2, +d2, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
	v[6] = Vertex(+w2, +h2, +d2, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
	v[7] = Vertex(-w2, +h2, +d2, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f);

	// Fill in the top face vertex data.
	v[8]  = Vertex(-w2, +h2, -d2, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
	v[9]  = Vertex(-w2, +h2, +d2, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
	v[10] = Vertex(+w2, +h2, +d2, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);
	v[11] = Vertex(+w2, +h2, -d2, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);

	// Fill in the bottom face vertex data.
	v[12] = Vertex(-w2, -h2, -d2, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f);
	v[13] = Vertex(+w2, -h2, -d2, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f);
	v[14] = Vertex(+w2, -h2, +d2, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f);
	v[15] = Vertex(-w2, -h2, +d2, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

	// Fill in the left face vertex data.
	v[16] = Vertex(-w2, -h2, +d2, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
	v[17] = Vertex(-w2, +h2, +d2, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	v[18] = Vertex(-w2, +h2, -d2, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	v[19] = Vertex(-w2, -h2, -d2, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f);

	// Fill in the right face vertex data.
	v[20] = Vertex(+w2, -h2, -d2, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
	v[21] = Vertex(+w2, +h2, -d2, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	v[22] = Vertex(+w2, +h2, +d2, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	v[23] = Vertex(+w2, -h2, +d2, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f);

	meshData.mVertices.assign(&v[0], &v[24]);

	//Create indices

	unsigned int i[36];

	// Fill in the front face index data
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 2; i[5] = 3;

	// Fill in the back face index data
	i[6] = 4; i[7]  = 5; i[8]  = 6;
	i[9] = 4; i[10] = 6; i[11] = 7;

	// Fill in the top face index data
	i[12] = 8; i[13] =  9; i[14] = 10;
	i[15] = 8; i[16] = 10; i[17] = 11;

	// Fill in the bottom face index data
	i[18] = 12; i[19] = 13; i[20] = 14;
	i[21] = 12; i[22] = 14; i[23] = 15;

	// Fill in the left face index data
	i[24] = 16; i[25] = 17; i[26] = 18;
	i[27] = 16; i[28] = 18; i[29] = 19;

	// Fill in the right face index data
	i[30] = 20; i[31] = 21; i[32] = 22;
	i[33] = 20; i[34] = 22; i[35] = 23;

	meshData.mIndices.assign(&i[0], &i[36]);
	
	// Put a cap on the number of subdivisions.
	numSubdivisions = std::min(numSubdivisions, 5u);

	for(unsigned int i = 0; i < numSubdivisions; ++i)
		Subdivide(meshData);
}

void GeometryGenerator::CreateSphere(float radius, unsigned int sliceCount, unsigned int stackCount, MeshData& meshData)
{
	meshData.mVertices.clear();
	meshData.mIndices.clear();

	//Start at top vertex and go down

	Vertex topVertex(0.0f, +radius, 0.0f, 0.0f, +1.0f, 0.0f, 0.0f, 0.0f);
	Vertex bottomVertex(0.0f, -radius, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f);

	meshData.mVertices.push_back(topVertex);
	float phiStep	= M_PI / stackCount;
	float thetaStep = 2.0f * M_PI / sliceCount;

	for (unsigned int i = 1; i <= stackCount - 1; ++i)
	{
		float phi = i * phiStep;

		for (unsigned int j = 0; j <= sliceCount; ++j)
		{
			float theta = j *  thetaStep;

			Vertex v;

			v.mPosition[0] = radius * sinf(phi) * cosf(theta);
			v.mPosition[1] = radius * cosf(phi);
			v.mPosition[2] = radius * sinf(phi) * sinf(theta);

			NormalizeVector3(v.mPosition, v.mNormal);

			v.mUV[0] = theta / (M_PI * 2.0f);
			v.mUV[1] = phi / M_PI;

			meshData.mVertices.push_back(v);
		}
	}

	meshData.mVertices.push_back(bottomVertex);

	
	// Compute indices for top stack.  The top stack was written first to the vertex buffer
	// and connects the top pole to the first ring.

	for(unsigned int i = 1; i <= sliceCount; ++i)
	{
		meshData.mIndices.push_back(0);
		meshData.mIndices.push_back(i + 1);
		meshData.mIndices.push_back(i);
	}
	
	// Compute indices for inner stacks (not connected to poles).

	// Offset the indices to the index of the first vertex in the first ring.
	// This is just skipping the top pole vertex.
	int baseIndex = 1;
	int ringVertexCount = sliceCount + 1;
	for( unsigned int i = 0; i < stackCount - 2; ++i)
	{
		for(unsigned int j = 0; j < sliceCount; ++j)
		{
			meshData.mIndices.push_back(baseIndex + i * ringVertexCount + j);
			meshData.mIndices.push_back(baseIndex + i * ringVertexCount + j + 1);
			meshData.mIndices.push_back(baseIndex + (i + 1) * ringVertexCount + j);

			meshData.mIndices.push_back(baseIndex + (i + 1) * ringVertexCount + j);
			meshData.mIndices.push_back(baseIndex + i * ringVertexCount + j + 1);
			meshData.mIndices.push_back(baseIndex + (i + 1) * ringVertexCount + j + 1);
		}
	}

	// Compute indices for bottom stack.  The bottom stack was written last to the vertex buffer
	// and connects the bottom pole to the bottom ring.

	// South pole vertex was added last.
	int southPoleIndex = (unsigned int)meshData.mVertices.size() - 1;

	// Offset the indices to the index of the first vertex in the last ring.
	baseIndex = southPoleIndex - ringVertexCount;
	
	for(unsigned int i = 0; i < sliceCount; ++i)
	{
		meshData.mIndices.push_back(southPoleIndex);
		meshData.mIndices.push_back(baseIndex + i);
		meshData.mIndices.push_back(baseIndex + i + 1);
	}
}

void GeometryGenerator::CreateGrid(float width, float depth, unsigned int m, unsigned int n, MeshData& meshData)
{
	unsigned int vertexCount	= m * n;
	unsigned int faceCount		= (m - 1) * (n - 1) * 2;

	//Create the vertices
	float halfWidth = 0.5f * width;
	float halfDepth = 0.5f * depth;

	float dx = width / (n - 1.0f);
	float dz = depth / (m - 1.0f);

	float du = 1.0f / (n - 1.0f);
	float dv = 1.0f / (m - 1.0f);

	meshData.mVertices.resize(vertexCount);

	for (int i = 0; i < m; ++i)
	{
		float z = halfDepth - i * dz;
		for (int j = 0; j < n; ++j)
		{
			float x = -halfWidth + j * dx;

			meshData.mVertices[i * n + j].mPosition[0] = x;
			meshData.mVertices[i * n + j].mPosition[1] = 0.0f;
			meshData.mVertices[i * n + j].mPosition[2] = z;

			meshData.mVertices[i * n + j].mUV[0] = j *du;
			meshData.mVertices[i * n + j].mUV[1] = i * dv;
		}
	}

	// Create the indices.

	meshData.mIndices.resize(faceCount * 3); // 3 indices per face

	// Iterate over each quad and compute indices.
	unsigned int k = 0;
	for(int i = 0; i < m-1; ++i)
	{
		for(int j = 0; j < n-1; ++j)
		{
			meshData.mIndices[k]   = i * n + j;
			meshData.mIndices[k + 1] = i * n + j + 1;
			meshData.mIndices[k + 2] = (i + 1) * n + j;

			meshData.mIndices[k + 3] = (i + 1) * n + j;
			meshData.mIndices[k + 4] = i * n + j + 1;
			meshData.mIndices[k + 5] = (i + 1) * n + j + 1;

			k += 6; // next quad
		}
	}
}

void GeometryGenerator::CreateFromObjFile(std::string filePath, MeshData& meshData)
{
	std::ifstream file;
	file.open(filePath);

	if( !file.is_open() )
	{
		std::cout << "Could not open file \'" << filePath << "\'\n";
		return;
	}

	std::cout << "Loading mesh \'" << filePath << "\'... ";

	// Load obj file

	//std::vector<glm::vec3> positions;
	//std::vector<glm::vec3> normals;
	std::vector<float*> positions;
	std::vector<float*> normals;

	std::string line;
	std::stringstream stream;
	std::vector<std::string> tokens;
	std::string currentToken;
		
	int faceCount = 0;
	std::vector<std::string> faceTokens;
	std::stringstream faceStream;
	std::string currentFaceToken;

	std::vector<FaceDef> faces;
	FaceDef tempFace;

	//glm::vec3 tempVec;
	


	int curLine = 0; // For debugging
	while( std::getline(file, line) )
	{
		stream.clear();
		stream.str( line );
		tokens.clear();
		while(std::getline( stream, currentToken, ' ' ))
		{
			tokens.push_back( currentToken );
		}
		if( tokens.empty() )
			continue;

		// VERTEX
		if( tokens[0] == "v" )
		{
			if( tokens.size() >= 4 )
			{
				
				//tempVec = glm::vec3(0.0f, 0.0f, 0.0f);
				//tempVec.x = StringToFloat(tokens[1]);
				//tempVec.y = StringToFloat(tokens[2]);
				//tempVec.z = StringToFloat(tokens[3]);
				//positions.push_back(tempVec);


				float *tempVec = new float[3];
				tempVec[0] = StringToFloat(tokens[1]);
				tempVec[1] = StringToFloat(tokens[2]);
				tempVec[2] = StringToFloat(tokens[3]);

				positions.push_back( tempVec );

			}
			else
			{
				std::cout << "Bad vertex at line " << curLine << "\n";

				float *tempVec = new float[3];
				tempVec[0] = 0.0f;
				tempVec[1] = 0.0f;
				tempVec[2] = 0.0f;
				positions.push_back( tempVec );

				//positions.push_back( glm::vec3(0.0f, 0.0f, 0.0f) );
			}
		}
		// VERTEX NORMAL
		else if( tokens[0] == "vn") 
		{
			if( tokens.size() >= 4 )
			{
				//tempVec = glm::vec3(0.0f);
				//tempVec.x = StringToFloat(tokens[1]);
				//tempVec.y = StringToFloat(tokens[2]);
				//tempVec.z = StringToFloat(tokens[3]);
				//normals.push_back(tempVec);

				float *tempVec = new float[3];
				tempVec[0] = StringToFloat(tokens[1]);
				tempVec[1] = StringToFloat(tokens[2]);
				tempVec[2] = StringToFloat(tokens[3]);
				normals.push_back(tempVec);
			}
			else
			{
				std::cout << "Bad normal at line " << curLine << "\n";

				float *tempVec = new float[3];
				tempVec[0] = 0.0f;
				tempVec[1] = 0.0f;
				tempVec[2] = 0.0f;
				positions.push_back( tempVec );

				//normals.push_back( glm::vec3(0.0f, 0.0f, 0.0f) );
			}
		}
		// VERTEX TEXTURE COORDINATE
		else if( tokens[0] == "vt") 
		{
			std::cout << "UV are not implemented yet, but hey! Dont blame Jaoel!\n";
		}
		// FACE
		else if( tokens[0] == "f") 
		{
			faceCount++;
			if( tokens.size() == 4 ) // Only triangulated faces;
			{

				//Vertex 1
				faceStream.clear();
				faceStream.str(tokens[3]);
				faceTokens.clear();
				while(std::getline(faceStream, currentFaceToken, '/'))
				{
					faceTokens.push_back(currentFaceToken);
				}
				if(faceTokens.size() == 3)
				{
					tempFace.vertIndex[0] = StringToInt(faceTokens[0]) - 1;

					tempFace.uvIndex[0] = StringToInt(faceTokens[1]) - 1;

					tempFace.normalIndex[0] = StringToInt(faceTokens[2]) - 1;
				}
				
				

				//Vertex 2
				faceStream.clear();
				faceStream.str(tokens[2]);
				faceTokens.clear();
				while(std::getline(faceStream, currentFaceToken, '/'))
				{
					faceTokens.push_back(currentFaceToken);
				}
				if(faceTokens.size() == 3)
				{
					tempFace.vertIndex[1] = StringToInt(faceTokens[0]) - 1;

					tempFace.uvIndex[1] = StringToInt(faceTokens[1]) - 1;

					tempFace.normalIndex[1] = StringToInt(faceTokens[2]) - 1;
				}

				//Vertex 3
				faceStream.clear();
				faceStream.str(tokens[1]);
				faceTokens.clear();
				while(std::getline(faceStream, currentFaceToken, '/'))
				{
					faceTokens.push_back(currentFaceToken);
				}
				if(faceTokens.size() == 3)
				{
					tempFace.vertIndex[2] = StringToInt(faceTokens[0]) - 1;

					tempFace.uvIndex[2] = StringToInt(faceTokens[1]) - 1;

					tempFace.normalIndex[2] = StringToInt(faceTokens[2]) - 1;
				}
				
				faces.push_back(tempFace);

			}
			else
			{
				std::cout << "Mesh must be triangles, but hey! Dont blame Jaoel!\n";
			}


		}

		curLine++;
	}

	// Add available vertices to a list
	std::vector<Vertex> vertices;
	for( int i = 0; i < faceCount; i++ )
	{
		tempFace = faces[i];
		Vertex tempVertex;
		for(int e = 0; e < 3; e++)
		{
			//glm::vec3 p = positions[tempFace.vertIndex[e]];
			//glm::vec3 n = normals[tempFace.normalIndex[e]];
			//glm::vec2 u = glm::vec2(0.0f, 0.0f);
			//tempVertex = Vertex( p.x, p.y, p.z, n.x, n.y, n.z, u.x, u.y );
			//vertices.push_back(tempVertex);

			tempVertex = Vertex( 
				positions[tempFace.vertIndex[e]][0], 
				positions[tempFace.vertIndex[e]][1], 
				positions[tempFace.vertIndex[e]][2], 
				normals[tempFace.normalIndex[e]][0], 
				normals[tempFace.normalIndex[e]][1], 
				normals[tempFace.normalIndex[e]][2], 
				0.0f, 0.0f );
			vertices.push_back(tempVertex);
		}
	}

	// Fix duplicates and build index array
	for( int i = 0; i < vertices.size(); i++ )
	{
		bool isDuplicate = false;
		for( int e = 0; e < meshData.mVertices.size(); e++ )
		{
			if( vertices[i] == meshData.mVertices[e] )
			{
				isDuplicate = true;

				meshData.mIndices.push_back(e);

				break;
			}
		}
		if( !isDuplicate )
		{
			meshData.mIndices.push_back(meshData.mVertices.size());
			meshData.mVertices.push_back(vertices[i]);
		}
	}
	std::cout << "Done \n";
	for( int i = 0; i < positions.size(); i++ )
	{
		delete[] positions[i];
	}
	for( int i = 0; i < normals.size(); i++ )
	{
		delete[] normals[i];
	}
}

void GeometryGenerator::Subdivide(MeshData& meshData)
{
	MeshData inputCopy = meshData;

	meshData.mVertices.resize(0);
	meshData.mIndices.resize(0);

	/*       v1
	//       *
	//      / \
	//     /   \
	//  m0*-----*m1
	//   / \   / \
	//  /   \ /   \
	// *-----*-----*
	// v0    m2     v2
	*/

	unsigned int numTris = inputCopy.mIndices.size()/3;

	for (int i = 0; i < numTris; ++i)
	{
		Vertex v0 = inputCopy.mVertices[ inputCopy.mIndices[i * 3 + 0] ];
		Vertex v1 = inputCopy.mVertices[ inputCopy.mIndices[i * 3 + 1] ];
		Vertex v2 = inputCopy.mVertices[ inputCopy.mIndices[i * 3 + 2] ];

		//Generate midpoints

		Vertex m0;
		Vertex m1;
		Vertex m2;

		LerpVec3(v0.mPosition, v1.mPosition, 0.5f, m0.mPosition);
		LerpVec3(v1.mPosition, v2.mPosition, 0.5f, m1.mPosition);
		LerpVec3(v0.mPosition, v2.mPosition, 0.5f, m2.mPosition);

		LerpVec2(v0.mUV, v1.mUV, 0.5f, m0.mUV);
		LerpVec2(v1.mUV, v2.mUV, 0.5f, m1.mUV);
		LerpVec2(v0.mUV, m2.mUV, 0.5f, m2.mUV);

		LerpVec3(v0.mNormal, v1.mNormal, 0.5f, m0.mNormal);
		NormalizeVector3(m0.mNormal);

		LerpVec3(v1.mNormal, v2.mNormal, 0.5f, m1.mNormal);
		NormalizeVector3(m1.mNormal);

		LerpVec3(v0.mNormal, v2.mNormal, 0.5f, m2.mNormal);
		NormalizeVector3(m2.mNormal);

		// Add new geometry.
		meshData.mVertices.push_back(v0); // 0
		meshData.mVertices.push_back(v1); // 1
		meshData.mVertices.push_back(v2); // 2
		meshData.mVertices.push_back(m0); // 3
		meshData.mVertices.push_back(m1); // 4
		meshData.mVertices.push_back(m2); // 5
 
		meshData.mIndices.push_back(i*6+0);
		meshData.mIndices.push_back(i*6+3);
		meshData.mIndices.push_back(i*6+5);

		meshData.mIndices.push_back(i*6+3);
		meshData.mIndices.push_back(i*6+4);
		meshData.mIndices.push_back(i*6+5);

		meshData.mIndices.push_back(i*6+5);
		meshData.mIndices.push_back(i*6+4);
		meshData.mIndices.push_back(i*6+2);

		meshData.mIndices.push_back(i*6+3);
		meshData.mIndices.push_back(i*6+1);
		meshData.mIndices.push_back(i*6+4);
	}
}

void GeometryGenerator::NormalizeVector3(float* vec3)
{
	float length = sqrt ( (vec3[0] * vec3[0] + vec3[1] * vec3[1] + vec3[2] * vec3[2]) );

	if (length <= 0)
		return;

	vec3[0] = vec3[0] / length;
	vec3[1] = vec3[1] / length;
	vec3[2] = vec3[2] / length;
}

void GeometryGenerator::NormalizeVector3(float* inVec3, float* outVec3)
{
	float length = sqrt ( (inVec3[0] * inVec3[0] + inVec3[1] * inVec3[1] + inVec3[2] * inVec3[2]) );

	if (length <= 0)
		return;

	outVec3[0] = inVec3[0] / length;
	outVec3[1] = inVec3[1] / length;
	outVec3[2] = inVec3[2] / length;
}

void GeometryGenerator::LerpVec2(const float* vec1, const float* vec2, float t, float* outVec)
{
	outVec[0] = (1.0f - t) * vec1[0] + t * vec2[0];
	outVec[1] = (1.0f - t) * vec1[1] + t * vec2[1];
}

void GeometryGenerator::LerpVec3(const float* vec1, const float* vec2, float t, float* outVec)
{
	outVec[0] = (1.0f - t) * vec1[0] + t * vec2[0];
	outVec[1] = (1.0f - t) * vec1[1] + t * vec2[1];
	outVec[2] = (1.0f - t) * vec1[2] + t * vec2[2];
}
float GeometryGenerator::StringToFloat( std::string s )
{
	float ret = 0.0f;
	if( !s.empty() )
	{
		std::stringstream ss(s);
		ss >> ret;
	}
	return ret;
}
int GeometryGenerator::StringToInt( std::string s )
{
	int ret = 0;
	if( !s.empty() )
	{
		std::stringstream ss(s);
		ss >> ret;
	}
	return ret;
}
