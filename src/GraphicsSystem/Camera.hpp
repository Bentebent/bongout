#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "glm/glm.hpp"

class Camera
{
public:
	Camera();
	Camera( const Camera& cam );
	~Camera();

	void SetPosition( float x, float y, float z );
	void SetRotation( float x, float y, float z );
	void SetFov( float fov );

	glm::vec3 GetPosition();
	glm::vec3 GetRotation();

	void UpdateProjection( const float &width, const float &height, const float &near, const float &far );
	void UpdateView();
	void UpdateView( glm::vec3 &lookAt );
	void UpdateView( glm::vec3 &pos, glm::vec3 &lookAt );
	void GetViewMatrix( glm::mat4& view );
	void GetProjectionMatrix( glm::mat4& proj );

private:
	void LookAt( glm::vec3 &target );

	//float m_positionX, m_positionY, m_positionZ;
	//float m_rotationX, m_rotationY, m_rotationZ;

	glm::vec3 position;

	glm::vec3 forward;
	glm::vec3 up;

	glm::mat4 m_viewMatrix;
	glm::mat4 m_projMatrix;
	float m_fov;
};

#endif