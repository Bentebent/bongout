#include "LoadInterface.hpp"


void LoadInterface::LoadContent( unsigned int mode )
{
/* Menu Entities */
    // Main Menu
    this->LoadObj( "assets/mesh/menu_text/BongOutText.obj",		mode, MeshType::BongOutText );
    this->LoadObj( "assets/mesh/menu_text/StartGameText.obj",	mode, MeshType::StartGameText );
    this->LoadObj( "assets/mesh/menu_text/QuitText.obj",		mode, MeshType::QuitText );

    // Endgame Menu
    this->LoadObj( "assets/mesh/menu_text/GameOverText.obj",	mode, MeshType::GameOverText );
    this->LoadObj( "assets/mesh/menu_text/Player1WinsText.obj",	mode, MeshType::Player1WinsText );
    this->LoadObj( "assets/mesh/menu_text/Player2WinsText.obj",	mode, MeshType::Player2WinsText );
    this->LoadObj( "assets/mesh/menu_text/RestartGameText.obj",	mode, MeshType::RestartGameText );
    this->LoadObj( "assets/mesh/menu_text/ExitGameText.obj",	mode, MeshType::ExitGameText );
    this->LoadObj( "assets/mesh/menu_text/MainMenuText.obj",	mode, MeshType::MainMenuText );
    
	//Ready menu
	this->LoadObj( "assets/mesh/menu_text/PrepareToFightText.obj", mode, MeshType::PrepareToFightText);

/* Numbers */
    this->LoadObj( "assets/mesh/primitives/Num0.obj",	mode, MeshType::Num0 );
    this->LoadObj( "assets/mesh/primitives/Num1.obj",	mode, MeshType::Num1 );
    this->LoadObj( "assets/mesh/primitives/Num2.obj",	mode, MeshType::Num2 );
    this->LoadObj( "assets/mesh/primitives/Num3.obj",	mode, MeshType::Num3 );
    this->LoadObj( "assets/mesh/primitives/Num4.obj",	mode, MeshType::Num4 );
    this->LoadObj( "assets/mesh/primitives/Num5.obj",	mode, MeshType::Num5 );
    this->LoadObj( "assets/mesh/primitives/Num6.obj",	mode, MeshType::Num6 );
    this->LoadObj( "assets/mesh/primitives/Num7.obj",	mode, MeshType::Num7 );
    this->LoadObj( "assets/mesh/primitives/Num8.obj",	mode, MeshType::Num8 );
    this->LoadObj( "assets/mesh/primitives/Num9.obj",	mode, MeshType::Num9 );

/* Game Entities */
    this->LoadObj( "assets/mesh/primitives/cube.obj",						mode, MeshType::Paddle );
    this->LoadObj( "assets/mesh/primitives/sphere.obj",						mode, MeshType::Ball );
    this->LoadObj( "assets/mesh/primitives/cube.obj",						mode, MeshType::Brick );
    this->LoadObj( "assets/mesh/game_entities/PlayerzoneWall.obj",			mode, MeshType::PlayerWall );
    this->LoadObj( "assets/mesh/game_entities/PlayerzoneFloor.obj",			mode, MeshType::PlayerFloor );
    this->LoadObj( "assets/mesh/game_entities/PlayerzoneThinWall.obj",		mode, MeshType::FieldThinWall );
    this->LoadObj( "assets/mesh/game_entities/PlayerzoneThinDelimiter.obj",	mode, MeshType::FieldThinDelimiter );
    this->LoadObj( "assets/mesh/game_entities/ComplexBackground.obj",		mode, MeshType::ComplexBackground );
}
