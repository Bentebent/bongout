#include <iostream>
#include <ctime>
#include <chrono>
#include <thread>

#include <sstream>


#include <GLFW/glew.h>
#include <GLFWInclude.hpp>
#include <InitInput.hpp>

#include <Utility/Physics.hpp>
#include <ComponentFramework/EntityFactory.hpp>

#include <ComponentFramework/World.hpp>
#include <ComponentFramework/SystemHandler.hpp>
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/Components.hpp>

#include <Utility/ErrorHandler.hpp>
#include <GraphicsSystem/OpenGL/GLgraphics.hpp>
#include <GraphicsSystem/GraphicsMaterial.hpp>

#include <Audio/AudioCore.hpp>

void APIENTRY glErrorCallback( GLenum _source, GLenum _type, GLuint _id, GLenum _severity, GLsizei _length, const char* _message, void* _userParam) 
{
    char* source = "";

    if( _source == GL_DEBUG_SOURCE_API_ARB )
        source = "The GL";
    else if( _source == GL_DEBUG_SOURCE_SHADER_COMPILER_ARB )
        source = "The GLSL Shader compiler";
    else if( _source == GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB )
        source = "The source window system arb";

	std::cout << "OPENGL_ERROR: id:" << source << ":" << _type << ":"  << _message << std::endl;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	GLgraphics::Get().SetNewWindowLimits( width, height );
}

int main(int argc, char** argv)
{
    World* world;
	GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
    {
		std::cout << "Unable to init glfw." << std::endl;
        return -1;
    }

	/* Set context version to forward compatible 4.2 mode */
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE );

	int windowX = 1280;
	int windowY = 720;


    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(windowX, windowY, "BongOut", nullptr, nullptr);
    if (!window)
    {
        glfwTerminate();
		std::cout << "Unable to create window" << std::endl;
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

	std::cout << glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR) << "." << glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR) << std::endl;


	// if 1 then limits system to max 60 fps!
	glfwSwapInterval( 0 ); 
	glClearColor( 0, 0, 0, 0 );

	// init glew
	glewExperimental = true;
	GLenum err = glewInit();
	if ( GLEW_OK != err ) {
		ErrorHandler::Get().Throw( "error initialising GLEW..." );
	}

	// assign callback functions...
	glDebugMessageCallbackARB(glErrorCallback, NULL);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);


	glEnable(GL_DEPTH_TEST); 
	glDepthFunc(GL_LESS); 
	glEnable(GL_CULL_FACE); 

	glCullFace(GL_BACK); 
	glFrontFace(GL_CW); 
	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE );

	InputDevices InputDevices = InitOIS(window);
	world = new World(&InputDevices);

	GLgraphics::Get().Init( windowX, windowY, world->mSystemHandler->mParticleSystem );
	GLgraphics::Get().LoadContent( GL_TRIANGLES );

	GLgraphics::Get().SetAmbienceColor( glm::vec3( 1,1,1 ) );
	GLgraphics::Get().SetAmbienceLevel( 0.2 );

    

	world->mCamera.SetFov( 45 );
	glm::vec3 lookAt( 0, 0, 0.05f);
	world->mCamera.UpdateView( lookAt );
	world->mCamera.UpdateProjection( windowX, windowY, 1, 500 );

	auto currentTime = std::chrono::high_resolution_clock::now();
	const float timeIntergrationStep = 1.0f/60.0f;
	
	int nFramesFPS = 0;

    /* Loop until the user closes the window */
    while(!glfwWindowShouldClose(window))
    {	
		auto newTime = std::chrono::high_resolution_clock::now();
		float frameTime = std::chrono::duration_cast<std::chrono::milliseconds>(newTime - currentTime).count() / 1000.0f;

		auto startTime = std::chrono::high_resolution_clock::now();

		/* Render here */
		GLgraphics::Get().Update( *world );			
		
		while(frameTime > 0.0f)
		{
			world->mDelta = std::min(frameTime, timeIntergrationStep);
			world->mElapsedTime += world->mDelta;
			world->mSystemHandler->Update(*world);
			frameTime -= world->mDelta;		
		}
		
		auto endTime = std::chrono::high_resolution_clock::now();		

		/* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
		std::this_thread::sleep_for(std::chrono::milliseconds(std::max(0LL, static_cast<long long>((timeIntergrationStep * 1000LL) - (std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count())))));
		
		if(world->mCurrentState == GameState::EXIT_PROGRAM)
		{
			glfwDestroyWindow(window);
		}

		++nFramesFPS;
		if(nFramesFPS > 30)
		{
			//std::cout << "FPS:" <<  1000.0 / static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(newTime - currentTime).count()) << std::endl;
			nFramesFPS = 0;
		}
		currentTime = newTime;
    }
    
    delete world;

    glfwTerminate();
    return 0;
}
