#ifndef SRC_INIT_INPUT_HPP
#define SRC_INIT_INPUT_HPP

#include <GLFWInclude.hpp>
#include <InputDevices.hpp>

#include <iostream>
#include <vector>
#include <sstream>

#if defined OIS_WIN32_PLATFORM
#  define WIN32_LEAN_AND_MEAN
#  include <Windows.h>
#  ifdef min
#    undef min
#  endif
//////////////////////////////////////////////////////////////////////
////////////////////////////////////Needed Linux Headers//////////////
#elif defined OIS_LINUX_PLATFORM
#  include <X11/Xlib.h>
   void checkX11Events();
#endif

const char *g_DeviceType[6] = {"OISUnknown", "OISKeyboard", "OISMouse", "OISJoyStick", "OISTablet", "OISOther"};


InputDevices InitOIS(GLFWwindow* window)
{
	InputDevices InputDevices = {0};
	OIS::ParamList pl;
#if defined OIS_WIN32_PLATFORM

	std::ostringstream wnd;
	wnd << (size_t)glfwGetWin32Window(window);

	pl.insert(std::make_pair( std::string("WINDOW"), wnd.str() ));
	
	//Default mode is foreground exclusive..but, we want to show mouse - so nonexclusive
	pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
	pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
#elif defined OIS_LINUX_PLATFORM

    Window xWin;
    Display *xDisp;
    /*
	//Connects to default X window
	if( !(xDisp = XOpenDisplay(0)) )
		OIS_EXCEPT(E_General, "Error opening X!");
	//Create a window
	xWin = XCreateSimpleWindow(xDisp,DefaultRootWindow(xDisp), 0,0, 100,100, 0, 0, 0);

	//bind our connection to that window
	XMapWindow(xDisp, xWin);
    */

	std::ostringstream wnd;

    xDisp = glfwGetX11Display();
	xWin = glfwGetX11Window( window );

    wnd << xWin;
	//Select what events we want to listen to locally
	XSelectInput(xDisp, xWin, StructureNotifyMask);
	XEvent evtent;
	do
	{
		XNextEvent(xDisp, &evtent);
	} while(evtent.type != MapNotify);

	pl.insert(std::make_pair(std::string("WINDOW"), wnd.str()));

	//For this demo, show mouse and do not grab (confine to window)
	pl.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
	pl.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
	pl.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
#endif

	//This never returns null.. it will raise an exception on errors
	InputDevices.g_InputManager = OIS::InputManager::createInputSystem(pl);

	//Lets enable all addons that were compiled in:
	InputDevices.g_InputManager->enableAddOnFactory(OIS::InputManager::AddOn_All);


	//Print debugging information
	unsigned int v = InputDevices.g_InputManager->getVersionNumber();
	std::cout << "OIS Version: " << (v>>16 ) << "." << ((v>>8) & 0x000000FF) << "." << (v & 0x000000FF)
		<< "\nRelease Name: " << InputDevices.g_InputManager->getVersionName()
		<< "\nManager: " << InputDevices.g_InputManager->inputSystemName()
		<< "\nTotal Keyboards: " << InputDevices.g_InputManager->getNumberOfDevices(OIS::OISKeyboard)
		<< "\nTotal Mice: " << InputDevices.g_InputManager->getNumberOfDevices(OIS::OISMouse)
		<< "\nTotal JoySticks: " << InputDevices.g_InputManager->getNumberOfDevices(OIS::OISJoyStick);


	//List all devices
	OIS::DeviceList list = InputDevices.g_InputManager->listFreeDevices();
	for( OIS::DeviceList::iterator i = list.begin(); i != list.end(); ++i )
		std::cout << "\n\tDevice: " << g_DeviceType[i->first] << " Vendor: " << i->second;

	InputDevices.g_kb = (OIS::Keyboard*)InputDevices.g_InputManager->createInputObject( OIS::OISKeyboard, true );
	InputDevices.g_m = (OIS::Mouse*)InputDevices.g_InputManager->createInputObject( OIS::OISMouse, true );


	try
	{
		//This demo uses at most 4 joysticks - use old way to create (i.e. disregard vendor)
		int numSticks = std::min(InputDevices.g_InputManager->getNumberOfDevices(OIS::OISJoyStick), 4);
		for( int i = 0; i < numSticks; ++i )
		{
			InputDevices.g_joys[i] = (OIS::JoyStick*)InputDevices.g_InputManager->createInputObject( OIS::OISJoyStick, true );
			std::cout << "\n\nCreating Joystick " << (i + 1)
				<< "\n\tAxes: " << InputDevices.g_joys[i]->getNumberOfComponents(OIS::OIS_Axis)
				<< "\n\tSliders: " << InputDevices.g_joys[i]->getNumberOfComponents(OIS::OIS_Slider)
				<< "\n\tPOV/HATs: " << InputDevices.g_joys[i]->getNumberOfComponents(OIS::OIS_POV)
				<< "\n\tButtons: " << InputDevices.g_joys[i]->getNumberOfComponents(OIS::OIS_Button)
				<< "\n\tVector3: " << InputDevices.g_joys[i]->getNumberOfComponents(OIS::OIS_Vector3);
		}
	}
	catch(OIS::Exception &ex)
	{
		std::cout << "\nException raised on joystick creation: " << ex.eText << std::endl;
	}

	return InputDevices;
}


#endif
