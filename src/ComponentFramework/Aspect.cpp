#include "Aspect.hpp"
#include "EntityHandler.hpp"
#include "Components.hpp"

Aspect::Aspect( m_int inclusive, m_int exclusive )
{
	this->inclusive = inclusive;
	this->exclusive = exclusive;
}

Aspect::Aspect()
{
    this->inclusive = 0UL;
    this->exclusive = 0UL;
}

bool Aspect::Matches( m_int aspect )
{
    return ((inclusive & aspect) == inclusive) && ((exclusive & aspect) == 0);
}

void Aspect::AddInclusiveAspect( ComponentID component )
{
    this->inclusive |= (1UL << component); 
}

void Aspect::RemoveInclusiveAspect( ComponentID component )
{
    this->inclusive = this->inclusive & ~(1UL << component);
}

void Aspect::AddExclusiveAspect( ComponentID component )
{
    this->exclusive |= (1UL << component);
}

void Aspect::RemoveExclusiveAspect( ComponentID component )
{
    this->exclusive = this->inclusive & ~(1UL << component);
}
