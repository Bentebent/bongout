#ifndef SRC_ASPECT_H
#define SRC_ASPECT_H

#include "SharedAspectType.hpp"
#include "Components.hpp"

enum ComponentID;

class Aspect
{
private:
    Aspect( m_int inclusive, m_int exclusive );
    m_int inclusive;
    m_int exclusive;

public:
    Aspect();
    bool Matches( m_int aspect );

    void AddInclusiveAspect( ComponentID component );
    void RemoveInclusiveAspect( ComponentID component );
    
    void AddExclusiveAspect( ComponentID component );
    void RemoveExclusiveAspect( ComponentID component );
};

#endif
