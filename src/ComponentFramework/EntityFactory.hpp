#ifndef SRC_ENITITY_FACTORY_HPP
#define SRC_ENTITITY_FACTORY_HPP

#include <ComponentFramework/EntityHandler.hpp>

template<typename Handler, typename A, typename... Args> void Call(unsigned int ID, Handler& h, A a, Args... args)
{
	h.AddComponent(ID, a.ID, &a);
	Call<Handler, Args...>(ID, h, args...);
}

template<typename Handler, typename A> void Call(unsigned int ID, Handler& h, A a)
{
	h.AddComponent(ID, a.ID, &a);
}

template<typename ...Args> unsigned int CreateEntity(EntityHandler* h, Args... args)
{
	unsigned int EntityID = h->CreateEntity();

	Call<EntityHandler, Args...>(EntityID, (*h), args...);

	return EntityID;
}

#endif