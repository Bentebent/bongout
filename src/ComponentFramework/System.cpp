#include "System.hpp"
#include <ComponentFramework/World.hpp>
#include <ComponentFramework/EntityHandler.hpp>

System::System()
{

}

System::~System()
{

}

std::vector<unsigned int> System::getEntities(World& world)
{
	return std::move( world.mEntityHandler->GetEntities( Asp ) );
}

void System::onComponentAdd(unsigned int entityId, ComponentID componentId, Component* cmp)
{

}

void System::onComponentRemove(unsigned int entityId, ComponentID componentId, Component* cmp)
{

}

void System::onEntityAspectChange(unsigned int entityId, m_int AspectMask)
{
	for(auto it = EntityList.begin(); it != EntityList.end(); ++it)
	{
		if((*it) == entityId)
		{
			if(!this->Asp.Matches(AspectMask))
			{
				EntityList.erase(it);
				break;
			}
		}
	}
}
