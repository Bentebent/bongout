#include "EntityHandler.hpp"

#include <assert.h>
#include <iostream>

#include <ComponentFramework/SystemHandler.hpp>
#include <ComponentFramework/World.hpp>

#include "ChunkVector.hpp"
#include "Aspect.hpp"

EntityHandler::EntityHandler(World* world)
{
	this->world = world;

    componentSizes.push_back( sizeof( WorldPositionComponent ) );
	componentSizes.push_back( sizeof( ScaleComponent ) );
    componentSizes.push_back( sizeof( ScreenPositionComponent ) );
	componentSizes.push_back( sizeof( VelocityComponent ) );
	componentSizes.push_back( sizeof( AABBComponent ) );

    componentSizes.push_back( sizeof( TestComponent ) );
	componentSizes.push_back( sizeof( PlayerComponent ) );
	componentSizes.push_back( sizeof( InputComponent ) );
	componentSizes.push_back( sizeof( MenuComponent ) );
	componentSizes.push_back( sizeof( MainMenuComponent ) ); 

	componentSizes.push_back( sizeof( ReadyMenuComponent ) );
	componentSizes.push_back( sizeof( RoundOverMenuComponent ) );
	componentSizes.push_back( sizeof( EndMenuComponent ) );
	componentSizes.push_back( sizeof( PhysicsComponent ) );
	componentSizes.push_back( sizeof( CirclePhysicsComponent ) );

	componentSizes.push_back( sizeof( GraphicsComponent ) );
	componentSizes.push_back( sizeof( LightComponent ) );
	componentSizes.push_back( sizeof( BrickComponent ) );
	componentSizes.push_back( sizeof( RemovableComponent ) );
	componentSizes.push_back( sizeof( BackgroundComponent ) );
	
	componentSizes.push_back( sizeof( AudioSourceComponent ) );
	componentSizes.push_back( sizeof( AudioWorldPanningComponent ) );
	componentSizes.push_back( sizeof( AudioDropComponent ) );
	componentSizes.push_back( sizeof( LifeComponent ) );
	componentSizes.push_back( sizeof( EmitterComponent ) );
	
	componentSizes.push_back ( sizeof( TimerComponent ) );

    //Check to see if someone missed adding components into the aforementioned arrays
    assert( COMPONENT_COUNT == componentSizes.size() );

    for( int i = 0; i < COMPONENT_COUNT; i++ )
    {
        components.push_back( new ChunkVector( componentSizes[i] ) );
        removedComponents.push_back( new std::vector<unsigned int> );
        entities.push_back( new std::vector<int> );
    }

}

EntityHandler::~EntityHandler()
{
    for( auto it: components )
        delete it;
}

unsigned int EntityHandler::CreateEntity()
{
	unsigned int entityId;

	if( this->removedIndices.size() > 0 )
	{
		entityId = this->removedIndices.back();
		this->removedIndices.pop_back();
    
        this->entityAspects[entityId] = 0UL; 
	}
	else
	{
		entityId = entityAspects.size();

		for( auto it: entities )
			it->push_back( -1 );

        this->entityAspects.push_back( 0UL );
	}

	return entityId;
}


void EntityHandler::RemoveEntity( unsigned int entityId )
{
	this->removedIndices.push_back( entityId );

	for( auto it: entities )
		(*it)[entityId] = -1;

    this->entityAspects[entityId] = 0UL;
}

void EntityHandler::AddComponent( unsigned int entityId, ComponentID type, Component* Component)
{
    int componentId;
    if( removedComponents[type]->size() > 0 )
    {
        componentId = removedComponents[type]->back();
        removedComponents[type]->pop_back(); 

        components[type]->set( componentId, Component);
    } 
    else
    {
        componentId = components[type]->size();
        
        components[type]->push_back( Component );
    }

    (*entities[type])[entityId] = componentId;

    this->entityAspects[entityId] |= this->GetComponentAspect( type );
	this->world->mSystemHandler->onEntityAspectChange(entityId, this->entityAspects[entityId]);
	this->world->mSystemHandler->onComponentAdd(entityId, type, Component, this->entityAspects[entityId]);
}
/*
void EntityHandler::AddComponent( unsigned int entityId, ComponentID type )
{
    int componentId;
    if( removedComponents[type]->size() > 0 )
    {
        componentId = removedComponents[type]->back();
        removedComponents[type]->pop_back(); 

        components[type]->set( componentId, componentDefaults[type] );
    } 
    else
    {
        componentId = components[type]->size();
        
        components[type]->push_back( componentDefaults[type] );
    }

    (*entities[type])[entityId] = componentId;

    this->entityAspects[entityId] |= this->GetComponentAspect( type );
}
*/

void EntityHandler::RemoveComponent( unsigned int entityId, ComponentID type )
{
    int componentId = (*entities[type])[entityId];
    if( componentId >= 0 )
    {
        (*entities[type])[entityId] = -1;

		this->world->mSystemHandler->onComponentRemove(entityId, type, this->GetComponent(entityId, type), this->entityAspects[entityId]);
        removedComponents[type]->push_back( componentId );
        this->entityAspects[entityId] = this->entityAspects[entityId] & ~(this->GetComponentAspect(type));
		this->world->mSystemHandler->onEntityAspectChange(entityId, this->entityAspects[entityId]);
    }
    else
    {
        std::cout << "Given entity index doesn't have any component of given type." << componentId << std::endl;
    }
}

Component* EntityHandler::GetComponent( unsigned int entity_id, ComponentID type )
{
    int component_id = (*entities[type])[entity_id];
    
    if( component_id >= 0 )
    {
        return static_cast<Component*>(components[type]->get( component_id ));
    }   

    return nullptr;
}

std::vector<unsigned int> EntityHandler::GetEntities( Aspect &aspect )
{
    std::vector<unsigned int> returnVec;

    for( unsigned int i = 0; i < this->entityAspects.size(); i++ )
    {
        if( aspect.Matches( this->entityAspects[i] ) && this->entityAspects[i] != 0UL )
        {
            returnVec.push_back( i );
        }
    }

    return std::move(returnVec);
}

m_int EntityHandler::GetEntityAspect( unsigned int entity_id )
{
    return this->entityAspects[entity_id]; 
}

m_int EntityHandler::GetComponentAspect( ComponentID component )
{
    return 1UL << component;
}
