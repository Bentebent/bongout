#ifndef SRC_CHUNKVECTOR_H
#define SRC_CHUNKVECTOR_H

#include <vector> 
#include <string.h>

class ChunkVector
{
public:
    ChunkVector( int bytes );
    ~ChunkVector();

    void push_back( void * source );
    void set( int index, void * source );
    void* get( int index );
    int size();
    
private:
    std::vector<unsigned char> data;
    int chunk_size;
    int byte_size;
};
#endif
