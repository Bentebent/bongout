#include "ExampleSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <iostream>

ExampleSystem::ExampleSystem()
{
    Asp.AddInclusiveAspect( WORLD_POSITION_COMPONENT );
    Asp.AddInclusiveAspect( SCREEN_POSITION_COMPONENT );

    Asp.AddExclusiveAspect( TEST_COMPONENT );
}
void ExampleSystem::Update(World &world)
{
	std::vector<unsigned int> entities = this->getEntities( world );

    //std::cout << "ExampleSystem.cpp:";
    for( auto it : entities )
    {
       // std::cout << it << " ";
        
        ScreenPositionComponent* spc = (ScreenPositionComponent*)world.mEntityHandler->GetComponent( it, SCREEN_POSITION_COMPONENT );
        
        //std::cout << ":" << spc->y << ". ";
        spc->y = spc->y + it + 1;
    } 
    //std::cout << std::endl;

}

