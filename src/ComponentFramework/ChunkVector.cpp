#include "ChunkVector.hpp"

ChunkVector::ChunkVector( int bytes )
{
    this->chunk_size = bytes / sizeof( unsigned char );
    this->byte_size = bytes;
}

ChunkVector::~ChunkVector()
{

}

void ChunkVector::push_back( void * source )
{
    for( int i = 0; i < chunk_size; i++ )
    {
        this->data.push_back(0);
    }

    memcpy( &data[data.size() - chunk_size], source, byte_size );     
}

void ChunkVector::set( int index, void * source )
{
    memcpy( &data[index*chunk_size], source, byte_size );     
}

int ChunkVector::size()
{
    return data.size()/chunk_size;
}

void* ChunkVector::get( int index )
{
    return &data[index*chunk_size];
}
