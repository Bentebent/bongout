#ifndef SRC_EXAMPLESYSTEM_H
#define SRC_EXAMPLESYSTEM_H

#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Aspect.hpp>

class ExampleSystem : public System 
{
    public:
    ExampleSystem();
	virtual void Update(World &world) override;
};
#endif
