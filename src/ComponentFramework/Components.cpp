#include "Components.hpp"

WorldPositionComponent::WorldPositionComponent()
{
    x=0.0f;y=0.0f;z=0.0f;
}

WorldPositionComponent::WorldPositionComponent(float x, float y, float z): x(x), y(y), z(z)
{
}

ScaleComponent::ScaleComponent(): x(1.0f), y(1.0f), z(1.0f)
{

}

ScaleComponent::ScaleComponent(float x, float y, float z): x(x), y(y), z(z)
{
	
}

ScreenPositionComponent::ScreenPositionComponent()
{
    x=0;y=0;
}

ScreenPositionComponent::ScreenPositionComponent(int x, int y): x(x), y(y)
{

}

VelocityComponent::VelocityComponent(): x(0.0f), y(0.0f)
{
	
}

VelocityComponent::VelocityComponent(float x, float y): x(x), y(y)
{

}

AABBComponent::AABBComponent() : aabb()
{

}

AABBComponent::AABBComponent(Vector2 size) : aabb(Vector2(0.0f, 0.0f), size)
{}

AABBComponent::AABBComponent(Vector2 topLeft, Vector2 bottomRight) : aabb(topLeft, bottomRight)
{}

CirclePhysicsComponent::CirclePhysicsComponent()
{
	radius = 0.0f;
}

CirclePhysicsComponent::CirclePhysicsComponent(float radius): radius(radius)
{

}

PlayerComponent::PlayerComponent()
{

}

PlayerComponent::PlayerComponent(int id, int score): playerID(id), score(score)
{
	
}

InputComponent::InputComponent()
{

}

InputComponent::InputComponent(int inputDevId, int kbdUp, int kbdDown) :inputDeviceID(inputDevId), keyBindUp(kbdUp), keyBindDown(kbdDown)
{

}

PhysicsComponent::PhysicsComponent()
{
	hasCollided = false;
}
GraphicsComponent::GraphicsComponent()
{
	meshType = MeshType::NoMesh;
	renderMe = true;
}

GraphicsComponent::GraphicsComponent(MeshType type)
{
	meshType = type;
	renderMe = true;
}

GraphicsComponent::GraphicsComponent( MeshType type, MeshMaterial material )
{
	meshType = type;
	meshMaterial = material;
	renderMe = true;
}

LightComponent::LightComponent()
{
	lightType = LightType::NoLight;
}

LightComponent::LightComponent( LightType type_, LightMaterial material )
{
	lightType = type_;
	lightMaterial = material;
}

BrickComponent::BrickComponent()
{
	OwnerID = 0;
}

BrickComponent::BrickComponent(unsigned int owner):OwnerID(owner)
{
}

MenuComponent::MenuComponent()
{
	defaultPosX = 0.0f;
	defaultPosY = 0.0f;
	defaultPosZ = 0.0f;
	defaultScale = 1.0f;
	popN = 10.0f;
	popW = 3.0f;
}
MenuComponent::MenuComponent( float x, float y, float z, float scale, float N, float W, float initZ )
{
	defaultPosX = x;
	defaultPosY = y;
	defaultPosZ = z;
	defaultScale = scale;
	popN = N;
	popW = W;
	initialZ = initZ;
}

MainMenuComponent::MainMenuComponent()
{
	id = -1;
}

MainMenuComponent::MainMenuComponent(int id): id(id)
{

}

EndMenuComponent::EndMenuComponent()
{
	id = -1;
}

EndMenuComponent::EndMenuComponent(int id): id(id)
{

}

RoundOverMenuComponent::RoundOverMenuComponent()
{
	id = -1;
}

RoundOverMenuComponent::RoundOverMenuComponent(int id): id(id)
{

}

ReadyMenuComponent::ReadyMenuComponent()
{
	id = -1;
	stayTimer = 0;
}

ReadyMenuComponent::ReadyMenuComponent(int id, float stayTimer) : id(id), stayTimer(stayTimer)
{
}

RemovableComponent::RemovableComponent(): removeOn(ON_NONE)
{

}

RemovableComponent::RemovableComponent(RemoveOn On): removeOn(On)
{

}

AudioSourceComponent::AudioSourceComponent( int buffer )
{
    playing = true;
    looping = true;
    panning = 0.0f;
    timer = 0;
    amplitude = 0.5f;    
    this->buffer = buffer;
}

AudioSourceComponent::AudioSourceComponent( int buffer, bool play, bool loop )
{
    playing = play;
    looping = loop;
    panning = 0.0f;
    timer = 0;
    amplitude = 0.3f;    
    this->buffer = buffer;
}

AudioSourceComponent::AudioSourceComponent( int buffer, bool play, bool loop, long unsigned int t )
{
    playing = play;
    looping = loop;
    panning = 0.0f;
    timer = t;
    amplitude = 0.3f;    
    this->buffer = buffer;
}

AudioWorldPanningComponent::AudioWorldPanningComponent( float center, float width ) 
{
    this->x_center = center;
    this->x_width = width; 
}

AudioDropComponent::AudioDropComponent( int buffer, float center, float width )
{
    this->buffer = buffer;
    this->center = center;
    this->width = width;
}

LifeComponent::LifeComponent()
{
	lives = 1;
}

LifeComponent::LifeComponent(int lives): lives(lives)
{

}

BackgroundComponent::BackgroundComponent()
{
}

#ifndef WIN32
	const ComponentID EmitterComponent::ID;
#endif
EmitterComponent::EmitterComponent(): emitterType(DEFAULT_EMITTER)
{

}

EmitterComponent::EmitterComponent(EmitterType type, int particlesPerSec, float offset_x, float offset_y, float offset_z ) 
	: emitterType(type), ParticlesPerSecond(particlesPerSec), Offset_x(offset_x), Offset_y(offset_y),  Offset_z(offset_z)
{

}

TimerComponent::TimerComponent()
	: timerType(NO_TIMER), Time(0.0f), UserData(nullptr)
{

}

TimerComponent::TimerComponent(TimerType type, float time, void* userData)
	:timerType(type), Time(time), UserData(userData)
{

}

