#ifndef SYSTEMHANDLER_HPP
#define SYSTEMHANDLER_HPP

#include <vector>
#include <ComponentFramework/Components.hpp>
#include <ComponentFramework/Aspect.hpp>

class System;
class World;
class ParticleSystem;
struct InputDevices;

class SystemHandler
{
public:
	SystemHandler(InputDevices* InputDevices);
	~SystemHandler();

	void Update(World &world);
	void onComponentAdd(unsigned int entityId, ComponentID componentId, Component* cmp, m_int AspectMask);
	void onComponentRemove(unsigned int entityId, ComponentID componentId, Component* cmp, m_int AspectMask);
	void onEntityAspectChange(unsigned int entityId, m_int AspectMask);

	ParticleSystem* mParticleSystem;

private:
	std::vector<System*> mSystems;
};
#endif
