#ifndef SRC_PARTICLE_COLLISION_SYSTEM_HPP
#define SRC_PARTICLE_COLLISION_SYSTEM_HPP

#include <ComponentFramework/System.hpp>


class ParticleCollisionSystem : public System
{
public:
	ParticleCollisionSystem();
	virtual void Update(World& world) override;

};


#endif