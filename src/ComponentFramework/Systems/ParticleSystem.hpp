#ifndef SRC_PARTICLE_SYSTEM_HPP
#define SRC_PARTICLE_SYSTEM_HPP

#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Aspect.hpp>
#include <GraphicsSystem/OpenGL/GLShader.hpp>

#ifdef OPENGL
#include <GLFW/glew.h>
#endif

#define MAX_PARTICLES 100000

class ParticleSystem : public System
{
public:
	ParticleSystem();
	virtual void Update(World& world) override;

	void Render(World& world);

private:
	struct Particle
	{
		float x;
		float y;
		float z;
		float scale;
		float life;		
	};

	struct ParticleSpeed
	{
		float x_vel;
		float y_vel;
		float z_vel;
	};

	struct ParticleAllocation
	{
		int StartIndex;
		int Allocated;
		int Size;
		unsigned int entityOwner;
	};

	Particle ParticleData[EmitterComponent::EmitterType::NUMBER_OF_EMITTERS][MAX_PARTICLES];
	ParticleSpeed ParticleSpeeds[EmitterComponent::EmitterType::NUMBER_OF_EMITTERS][MAX_PARTICLES];
	int NextParticle[EmitterComponent::EmitterType::NUMBER_OF_EMITTERS];

#ifdef OPENGL
	GLuint ParticleVertexAttributeObjects[EmitterComponent::EmitterType::NUMBER_OF_EMITTERS];
	GLuint ParticleBuffers[EmitterComponent::EmitterType::NUMBER_OF_EMITTERS];	
	GLShader ParticleShaders[EmitterComponent::EmitterType::NUMBER_OF_EMITTERS];
	GLuint UniformLocations[2][EmitterComponent::EmitterType::NUMBER_OF_EMITTERS]; 
#endif
};

#endif
