#ifndef BACKGROUNDLIGHTSYSTEM_HPP
#define BACKGROUNDLIGHTSYSTEM_HPP

#include <Utility/Physics.hpp>
#include <ComponentFramework/System.hpp>

class BackgroundLightSystem : public System
{
public:
	BackgroundLightSystem();
	virtual void Update(World& world) override;

private:

};

#endif