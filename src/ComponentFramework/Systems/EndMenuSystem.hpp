#ifndef END_MENU_SYSTEM_H
#define END_MENU_SYSTEM_H

//#include <InputDevices.hpp>
#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Systems/BaseMenuSystem.hpp>

struct InputDevices;

enum EndMenuEntities
{
	END_MENU_RESTART = 0,
	END_MENU_MAIN_MENU,
	END_MENU_EXIT_GAME,
	END_MENU_SIZE
};

class EndMenuSystem : public BaseMenuSystem
{
public:
	EndMenuSystem(InputDevices* InputDev);
	virtual void Update(World &world) override;

private:
	InputDevices* InputDev;
	int mCurrentEntity;
	bool mUpKey;
	bool mDownKey;
	bool mEnterKey;

	Aspect mGameEntitesAspect;
	Aspect mEndMenuEntitesAspect;
	Aspect mMenuMusicAspect;
};

#endif
