#include "PlayerInputSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <InputDevices.hpp>
#include <iostream>

// DEBUG
bool PlayerInputSystem::sShowDebugText = false;

PlayerInputSystem::PlayerInputSystem(InputDevices* InputDev)
{
	this->InputDev = InputDev;

	Asp.AddInclusiveAspect(INPUT_COMPONENT);

	mJoystickDeadZone = 15000.0f;
}

void PlayerInputSystem::Update(World &world)
{
	if (world.mCurrentState != GameState::GAMEPLAY)
		return;

	std::vector<unsigned int> entities = this->getEntities( world );
	std::vector<OIS::JoyStickState> jsStates;

	InputDev->g_kb->capture();
	InputDev->g_m->capture();

	for(int i = 0; i < 4 ; ++i )
	{
		if(InputDev->g_joys[i] )
		{
			InputDev->g_joys[i]->capture();			
			jsStates.push_back(InputDev->g_joys[i]->getJoyStickState());
		}
	}

    for( auto it : entities )
    {
		WorldPositionComponent* wpc = (WorldPositionComponent*)world.mEntityHandler->GetComponent( it, WORLD_POSITION_COMPONENT );
		InputComponent* inputComponent = (InputComponent*)world.mEntityHandler->GetComponent( it, INPUT_COMPONENT );

		if (inputComponent->inputDeviceID == InputDevice::KEYBOARD)
		{
			if(InputDev->g_kb->isKeyDown((OIS::KeyCode)inputComponent->keyBindUp))
			{
				wpc->y += 1.2f;
				if( wpc->y > 45.0f )
					wpc->y = 45.0f;
			}
			if(InputDev->g_kb->isKeyDown((OIS::KeyCode)inputComponent->keyBindDown))
			{
				wpc->y -= 1.2f;
				if( wpc->y < -45.0f )
					wpc->y = -45.0f;
			}

			if (sShowDebugText)
				std::cout << "Paddle position: (" << wpc->x << ", " << wpc->y << ")" << std::endl;

			if(InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_ESCAPE))
				world.mCurrentState = GameState::MAIN_MENU;
		}
		else
		{
			if(jsStates[inputComponent->inputDeviceID].mAxes[0].abs < -mJoystickDeadZone)
			{
				wpc->y += 1.2f;
				if( wpc->y > 45.0f )
					wpc->y = 45.0f;
			}
			if(jsStates[inputComponent->inputDeviceID].mAxes[0].abs >  mJoystickDeadZone)
			{
				wpc->y -= 1.2f;
				if( wpc->y < -45.0f )
					wpc->y = -45.0f;
			}
		}
	}

	if(InputDev->g_kb->isKeyDown(OIS::KC_B))
		sShowDebugText = true;
	
	if(InputDev->g_kb->isKeyDown(OIS::KC_V))
		sShowDebugText = false;

	if(InputDev->g_kb->isKeyDown(OIS::KC_N))
		std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << std::endl;
}
