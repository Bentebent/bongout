#include "AudioSystem.hpp"
#include <ComponentFramework/World.hpp>
#include <ComponentFramework/EntityHandler.hpp>

#define _USE_MATH_DEFINES
#include <math.h>
#include <climits>
#include <iostream>

AudioSystem::AudioSystem()
{
    Asp.AddInclusiveAspect( AUDIO_SOURCE_COMPONENT );
    this->audioCore = new AudioCore();

	 mMenuSoundAspect.AddInclusiveAspect (AUDIO_SOURCE_COMPONENT);
	 mMenuSoundAspect.AddInclusiveAspect(MENU_COMPONENT);
	
	 mGameplaySoundAspect.AddExclusiveAspect(MENU_COMPONENT);
	 mGameplaySoundAspect.AddInclusiveAspect(AUDIO_SOURCE_COMPONENT);
}

AudioSystem::~AudioSystem()
{
    delete this->audioCore;
}

void AudioSystem::Update( World &world )
{
    auto stream = this->audioCore->dequeueEmptyBuffer();

	switch (world.mCurrentState)
	{
		
		case GameState::LOAD_GAME:
		case GameState::READY_MENU:
		case GameState::GAMEPLAY:
			{
				mGameplaySoundAspect.RemoveInclusiveAspect(PHYSICS_COMPONENT);
				Asp = mGameplaySoundAspect;
			}
			break;
		case GameState::ROUND_OVER_MENU:
			{
				mGameplaySoundAspect.AddInclusiveAspect(PHYSICS_COMPONENT);
				Asp = mGameplaySoundAspect;
			}
			break;
		default:
				Asp = mMenuSoundAspect;
			break;
	}

    while ( stream != nullptr )
    {
        bool coreCanExpectMoreBuffers = false;
        for( int i = 0; i < stream->size(); i++ )
        {
            (*stream)[i] = 0;
        }

        for( auto it : this->getEntities( world ) )
        {
            auto source = static_cast<AudioSourceComponent*>( world.mEntityHandler->GetComponent( it, AUDIO_SOURCE_COMPONENT ) );
         
            if( source->playing )
            {
                coreCanExpectMoreBuffers = true; 
                const double SQRT_2 = sqrt( 2 ); 

                std::shared_ptr<AudioRawPCMData> buffer = AudioCore::dataBuffers[source->buffer].dataBuffer;
                
                int startSourceIndex = source->timer;
                int sourceSize = FRAMES_PER_BUFFER * 2;

                std::vector<short> dataSource = buffer->getData( startSourceIndex, sourceSize );
                //Constant power pan.
                double angle = M_PI / 4.0 * source->panning;
                float a_right = SQRT_2 / 2.0f * (cos(angle) + sin(angle));
                float a_left = SQRT_2 / 2.0f * (cos(angle) - sin(angle));

                for( int i = 0; i < FRAMES_PER_BUFFER; i++ )
                {
                    (*stream)[i*2+0] += (float)(dataSource[i*2+0] / (float)SHRT_MAX * a_left);
                    (*stream)[i*2+1] += (float)(dataSource[i*2+1] / (float)SHRT_MAX * a_right);
                }

                source->timer += FRAMES_PER_BUFFER * 2;

                if( source->timer > buffer->getSampleCount()  )
                {
                    if( source->looping )
                    {
                        source->timer = source->timer - buffer->getSampleCount();
                    }
                    else
                    {
                        source->timer = 0;
                        source->playing = false;
                    }
                }
            }
        }

        this->audioCore->tellAreBuffersComing( coreCanExpectMoreBuffers );
        this->audioCore->queueFilledBuffer( stream );
        stream = this->audioCore->dequeueEmptyBuffer();
    }
}
