#ifndef DAMAGE_SYSTEM_H
#define DAMAGE_SYSTEM_H

#include <ComponentFramework/System.hpp>

class DamageSystem : public System
{
public:
	DamageSystem();
	virtual void Update(World& world) override;
private:
};

#endif