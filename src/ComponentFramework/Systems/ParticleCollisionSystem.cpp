#include "ParticleCollisionSystem.hpp"

#include <ComponentFramework/EntityFactory.hpp>
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>

ParticleCollisionSystem::ParticleCollisionSystem()
{
	Asp.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	Asp.AddInclusiveAspect(BRICK_COMPONENT);
	Asp.AddInclusiveAspect(PHYSICS_COMPONENT);
}

void ParticleCollisionSystem::Update(World& world)
{
	std::vector<unsigned int> entities = this->getEntities( world );

    for( auto it : entities )
    {
		PhysicsComponent* phc = (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);
		WorldPositionComponent* wpc = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
		
		if(phc->hasCollided)
		{		
			unsigned int ent = CreateEntity<EmitterComponent, TimerComponent, WorldPositionComponent, RemovableComponent>(world.mEntityHandler, 
				EmitterComponent(EmitterComponent::EmitterType::DEFAULT_EMITTER, 1000)
				, TimerComponent(TimerComponent::TimerType::COMPONENT_REMOVE, 0.4f, (void*)&EmitterComponent::ID)
				, WorldPositionComponent(wpc->x, wpc->y, wpc->z)
				, RemovableComponent(RemovableComponent::RemoveOn::ON_TIMER));
			/*
			EmitterComponent* emc =  (EmitterComponent*)world.mEntityHandler->GetComponent(ent, EMITTER_COMPONENT);
			TimerComponent* tmc = (TimerComponent*)world.mEntityHandler->GetComponent(ent, TIMER_COMPONENT); 

			tmc->UserData = (void*)&emc->ID;
			*/
		}		
    }    
}