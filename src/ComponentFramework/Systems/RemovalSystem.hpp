#ifndef SRC_REMOVAL_SYSTEM_HPP
#define SRC_REMOVAL_SYSTEM_HPP

#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Aspect.hpp>

class RemovalSystem : public System
{
public:
	RemovalSystem();
	virtual void Update(World& world) override;

private:
};


#endif