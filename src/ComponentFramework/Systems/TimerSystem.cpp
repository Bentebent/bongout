#include "TimerSystem.hpp"

#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>

TimerSystem::TimerSystem()
{
	Asp.AddInclusiveAspect(TIMER_COMPONENT);
}

void TimerSystem::Update(World& world)
{
	std::vector<unsigned int> entities = this->getEntities( world );

    for( auto it : entities )
    {
		TimerComponent* tmc = (TimerComponent*)world.mEntityHandler->GetComponent(it, TIMER_COMPONENT);
		tmc->Time -= world.mDelta;
		if(tmc->Time <= 0.0f)
		{
			switch (tmc->timerType)
			{
				case TimerComponent::TimerType::COMPONENT_REMOVE:
				{
					ComponentID* cmpId = (ComponentID*)tmc->UserData;
					world.mEntityHandler->RemoveComponent(it, *cmpId);
					break;
				}			
			}	

			//Good bye!
			world.mEntityHandler->RemoveComponent(it, TIMER_COMPONENT);
		}
    }    
}