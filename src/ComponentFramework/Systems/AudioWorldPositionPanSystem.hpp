#ifndef SRC_COMPONENTFRAMEWORK_SYSTEMS_AUDIOWORLDPOSITIONPAN_H
#define SRC_COMPONENTFRAMEWORK_SYSTEMS_AUDIOWORLDPOSITIONPAN_H

#include <ComponentFramework/System.hpp>

class World;

class AudioWorldPositionPanSystem : public System
{
public:
    AudioWorldPositionPanSystem();
    ~AudioWorldPositionPanSystem();

    void Update( World &world );
};
#endif
