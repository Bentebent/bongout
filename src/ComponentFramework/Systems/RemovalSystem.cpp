#include "RemovalSystem.hpp"

#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>

RemovalSystem::RemovalSystem()
{
	Asp.AddInclusiveAspect( PHYSICS_COMPONENT );
	Asp.AddInclusiveAspect( BRICK_COMPONENT );
	Asp.AddInclusiveAspect( REMOVABLE_COMPONENT );
}

void RemovalSystem::Update(World &world)
{
	if (world.mCurrentState != GameState::GAMEPLAY)
		return;

	std::vector<unsigned int> entities = this->getEntities( world );

    for( auto it : entities )
    {
		RemovableComponent* rmc = (RemovableComponent*)world.mEntityHandler->GetComponent(it, REMOVABLE_COMPONENT);
		switch (rmc->removeOn)
		{
			case RemovableComponent::RemoveOn::ON_COLLISION:
			{
				PhysicsComponent* phc = (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);
				assert(phc != nullptr);
				if(phc->hasCollided)
				{
					world.mEntityHandler->RemoveEntity(it);
				}
				break;
			}
			case RemovableComponent::RemoveOn::ON_TIMER:
			{
				TimerComponent* tmc = (TimerComponent*)world.mEntityHandler->GetComponent(it, TIMER_COMPONENT);
				if(tmc->Time <= 0.0f)
				{
					world.mEntityHandler->RemoveEntity(it);
				}
				break;
			}
			case RemovableComponent::RemoveOn::ON_DEATH:
			{
				LifeComponent* lc = (LifeComponent*)world.mEntityHandler->GetComponent(it, LIFE_COMPONENT);
					
				if (lc->lives <= 0)
					world.mEntityHandler->RemoveEntity(it);
				break;
			}
		}
    }    
}