#include "ParticleSystem.hpp"

#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <ctime>
#include <cstring>

ParticleSystem::ParticleSystem()
{
	Asp.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	Asp.AddInclusiveAspect(EMITTER_COMPONENT);

	std::memset(NextParticle, 0, sizeof(int) * EmitterComponent::EmitterType::NUMBER_OF_EMITTERS);
	std::memset(ParticleData, 0, sizeof(Particle) * MAX_PARTICLES * EmitterComponent::EmitterType::NUMBER_OF_EMITTERS);
	std::memset(ParticleSpeeds, 0, sizeof(ParticleSpeed) * MAX_PARTICLES * EmitterComponent::EmitterType::NUMBER_OF_EMITTERS);
	std::srand(std::time(nullptr));


#ifdef OPENGL

	glGenBuffers(EmitterComponent::EmitterType::NUMBER_OF_EMITTERS, this->ParticleBuffers);
	glGenVertexArrays(EmitterComponent::EmitterType::NUMBER_OF_EMITTERS, this->ParticleVertexAttributeObjects);

	for(int i=0; i < EmitterComponent::EmitterType::NUMBER_OF_EMITTERS; ++i)
	{
		ParticleShaders[i].LoadShader("assets/OpenglShaders/DefaultParticle.vertex", "assets/OpenglShaders/DefaultParticle.geometry", "assets/OpenglShaders/DefaultParticle.fragment"); 		 
		glBindVertexArray(this->ParticleVertexAttributeObjects[i]);
		glBindBuffer(GL_ARRAY_BUFFER, this->ParticleBuffers[i]);
		glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * sizeof(Particle), &this->ParticleData[i * MAX_PARTICLES], GL_DYNAMIC_DRAW);

		//Position
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)0);
	
		//Scale
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)(3 * sizeof(float)));

		//Life
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)(4 * sizeof(float)));

		glBindVertexArray(0);

		UniformLocations[0][i] = ParticleShaders[i].GetPosition("viewMatrix");
		UniformLocations[1][i] = ParticleShaders[i].GetPosition("projMatrix");
	}

#endif
}

void ParticleSystem::Update(World& world)
{
	std::vector<unsigned int> entities = this->getEntities(world);

    for(auto it : entities )
    {		
		WorldPositionComponent* wpc = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
		EmitterComponent* emc = (EmitterComponent*)world.mEntityHandler->GetComponent(it, EMITTER_COMPONENT);
		int Start = NextParticle[emc->emitterType];
		int SpawnParticles = static_cast<int>(std::ceil(static_cast<float>(emc->ParticlesPerSecond) * world.mDelta)); 

		int Index = Start;
		for(int i=0; i < SpawnParticles; ++i)
		{
			Index = Index % MAX_PARTICLES;
			if(emc->emitterType == EmitterComponent::EmitterType::DEFAULT_EMITTER)
			{
				Particle p = 
				{
					wpc->x + emc->Offset_x,					
					wpc->y + emc->Offset_y,					
					wpc->z + emc->Offset_z,					
					0.5f,
					0.5f
				};
				ParticleData[emc->emitterType][Index] = p;
				ParticleSpeeds[emc->emitterType][Index].x_vel = (rand() % 500 / 500.0f)* 2.0f - 1.0f;
				ParticleSpeeds[emc->emitterType][Index].y_vel = (rand() % 500 / 500.0f)* 2.0f - 1.0f;
				ParticleSpeeds[emc->emitterType][Index].z_vel = (rand() % 500 / 500.0f)* 3.0f - 1.0f;
			}			
			Index++;			
		}		
		NextParticle[emc->emitterType] = (NextParticle[emc->emitterType] + SpawnParticles) % MAX_PARTICLES;
    }    

	for(int i = 0; i < EmitterComponent::EmitterType::NUMBER_OF_EMITTERS; ++i)
	{
		for(int k = 0; k < MAX_PARTICLES; ++k)
		{
			ParticleData[i][k].life -= world.mDelta;		
			ParticleData[i][k].x += ParticleSpeeds[i][k].x_vel * world.mDelta;
			ParticleData[i][k].y += ParticleSpeeds[i][k].y_vel * world.mDelta;
			ParticleData[i][k].z += ParticleSpeeds[i][k].z_vel * world.mDelta;
		}		
	}
}

void ParticleSystem::Render(World& world)
{
#ifdef OPENGL
	for(int i = 0; i < EmitterComponent::EmitterType::NUMBER_OF_EMITTERS; ++i)
	{
		glBindBuffer(GL_ARRAY_BUFFER, this->ParticleBuffers[i]);
		glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * sizeof(Particle), &this->ParticleData[i * MAX_PARTICLES], GL_DYNAMIC_DRAW);

		glm::mat4 viewMat, projMat;
		world.mCamera.GetViewMatrix(viewMat);
		world.mCamera.GetProjectionMatrix(projMat);	

		ParticleShaders[i].Activate();
		ParticleShaders[i].SetUniform(1, viewMat, UniformLocations[0][i]) ;
		ParticleShaders[i].SetUniform(1, projMat, UniformLocations[1][i]) ;
		
		glBindVertexArray(this->ParticleVertexAttributeObjects[i]);
		
		glDrawArrays(GL_POINTS, 0, MAX_PARTICLES);
		
		glBindVertexArray(0);
		ParticleShaders[i].DeActivate();
	}
#endif
}
