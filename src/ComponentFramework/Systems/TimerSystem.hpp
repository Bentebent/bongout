#ifndef SRC_TIMER_SYSTEM_HPP
#define SRC_TIMER_SYSTEM_HPP

#include <ComponentFramework/System.hpp>


class TimerSystem : public System
{
public:
	TimerSystem();
	virtual void Update(World& world) override;

};


#endif