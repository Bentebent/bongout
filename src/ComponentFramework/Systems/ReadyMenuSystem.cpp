#include "ReadyMenuSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include "../EntityFactory.hpp"
#include <ComponentFramework/World.hpp>
#include <InputDevices.hpp>
#include <iostream>

ReadyMenuSystem::ReadyMenuSystem()
{
	mCurrentEntity = 3;
	mElapsedTime = 0.0f;
	droppedPTF = false;
    Asp.AddInclusiveAspect( READY_MENU_COMPONENT );
	Asp.AddInclusiveAspect( WORLD_POSITION_COMPONENT );
}

void ReadyMenuSystem::Update(World &world)
{
	if (world.mCurrentState != GameState::READY_MENU)
	{
		mFirstEnter = true;
		ResetMenu(world);

		return;
	}
	mElapsedTime += world.mDelta;
	
	BaseMenuSystem::Update(world);

	std::vector<unsigned int> entities = this->getEntities( world );

    for( auto it : entities )
    {
		ReadyMenuComponent* readyMenuComponent = (ReadyMenuComponent*)world.mEntityHandler->GetComponent(it, READY_MENU_COMPONENT);
		WorldPositionComponent* worldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
		MenuComponent* menuComponent = (MenuComponent*)world.mEntityHandler->GetComponent(it, MENU_COMPONENT);
		ScaleComponent* scaleComponent = (ScaleComponent*)world.mEntityHandler->GetComponent(it, SCALE_COMPONENT);
		GraphicsComponent* graphicsComponent = (GraphicsComponent*)world.mEntityHandler->GetComponent(it, GRAPHICS_COMPONENT);

		if (readyMenuComponent->id == mCurrentEntity)
		{
			readyMenuComponent->elapsedTime += world.mDelta;
			graphicsComponent->renderMe = true;

			if (mCurrentEntity == 0)
			{
				if (!droppedPTF)
				{
					AudioDropComponent* adc = (AudioDropComponent*)world.mEntityHandler->GetComponent(it, AUDIO_DROP_COMPONENT);

					CreateEntity( world.mEntityHandler,
					WorldPositionComponent( *worldPos ),
					AudioSourceComponent( adc->buffer, true, false ),
					AudioWorldPanningComponent( adc->center, adc->width ) );
					droppedPTF = true;
				}
			}

			if (readyMenuComponent->elapsedTime > readyMenuComponent->stayTimer)
			{
				if (mCurrentEntity != 0)
				{
					worldPos->z = WeightedAverage(worldPos->z, 75, 175);
				}
				else
				{
					worldPos->z = WeightedAverage(worldPos->z, 25, 175);
				}
			}
			else
			{
				worldPos->z = WeightedAverage(worldPos->z, menuComponent->popN, menuComponent->popW);
				float scale = 0;

				if (mCurrentEntity != 0)
					scale = WeightedAverage(scaleComponent->x, 150, 5);
				else
				{
					scale = WeightedAverage(scaleComponent->x, 15, 5);
				}

				scaleComponent->x = scale;
				scaleComponent->y = scale;
				scaleComponent->z = scale;
			}

			if (readyMenuComponent->elapsedTime > readyMenuComponent->stayTimer * 1.2f)
				mCurrentEntity--;
		}
		else
		{
			graphicsComponent->renderMe = false;
		}
    } 

	if (mElapsedTime > 6.0f)
		world.mCurrentState = GameState::GAMEPLAY;
}

void ReadyMenuSystem::ResetMenu(World& world)
{
	std::vector<unsigned int> entities = this->getEntities( world );

	for (auto it : entities)
	{
		ReadyMenuComponent* readyMenuComponent = (ReadyMenuComponent*)world.mEntityHandler->GetComponent(it, READY_MENU_COMPONENT);
		readyMenuComponent->elapsedTime = 0.0f;
	}
	
	droppedPTF = false;
	mElapsedTime = 0;
	mCurrentEntity = 3;

}