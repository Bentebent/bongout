#ifndef SRC_COMPONENTFRAMEWORK_SYSTEMS_AUDIOSYSTEM_H
#define SRC_COMPONENTFRAMEWORK_SYSTEMS_AUDIOSYSTEM_H

#include <Audio/AudioCore.hpp>
#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Aspect.hpp>

class AudioSystem : public System
{
public:
    AudioSystem();
    virtual ~AudioSystem();
    virtual void Update(World& world) override;

private:
    AudioCore *audioCore;    

	Aspect mMenuSoundAspect;
	Aspect mGameplaySoundAspect;
};

#endif
