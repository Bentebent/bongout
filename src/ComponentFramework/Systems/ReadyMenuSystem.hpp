#ifndef READY_MENU_SYSTEM_H
#define READY_MENU_SYSTEM_H

//#include <InputDevices.hpp>
#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Systems/BaseMenuSystem.hpp>

enum ReadyMenuEntities
{
	READY_PREPARE_TO_FIGHT,
	READY_ONE,
	READY_TWO,
	READY_THREE
};

class ReadyMenuSystem : public BaseMenuSystem
{
public:
	ReadyMenuSystem();
	virtual void Update(World &world) override;
	void ResetMenu(World& world);
private:
	int mCurrentEntity;
	float mElapsedTime;
	bool droppedPTF;
};

#endif
