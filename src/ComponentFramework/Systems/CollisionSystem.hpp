#ifndef COLLISIONSYSTEM_H
#define COLLISIONSYSTEM_H

#include <Utility/Physics.hpp>
#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Aspect.hpp>

class CollisionSystem : public System
{
public:
	CollisionSystem();
	virtual void Update(World& world) override;

private:

	Aspect ballAspect;
	Aspect boxAspect;
};

#endif