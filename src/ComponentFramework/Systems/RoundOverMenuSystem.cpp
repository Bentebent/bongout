#include "RoundOverMenuSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include "../EntityFactory.hpp"
#include <ComponentFramework/World.hpp>
#include <InputDevices.hpp>
#include <iostream>

RoundOverMenuSystem::RoundOverMenuSystem()
{
	mElapsedTime = 0.0f;

    Asp.AddInclusiveAspect( ROUND_OVER_MENU_COMPONENT );
	Asp.AddInclusiveAspect( WORLD_POSITION_COMPONENT );
}

void RoundOverMenuSystem::Update(World &world)
{
	if (world.mCurrentState != GameState::ROUND_OVER_MENU)
	{
		mFirstEnter = true;
		Reset();
		return;
	}

	mElapsedTime += world.mDelta;

	BaseMenuSystem::Update(world);

	std::vector<unsigned int> entities = this->getEntities( world );

    for( auto it : entities )
    {
		WorldPositionComponent* worldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
		MenuComponent* menuComponent = (MenuComponent*)world.mEntityHandler->GetComponent(it, MENU_COMPONENT);
		ScaleComponent* scaleComponent = (ScaleComponent*)world.mEntityHandler->GetComponent(it, SCALE_COMPONENT);
		GraphicsComponent* grc = (GraphicsComponent*)world.mEntityHandler->GetComponent(it, GRAPHICS_COMPONENT);
		RoundOverMenuComponent* romc = (RoundOverMenuComponent*)world.mEntityHandler->GetComponent(it, ROUND_OVER_MENU_COMPONENT);

		worldPos->z = WeightedAverage(worldPos->z, menuComponent->popN, menuComponent->popW);
		
		float scale = WeightedAverage(scaleComponent->x, 15, 5);

		scaleComponent->x = scale;
		scaleComponent->y = scale;
		scaleComponent->z = scale;

		if (romc->id == 2)
		{
			if (world.mGameData.mWinner == 2)
				grc->renderMe = true;
			else
				grc->renderMe = false;
		}
		else if (romc->id == 1)
		{
			if (world.mGameData.mWinner == 1)
				grc->renderMe = true;
			else
				grc->renderMe = false;
		}
	}

	if (mElapsedTime > 3.0f)
		world.mCurrentState = GameState::LOAD_GAME;
}

void RoundOverMenuSystem::Reset()
{
	mElapsedTime = 0.0f;
}