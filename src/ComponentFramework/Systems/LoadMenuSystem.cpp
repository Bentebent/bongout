#include "LoadMenuSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include "../../InputDevices.hpp"
#include "../EntityFactory.hpp"
#include <GraphicsSystem/GraphicsMaterial.hpp>
#include <Audio/AudioCore.hpp>

LoadMenuSystem::LoadMenuSystem()
{

}

void LoadMenuSystem::Update(World& world)
{
	if (world.mCurrentState != GameState::LOAD_MENU)
		return;

	int music_buffer = AudioCore::loadFile( "assets/sound/menu.ogg" );
    CreateEntity( world.mEntityHandler, MenuComponent(), AudioSourceComponent( music_buffer ));

	LoadMainMenu(world);
	LoadReadyMenu(world);
	LoadEndMenu(world);
	LoadRoundOverMenu(world);

	world.mCurrentState = GameState::MAIN_MENU;
}

void LoadMenuSystem::LoadMainMenu(World& world)
{
	// init menu...
	LightMaterial aa = LightMaterial( 
		10, .7f,
		1, 1, 1,
		-1, 1, 1 );

	MeshMaterial MenuMaterial = MeshMaterial( 
		1, 0, 0, 
		1, 0, 0, 1, 
		0, 0, 0, 0 );

	// init menu...
	CreateEntity(world.mEntityHandler, 
		GraphicsComponent( MeshType::NoMesh, MenuMaterial ),
		MenuComponent(),
		MainMenuComponent(),
		LightComponent( LightType::DirectionalLight, aa ) 
		);

	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(-1.4f, 3.2f, -250000.0f),
		GraphicsComponent( MeshType::BongOutText, MenuMaterial ),
		MenuComponent(-1.4f, 3.2f, -100.0f, 5.0f, 6.0f, 6.0f, -250000.0f),
		ScaleComponent(5.0f, 5.0f, 5.0f),
		MainMenuComponent()
		);

	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0, -1.0f, -50000.0f),
		GraphicsComponent( MeshType::StartGameText, MenuMaterial ),
		MenuComponent(0.0f, -1.0f, -500.0f, 2.0f, 20.0f, 3.0f, -50000.0f),
		ScaleComponent(2.0f, 2.0f, 2.0f),
		MainMenuComponent(0)
		);

	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0, -3.5f, -100000.0f),
		GraphicsComponent( MeshType::QuitText, MenuMaterial ),
		MenuComponent(0.0f, -3.5f, -1000.0f, 2.0f, 20.0f, 3.0f, -100000.0f),
		ScaleComponent(2.0f, 2.0f, 2.0f),
		MainMenuComponent(1)
		);

}

void LoadMenuSystem::LoadEndMenu(World& world)
{
	// init end menu
	LightMaterial aa = LightMaterial( 
		10, .7f,
		1, 1, 1,
		-1, 1, 1 );

	MeshMaterial MenuMaterial = MeshMaterial( 
		1, 0, 0, 
		1, 0, 0, 1, 
		1, 1, 1, 0 );

	CreateEntity(world.mEntityHandler, 
		GraphicsComponent( MeshType::NoMesh, MenuMaterial ),
		MenuComponent(),
		EndMenuComponent(),
		LightComponent( LightType::DirectionalLight, aa ) 
		);
	
	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0.0f, 4.2f, -100.0f),
		GraphicsComponent( MeshType::GameOverText, MenuMaterial ),
		MenuComponent(0.0f, 3.2f, -100.0f, 5.0f, 10.0f, 6.0f, -100.0f),
		ScaleComponent(5.0f, 5.0f, 5.0f),
		EndMenuComponent()
		);
	
	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0.0f, 0.8f, -1500.0f),
		GraphicsComponent( MeshType::Player1WinsText, MenuMaterial ),
		MenuComponent(0.0f, 2.2f, -100.0f, 2.5f, 10.0f, 6.0f, -1500.0f),
		ScaleComponent(2.5f, 2.5f, 2.5f),
		EndMenuComponent(4)
		);

	
	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0.0f, 0.8f, -5000.0f),
		GraphicsComponent( MeshType::Player2WinsText, MenuMaterial ),
		MenuComponent(0.0f, 2.2f, 0.0f, 2.5f, 10.0f, 6.0f, -5000.0f),
		ScaleComponent(2.5f, 2.5f, 2.5f),
		EndMenuComponent(5)
		);
	
	
	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0.0f, -1.4f, -10000.0f),
		GraphicsComponent( MeshType::RestartGameText, MenuMaterial ),
		MenuComponent(0.0f, 1.2f, -1000.0f, 1.0f, 10.0f, 1.2f, -10000.0f),
		ScaleComponent(1.0f, 1.0f, 1.0f),
		EndMenuComponent(0)
		);
	
	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0.0f, -2.8f, -30000.0f),
		GraphicsComponent( MeshType::MainMenuText, MenuMaterial ),
		MenuComponent(0.0f, -1.2f, 0.0f, 1.0f, 10.0f, 1.2f, -30000.0f),
		ScaleComponent(1.0f, 1.0f, 1.0f),
		EndMenuComponent(1)
		);
	
	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0.0f, -4.8f, -50000.0f),
		GraphicsComponent( MeshType::ExitGameText, MenuMaterial ),
		MenuComponent(0.0f, -3.2f, 0.0f, 1.0f, 10.0f, 1.2f, -50000.0f),
		ScaleComponent(1.0f, 1.0f, 1.0f),
		EndMenuComponent(2)
		);
}

void LoadMenuSystem::LoadReadyMenu(World& world)
{
	int ptf_buffer = AudioCore::loadFile( "assets/sound/ptf.ogg" );

	LightMaterial aa = LightMaterial( 10, 0.2f, 0.5f, 0.5f, 0.5f, 1, 1, 1 );

	MeshMaterial MenuMaterial = MeshMaterial( 
		1, 0, 0, 
		1, 0, 0, 1, 
		1, 1, 1, -1 );

	CreateEntity(world.mEntityHandler, 
		GraphicsComponent( MeshType::NoMesh, MenuMaterial ),
		MenuComponent(),
		ReadyMenuComponent(),
		LightComponent( LightType::DirectionalLight, aa ) 
		);

	unsigned int prepareToFight = CreateEntity( 
			world.mEntityHandler,
			WorldPositionComponent(0.0f, 0.0f, 0.0f),
			GraphicsComponent( MeshType::PrepareToFightText, MenuMaterial ),
			MenuComponent(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 120.0f, 0.0f),
			ScaleComponent(0.0f, 0.0f, 0.0f),
			ReadyMenuComponent(0, 1.5f),
            AudioDropComponent( ptf_buffer, 0, 100 )
			);
	
	unsigned int one = 	CreateEntity( world.mEntityHandler,
			WorldPositionComponent(0.0f, 0.0f, 0.0f),
			GraphicsComponent( MeshType::Num1, MenuMaterial ),
			MenuComponent(0.0f, 0.0f, 0.0f, 0.0f, 2.0f, 120.0f, 0.0f),
			ScaleComponent(0.0f, 0.0f, 0.0f),
			ReadyMenuComponent(1, 1.0f)
			);
	
	unsigned int two = 	CreateEntity( world.mEntityHandler,
			WorldPositionComponent(0.0f, 0.0f, 0.0f),
			GraphicsComponent( MeshType::Num2, MenuMaterial ),
			MenuComponent(0.0f, 0.0f, 0.0f, 0.0f, 2.0f, 120.0f, 0.0f),
			ScaleComponent(0.0f, 0.0f, 0.0f),
			ReadyMenuComponent(2, 1.0f)
			);
			
	unsigned int three = CreateEntity( world.mEntityHandler,
			WorldPositionComponent(0.0f, 0.0f, 0.0f),
			GraphicsComponent( MeshType::Num3, MenuMaterial ),
			MenuComponent(0.0f, 0.0f, 0.0f, 0.0f, 2.0f, 120.0f, 0.0f),
			ScaleComponent(0.0f, 0.0f, 0.0f),
			ReadyMenuComponent(3, 1.0f)
			);
			
}

void LoadMenuSystem::LoadRoundOverMenu(World& world)
{
	LightMaterial aa = LightMaterial( 10, 0.2f, 0.5f, 0.5f, 0.5f, 1, 1, 1 );

	MeshMaterial MenuMaterial = MeshMaterial( 
		1, 0, 0, 
		1, 0, 0, 1, 
		1, 1, 1, 0 );

	
	CreateEntity(world.mEntityHandler, 
		GraphicsComponent( MeshType::NoMesh, MenuMaterial ),
		MenuComponent(),
		RoundOverMenuComponent(),
		LightComponent( LightType::DirectionalLight, aa ) 
		);

	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0.0f, 0.0f, 0.0f),
		GraphicsComponent( MeshType::Player1WinsText, MenuMaterial ),
		MenuComponent(0.0f, 0.0f, 0.0f, 0.0f, 10.0f, 120.0f, 0.0f),
		ScaleComponent(0.0f, 0.0f, 0.0f),
		RoundOverMenuComponent(1)
		);

	CreateEntity(world.mEntityHandler, 
		WorldPositionComponent(0.0f, 0.0f, 0.0f),
		GraphicsComponent( MeshType::Player2WinsText, MenuMaterial ),
		MenuComponent(0.0f, 0.0f, 0.0f, 0.0f, 10.0f, 120.0f, 0.0f),
		ScaleComponent(0.0f, 0.0f, 0.0f),
		RoundOverMenuComponent(2)
		);
}