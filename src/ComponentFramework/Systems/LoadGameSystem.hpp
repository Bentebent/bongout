#ifndef LOAD_GAME_SYSTEM_H
#define LOAD_GAME_SYSTEM_H

#include <ComponentFramework/System.hpp>

class LoadInterface;

class LoadGameSystem : public System
{
public:
	LoadGameSystem();
	virtual void Update(World& world) override;

private:
	void LoadSimpleLevel(World& world);
	void LoadDemoLevel(World& world);

	Aspect mMusicAspect;
	bool mMusicLoaded;

	Aspect mBackgroundAspect;
};

#endif
