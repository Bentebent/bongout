#ifndef ROUND_OVER_MENU_SYSTEM_H
#define ROUND_OVER_MENU_SYSTEM_H

//#include <InputDevices.hpp>
#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Systems/BaseMenuSystem.hpp>

class RoundOverMenuSystem : public BaseMenuSystem
{
public:
	RoundOverMenuSystem();
	virtual void Update(World &world) override;

private:
	float mElapsedTime;
	void Reset();
	
};

#endif
