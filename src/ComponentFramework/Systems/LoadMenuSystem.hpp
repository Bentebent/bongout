#ifndef LOAD_MENU_SYSTEM_H
#define LOAD_MENU_SYSTEM_H

#include "../System.hpp"

class LoadMenuSystem : public System
{
public:
	LoadMenuSystem();
	virtual void Update(World& world) override;

	void LoadMainMenu(World& world);
	void LoadEndMenu(World& world);
	void LoadReadyMenu(World& world);
	void LoadRoundOverMenu(World& world);
private:
};

#endif