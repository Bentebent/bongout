#include "EndMenuSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <InputDevices.hpp>
#include <iostream>

EndMenuSystem::EndMenuSystem(InputDevices* InputDev)
{
	this->InputDev = InputDev;

	mCurrentEntity = 0;

	mUpKey = false;
	mDownKey = false;
	mEnterKey = false;

    Asp.AddInclusiveAspect( END_MENU_COMPONENT );
	Asp.AddInclusiveAspect( WORLD_POSITION_COMPONENT );

	mEndMenuEntitesAspect = Asp;

	mGameEntitesAspect.AddExclusiveAspect( MENU_COMPONENT);

	mMenuMusicAspect.AddInclusiveAspect(MENU_COMPONENT);
	mMenuMusicAspect.AddInclusiveAspect(AUDIO_SOURCE_COMPONENT);
}

void EndMenuSystem::Update(World &world)
{
	if (world.mCurrentState != GameState::END_GAME_MENU)
	{
		mFirstEnter = true;
		return;
	}

	if (mFirstEnter)
	{
		//Get all entities except menu entities
		Asp = mGameEntitesAspect;
		std::vector<unsigned int> entities = this->getEntities(world);

		//remove all
		for (auto it : entities)
			world.mEntityHandler->RemoveEntity(it);

		//Get background music entites
		Asp = mMenuMusicAspect;
		entities = this->getEntities(world);

		for (auto it : entities)
		{
			AudioSourceComponent* asc = (AudioSourceComponent*)world.mEntityHandler->GetComponent(it, AUDIO_SOURCE_COMPONENT);
			asc->timer = 0;
		}

		//reset aspect
		Asp = mEndMenuEntitesAspect;
	}

	BaseMenuSystem::Update(world);

	world.mCamera.SetPosition( 0, 0, 20 );
	glm::vec3 lookAt( 0, 0, 0.05f);
	world.mCamera.UpdateView( lookAt );

	std::vector<unsigned int> entities = this->getEntities( world );

	InputDev->g_kb->capture();
	
	for(int i = 0; i < 4 ; ++i )
	{
		if(InputDev->g_joys[i] )
		{
			InputDev->g_joys[i]->capture();			
		}
	}

	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_DOWN) && !mDownKey)
	{
			mDownKey = true;
			mCurrentEntity += 1;
	}
	else if (!InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_DOWN) )
		mDownKey = false;

	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_UP) && !mUpKey)
	{
			mUpKey = true;
			mCurrentEntity -= 1;
	}
	else if (!InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_UP) )
		mUpKey = false;

	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_RETURN) && !mEnterKey)
	{
			mEnterKey = true;
	}
	else if (!InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_RETURN) )
		mEnterKey = false;

	if (mCurrentEntity < 0)
		mCurrentEntity =  EndMenuEntities::END_MENU_SIZE - 1;

	mCurrentEntity = abs(mCurrentEntity) % EndMenuEntities::END_MENU_SIZE;


    for( auto it : entities )
    {
		EndMenuComponent* endMenuComponent = (EndMenuComponent*)world.mEntityHandler->GetComponent(it, END_MENU_COMPONENT);
		WorldPositionComponent* worldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
		MenuComponent* menuComponent = (MenuComponent*)world.mEntityHandler->GetComponent(it, MENU_COMPONENT);
		ScaleComponent* scaleComponent = (ScaleComponent*)world.mEntityHandler->GetComponent(it, SCALE_COMPONENT);
		GraphicsComponent* graphicsComponent = (GraphicsComponent*)world.mEntityHandler->GetComponent(it, GRAPHICS_COMPONENT);

		if (endMenuComponent->id == 4)
		{
			if (world.mGameData.mWinner == 2)
				graphicsComponent->renderMe = true;
			else
				graphicsComponent->renderMe = false;
		}
		else if (endMenuComponent->id == 5)
		{
			if (world.mGameData.mWinner == 1)
				graphicsComponent->renderMe = true;
			else
				graphicsComponent->renderMe = false;
		}

		int componentID = endMenuComponent->id;
		
		if (mCurrentEntity == componentID)
		{
			worldPos->z = WeightedAverage(worldPos->z, menuComponent->popN, menuComponent->popW);

			float pulse = Pulse(scaleComponent->x, menuComponent->popN, menuComponent->popW, 0.0f, world.mElapsedTime);
			
			scaleComponent->x = pulse;
			scaleComponent->y = pulse;
			scaleComponent->z = pulse;

			switch (mCurrentEntity)
			{
					case EndMenuEntities::END_MENU_RESTART:
						{
							if (mEnterKey)
								world.mCurrentState = GameState::LOAD_GAME;
						}
					break;

					case EndMenuEntities::END_MENU_MAIN_MENU:
						{
							if (mEnterKey)
							{
								world.mPrevState = GameState::END_GAME_MENU;
								world.mCurrentState = GameState::MAIN_MENU;
							}
						}
					break;

					case EndMenuEntities::END_MENU_EXIT_GAME:
						{
							if (mEnterKey)
								world.mCurrentState = GameState::EXIT_PROGRAM;
						}
					break;
			}
		}
		else
		{
			worldPos->z = WeightedAverage(worldPos->z, menuComponent->popN, 0.0f);

			float scale = WeightedAverage(scaleComponent->x, menuComponent->popN, menuComponent->defaultScale);

			scaleComponent->x = scale;
			scaleComponent->y = scale;
			scaleComponent->z = scale;
		}
			
    } 

}
