#ifndef SRC_MENU_INPUT_SYSTEM_HPP
#define SRC_MENU_INPUT_SYSTEM_HPP

//#include <InputDevices.hpp>
#include <ComponentFramework/System.hpp>
#include "BaseMenuSystem.hpp"

struct InputDevices;

enum MainMenuEntities
{
	START = 0,
	QUIT,
	SIZE
};

class MainMenuSystem : public BaseMenuSystem
{
public:
	MainMenuSystem(InputDevices* InputDev);
	virtual void Update(World &world) override;

private:
	InputDevices* InputDev;
	int mCurrentEntity;
	bool mUpKey;
	bool mDownKey;
	bool mEnterKey;
	unsigned char angry;
};

#endif
