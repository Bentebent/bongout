#include "BackgroundLightSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>

BackgroundLightSystem::BackgroundLightSystem()
{
	Asp.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	Asp.AddInclusiveAspect(VELOCITY_COMPONENT);
	Asp.AddInclusiveAspect(CIRCLE_PHYSICS_COMPONENT);
	Asp.AddInclusiveAspect(LIGHT_COMPONENT);
	Asp.AddInclusiveAspect(BACKGROUND_COMPONENT);
}

void BackgroundLightSystem::Update(World &world)
{
	if (!(world.mCurrentState == GameState::GAMEPLAY || world.mCurrentState == GameState::ROUND_OVER_MENU || world.mCurrentState == GameState::READY_MENU))
	{
		return;
	}

	std::vector<unsigned int> entities = this->getEntities( world );

	for (auto it : entities)
	{
		WorldPositionComponent* circleWorldPos	= (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
		VelocityComponent* circleVelocity		= (VelocityComponent*)world.mEntityHandler->GetComponent(it, VELOCITY_COMPONENT);
		PhysicsComponent* physics				= (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);
		CirclePhysicsComponent* circlePhysics	= (CirclePhysicsComponent*)world.mEntityHandler->GetComponent(it, CIRCLE_PHYSICS_COMPONENT);
		GraphicsComponent* graphicsComponent	= (GraphicsComponent*)world.mEntityHandler->GetComponent(it, GRAPHICS_COMPONENT);

		circleWorldPos->x += circleVelocity->x * world.mDelta;
		circleWorldPos->y += circleVelocity->y * world.mDelta;
			
		if (circleWorldPos->x < -250.0f)
			circleVelocity->x = abs(circleVelocity->x);
		if (circleWorldPos->x > 250.0f)
			circleVelocity->x = abs(circleVelocity->x) * -1.0f;
			
		if (circleWorldPos->y < -150.5f)
			circleVelocity->y = abs(circleVelocity->y);
		if (circleWorldPos->y > 150.5f)
			circleVelocity->y = abs(circleVelocity->y) * -1.0f;
	}

}