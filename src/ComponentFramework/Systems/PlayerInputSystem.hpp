#ifndef SRC_INPUT_SYSTEM_HPP
#define SRC_INPUT_SYSTEM_HPP

//#include <InputDevices.hpp>
#include <ComponentFramework/System.hpp>

struct InputDevices;

enum InputDevice
{
	JOYPAD_ONE = 0,
	JOYPAD_TWO,
	JOYPAD_THREE,
	JOYPAD_FOUR,
	KEYBOARD
};

class PlayerInputSystem : public System
{
public:
	PlayerInputSystem(InputDevices* InputDev);
	virtual void Update(World &world) override;

	// DEBUG
	static bool sShowDebugText;

private:
	InputDevices* InputDev;

	float mJoystickDeadZone;
};

#endif
