#ifndef PHYSICSSYSTEM_HPP
#define PHYSICSSYSTEM_HPP

#include <Utility/Physics.hpp>
#include <ComponentFramework/System.hpp>

class PhysicsSystem : public System
{
public:
	PhysicsSystem();
	virtual void Update(World& world) override;

private:

};

#endif