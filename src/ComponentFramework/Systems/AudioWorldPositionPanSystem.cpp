#include "AudioWorldPositionPanSystem.hpp"
#include <ComponentFramework/World.hpp>
#include <ComponentFramework/EntityHandler.hpp>

#include <cmath>

AudioWorldPositionPanSystem::AudioWorldPositionPanSystem()
{
    this->Asp.AddInclusiveAspect( AUDIO_SOURCE_COMPONENT );
    this->Asp.AddInclusiveAspect( WORLD_POSITION_COMPONENT );
    this->Asp.AddInclusiveAspect( AUDIO_WORLD_PANNING_COMPONENT );
}

AudioWorldPositionPanSystem::~AudioWorldPositionPanSystem()
{

}
void AudioWorldPositionPanSystem::Update( World &world )
{
    for( auto ent : this->getEntities( world ) )
    {
        auto * wpc = (WorldPositionComponent*)world.mEntityHandler->GetComponent( ent, WORLD_POSITION_COMPONENT );
        auto * asc = (AudioSourceComponent*)world.mEntityHandler->GetComponent( ent, AUDIO_SOURCE_COMPONENT );
        auto * awpc = (AudioWorldPanningComponent*)world.mEntityHandler->GetComponent( ent, AUDIO_WORLD_PANNING_COMPONENT );

        float relative_position = wpc->x - awpc->x_center;
         
        asc->panning = std::max<float>( -1.0f, std::min<float>( 1.0f, relative_position / awpc->x_width ) );
        
    }
}

