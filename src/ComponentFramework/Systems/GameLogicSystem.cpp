#include "GameLogicSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <ComponentFramework/EntityFactory.hpp>
#include <GraphicsSystem/Materials.hpp>
#include <Audio/AudioCore.hpp>
#include <string>

GameLogicSystem::GameLogicSystem()
{
	mTimeLimit = 10.0f;
	mElapsedTime = 0.0f;
	mRoundLimit = INT_MAX;
	mRoundsLeft = mRoundLimit;
	mSuddenDeath = false;
	mEndGame = false;

	Asp.AddInclusiveAspect(BRICK_COMPONENT);
	Asp.AddInclusiveAspect(PLAYER_COMPONENT);

	mBrickAspect.AddInclusiveAspect(BRICK_COMPONENT);
	mPlayerAspect.AddInclusiveAspect(PLAYER_COMPONENT);
}

void GameLogicSystem::Update(World& world)
{
	if (world.mCurrentState != GameState::GAMEPLAY)
		return;

	mElapsedTime += world.mDelta;
	mOldState = world.mCurrentState;

	std::vector<unsigned int> entities = this->getEntities( world );

	WinCondition(world, entities);
	SuddenDeath(world, entities);

	if (mOldState != world.mCurrentState)
		ResetGameLogic(world);
}

void GameLogicSystem::WinCondition(World& world, std::vector<unsigned int>& entities)
{
	int playerOneBricks = 0;
	int playerTwoBricks = 0;

	//Check for win
	for (auto it : entities)
	{
		if(mBrickAspect.Matches( world.mEntityHandler->GetEntityAspect( it )))
		{
			BrickComponent* bc = (BrickComponent*)world.mEntityHandler->GetComponent(it, BRICK_COMPONENT);

			if (bc->OwnerID == 1)
				playerOneBricks++;
			else
				playerTwoBricks++;
		}
	}

	//Player 1 has zero bricks left, player two has won
	if (playerOneBricks <= 0)
	{
		mEndGame = true;
		world.mGameData.mPlayerTwoRoundsWon++;
		world.mGameData.mWinner = 2;
	}
	//Player 2 has zero bricks left, player one has won
	else if (playerTwoBricks <= 0)
	{
		mEndGame = true;
		world.mGameData.mPlayerOneRoundsWon++;
		world.mGameData.mWinner = 1;
	}

	if (mEndGame)
	{
		mRoundsLeft -= 1;
		
		if (mRoundsLeft <= 0)
		{
			world.mCurrentState = GameState::END_GAME_MENU;
			if (world.mGameData.mPlayerOneRoundsWon > world.mGameData.mPlayerTwoRoundsWon)
				world.mGameData.mWinner = 1;
			else
				world.mGameData.mWinner = 2;
		}
		else
		{
			world.mCurrentState = GameState::ROUND_OVER_MENU;
		}
	}
}

void GameLogicSystem::SuddenDeath(World& world, std::vector<unsigned int>& entities)
{
	//Sudden death logic
	if (mElapsedTime > mTimeLimit && !mSuddenDeath)
	{
		int bop_buffer = AudioCore::loadFile( "assets/sound/paddle_bounce_2.ogg" );

		int music_buffer = AudioCore::loadFile( "assets/sound/sudden_death.ogg" );
		CreateEntity( world.mEntityHandler, AudioSourceComponent( music_buffer, true, false ));

		for (int i = 0; i < 50; i++)
		{
			unsigned int Ball = CreateEntity(world.mEntityHandler,
			GraphicsComponent(MeshType::Ball, meshPalette.ballMaterial),
			ScaleComponent(),
			WorldPositionComponent(-4.0f, 0.0f, 0.0f),
			PhysicsComponent(),
			EmitterComponent(EmitterComponent::EmitterType::DEFAULT_EMITTER, 200),
			LightComponent( LightType::PointLight, lightPalette.ballLightMaterial ),
			CirclePhysicsComponent(1.0f),
			VelocityComponent(70.0f * sin(i+0.7f), 70.0f * cos(i+0.7f)),
			AudioDropComponent( bop_buffer, 0, 100 )
			);
		}
		for (auto it : entities)
		{
			if(mBrickAspect.Matches( world.mEntityHandler->GetEntityAspect( it )))
			{
				LifeComponent* lc = (LifeComponent*)world.mEntityHandler->GetComponent(it, LIFE_COMPONENT);
				lc->lives = 1;
			}
		}

		mSuddenDeath = true;
	}
}

void GameLogicSystem::ResetGameLogic(World& world)
{
	if (world.mCurrentState != GameState::ROUND_OVER_MENU)
	{
		mRoundsLeft = mRoundLimit;
		world.ResetGameData();
	}

	mSuddenDeath = false;
	mEndGame = false;
	mElapsedTime = 0.0f;
}
