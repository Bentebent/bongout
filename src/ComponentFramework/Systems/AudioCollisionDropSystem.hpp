#ifndef SRC_COMPONENTFRAMEWORK_SYSTEMS_AUDIOCOLLISIONDROPSYSTEM_H
#define SRC_COMPONENTFRAMEWORK_SYSTEMS_AUDIOCOLLISIONDROPSYSTEM_H

#include <Audio/AudioCore.hpp>
#include <ComponentFramework/System.hpp>
#include <ComponentFramework/Aspect.hpp>

#define MAX_DROPPED_SOUNDS 6


class AudioCollisionDropSystem : public System
{
    public:
       AudioCollisionDropSystem();

       void Update( World &world );             

       std::vector<unsigned int> droppedSounds;
};

#endif
