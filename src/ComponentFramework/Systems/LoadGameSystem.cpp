#include "LoadGameSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <InputDevices.hpp>
#include <ComponentFramework/EntityFactory.hpp>
#include <Audio/AudioCore.hpp>
#include <GraphicsSystem/GraphicsMaterial.hpp>
#include <GraphicsSystem/LoadInterface.hpp>

#include <GraphicsSystem/Materials.hpp>

LoadGameSystem::LoadGameSystem()
{
	Asp.AddExclusiveAspect(MENU_COMPONENT);
	mMusicAspect.AddExclusiveAspect(MENU_COMPONENT);
	mMusicAspect.AddInclusiveAspect(AUDIO_SOURCE_COMPONENT);
	mMusicAspect.AddInclusiveAspect(PHYSICS_COMPONENT);

	mBackgroundAspect.AddInclusiveAspect( BACKGROUND_COMPONENT );

	mMusicLoaded = false;
}


void LoadGameSystem::Update(World& world)
{
	if (world.mCurrentState != GameState::LOAD_GAME)
		return;

	std::vector<unsigned int> entities = this->getEntities( world );

	if (entities.size() == 0)
		mMusicLoaded = false;

	for (auto it : entities)
	{
		if (mMusicAspect.Matches( world.mEntityHandler->GetEntityAspect( it ) ))
		{
			mMusicLoaded = true;
			continue;
		}
		else if (mBackgroundAspect.Matches( world.mEntityHandler->GetEntityAspect( it) ))
		{
			continue;
		}
		else
			world.mEntityHandler->RemoveEntity(it);
	}

	if (!mMusicLoaded)
	{
		int space_fighter_music = AudioCore::loadFile( "assets/sound/gameplay.ogg" );

		unsigned int BackgroundMusic = CreateEntity( world.mEntityHandler, PhysicsComponent(),
				AudioSourceComponent( space_fighter_music, true, true ) );
	}

	LightMaterial aa = LightMaterial( 10, 0.2f, 0.5f, 0.5f, 0.5f, 1, 1, 1 );

	CreateEntity( world.mEntityHandler, 
		GraphicsComponent( MeshType::NoMesh ),
		LightComponent( LightType::DirectionalLight, aa ),
		PhysicsComponent()
		);

	unsigned int Player1 = CreateEntity(world.mEntityHandler,
		WorldPositionComponent(55.0f, 0.0f, 0.0f),
		ScaleComponent(2.0f, 10.0f, 1.0f),
		GraphicsComponent( MeshType::Brick ),
		PlayerComponent(),
		InputComponent(4, OIS::KeyCode::KC_UP, OIS::KeyCode::KC_DOWN),
		PhysicsComponent(),
		//VelocityComponent(5.0f, 0.0f),
		AABBComponent(Vector2(2.0f, 10.0f)) );

	unsigned int Player2 = CreateEntity(world.mEntityHandler,
		WorldPositionComponent(-55.0f, 0.0f, 0.0f),
		ScaleComponent(2.0f, 10.0f, 1.0f),
		GraphicsComponent( MeshType::Brick ),
		PlayerComponent(),
		InputComponent(4, OIS::KeyCode::KC_W, OIS::KeyCode::KC_S),
		PhysicsComponent(),
		AABBComponent(Vector2(2.0f, 10.0f)) );
		
	unsigned int Background = CreateEntity(world.mEntityHandler,
		WorldPositionComponent(0.0f,0.0f,-200.0f),
		ScaleComponent(10.0f,10.0f,10.0f),
		GraphicsComponent( MeshType::ComplexBackground, meshPalette.backgroundMaterialA )
		);
		
	unsigned int Delimiter = CreateEntity(world.mEntityHandler,
		WorldPositionComponent(0.0f,0.0f,0.0f),
		ScaleComponent(1.0f,1.0f,1.0f),
		GraphicsComponent( MeshType::FieldThinDelimiter, meshPalette.fieldWallMaterialA ) 
		);


	unsigned int UpperWall = CreateEntity(world.mEntityHandler,
		WorldPositionComponent(0.0f,50.0f,0.0f),
		ScaleComponent(1.0f, 1.0f, 1.0f),
		PhysicsComponent(),
		GraphicsComponent( MeshType::FieldThinWall, meshPalette.fieldWallMaterialA ),
		AABBComponent(Vector2(220.0f, 1.0f))
		);

	unsigned int LowerWall = CreateEntity(world.mEntityHandler,
		WorldPositionComponent(0.0f,-50.0f,0.0f),
		ScaleComponent(1.0f, 1.0f, 1.0f),
		PhysicsComponent(),
		GraphicsComponent( MeshType::FieldThinWall, meshPalette.fieldWallMaterialA ),
		AABBComponent(Vector2(220.0f, 1.0f))
		);

	unsigned int LeftWall = CreateEntity(world.mEntityHandler,
		WorldPositionComponent(-100.0f,0.0f,0.0f),
		ScaleComponent(1.0f,1.0f,1.0f),
		PhysicsComponent(),
		GraphicsComponent(MeshType::PlayerWall, meshPalette.fieldWallMaterialA),
		AABBComponent(Vector2(1.0f, 110.0f))
		);
		
	unsigned int RightWall = CreateEntity(world.mEntityHandler,
			WorldPositionComponent(100.0f,0.0f,0.0f),
			ScaleComponent(1.0f,1.0f,1.0f),
			PhysicsComponent(),
			GraphicsComponent( MeshType::PlayerWall, meshPalette.fieldWallMaterialA ),
			AABBComponent(Vector2(1.0f, 110.0f))
			);

	for (int i = 0; i < 20; i++)
	{
		unsigned int lightBackground = CreateEntity( world.mEntityHandler,
			GraphicsComponent( MeshType::NoMesh, meshPalette.ballMaterial ),
			ScaleComponent(),
			WorldPositionComponent(float(rand() % 200) - 100.f, float(rand() % 100) - 50.f, -200.0f),
			PhysicsComponent(),
			//EmitterComponent(EmitterComponent::EmitterType::DEFAULT_EMITTER, 300),
			CirclePhysicsComponent(-1.0f),
			VelocityComponent(35.0f * sin(i+0.7f), 35.0f * cos(i+0.7f)),
			LightComponent( LightType::PointLight, lightPalette.backroundLightMaterial ),
			BackgroundComponent()
			);
	}
	
	/*
	unsigned int LeftFloor = CreateEntity(world.mEntityHandler,
			WorldPositionComponent(-80.0f,0.0f,0.0f),
			ScaleComponent(1.0f,1.0f,1.0f),
			GraphicsComponent(MeshType::PlayerFloor, meshPalette.fieldWallMaterialA)
			);

	unsigned int RightFloor = CreateEntity(world.mEntityHandler,
			WorldPositionComponent(80.0f,0.0f,0.0f),
			ScaleComponent(1.0f,1.0f,1.0f),
			GraphicsComponent(MeshType::PlayerFloor, meshPalette.fieldWallMaterialA )
			);
	*/
	//LoadDemoLevel(world);
	LoadSimpleLevel(world);

    int bop_buffer = AudioCore::loadFile( "assets/sound/paddle_bounce_2.ogg" );
	for (int i = 0; i < 5; i++)
	{
		unsigned int Ball = CreateEntity( world.mEntityHandler,
			GraphicsComponent( MeshType::Ball, meshPalette.ballMaterial ),
			ScaleComponent(),
			WorldPositionComponent(0.0f, 0.0f, 0.0f),
			PhysicsComponent(),
			EmitterComponent(EmitterComponent::EmitterType::DEFAULT_EMITTER, 600),
			CirclePhysicsComponent(1.0f),
			VelocityComponent(70.0f * sin(i+0.7f), 70.0f * cos(i+0.7f)),
			LightComponent( LightType::PointLight, lightPalette.ballLightMaterial ),
            AudioDropComponent( bop_buffer, 0, 100 )
			);
	}
	
	world.mCamera.SetPosition (0, 0, 150);
	glm::vec3 lookAt( 0, 0, 0.05f);
	world.mCamera.UpdateView( lookAt );

	world.mCurrentState = GameState::READY_MENU;
}

void LoadGameSystem::LoadDemoLevel(World& world)
{
	for(int p = -1; p < 2; p += 2)
	{
		for (int i = 0; i < 5; i++)
		{
			int lives = 1;

		

			for (int k = i + 1; k < 7; k++)
			{
				if (k == (i + 1))
					lives = 2;
				else if (i == 0 || i == 4)
					lives = 2;
				else
					lives = 1;

				CreateEntity(world.mEntityHandler,
					WorldPositionComponent(
					80 * p - 3.5f * i * p, 
					45.0f - 7.5f * k, 
					0.0f),
					GraphicsComponent( MeshType::Brick, lives == 1 ? meshPalette.brickMaterialA : meshPalette.brickMaterialB ),
					ScaleComponent(3.0f, 7.0f, 1.0f),
					PlayerComponent(),
					PhysicsComponent(),
					BrickComponent(p== -1 ? 1 : 2),
					RemovableComponent(RemovableComponent::RemoveOn::ON_DEATH),
					LifeComponent(lives),
					AABBComponent(Vector2(3.0f, 7.0f)));
			}
		}
	}
	
	for(int p = -1; p < 2; p += 2)
	{
		for (int i = 0; i < 5; i++)
		{
			int lives = 1;

			for (int k = i + 2; k < 7; k++)
			{
				if (k == i + 2)
					lives = 2;
				else if (i == 0 || i == 4)
					lives = 2;
				else
					lives = 1;

				CreateEntity(world.mEntityHandler,
					WorldPositionComponent(
					80 * p - 3.5f * i * p, 
					-52.5f - 7.5f * -k, 
					0.0f),
					GraphicsComponent( MeshType::Brick, lives == 1 ? meshPalette.brickMaterialA : meshPalette.brickMaterialB ),
					ScaleComponent(3.0f, 7.0f, 1.0f),
					PlayerComponent(),
					PhysicsComponent(),
					BrickComponent(p==-1 ? 1 : 2),
					RemovableComponent(RemovableComponent::RemoveOn::ON_DEATH),
					LifeComponent(lives),
					AABBComponent(Vector2(3.0f, 7.0f)));
			}
		}
	}
	
}

void LoadGameSystem::LoadSimpleLevel(World& world)
{
	for(int p = 0; p < 2; ++p)
	{
		for(int i=0; i < 10; ++i)
		{
			int lives = 0;
				if ( p == 0)
				{
					if (i == 9)
					{
						lives = 2;
					}
					else
						lives = 1;
				}
				else
				{
					if (i == 0)
						lives = 2;
					else
						lives = 1;
				}

			for(int k=0; k < 13; ++k)
			{
				CreateEntity(world.mEntityHandler,
					WorldPositionComponent(-93.5f + 155.0f * p + 3.5f * i , 45.0f - 7.5f * k, 0.0f),
					GraphicsComponent( MeshType::Brick, lives == 1 ? meshPalette.brickMaterialA : meshPalette.brickMaterialB ),
					ScaleComponent(3.0f, 7.0f, 1.0f),
					PlayerComponent(),
					PhysicsComponent(),
					BrickComponent(p==0 ? 1 : 2),
					RemovableComponent(RemovableComponent::RemoveOn::ON_DEATH),
					LifeComponent(lives),
					AABBComponent(Vector2(3.0f, 7.0f)));
			}
		}
	}
	
}
