#include "AudioCollisionDropSystem.hpp"
#include <ComponentFramework/EntityFactory.hpp>

#include <ComponentFramework/World.hpp>
#include <ComponentFramework/EntityHandler.hpp>

#include <cmath>
#include <iostream>

AudioCollisionDropSystem::AudioCollisionDropSystem()
{
    Asp.AddInclusiveAspect( PHYSICS_COMPONENT );
    Asp.AddInclusiveAspect( AUDIO_DROP_COMPONENT );
    Asp.AddInclusiveAspect( WORLD_POSITION_COMPONENT ); 
}

void AudioCollisionDropSystem::Update( World &world )
{
    std::vector<unsigned int> droppedSoundsNew;
    for( auto ent : this->droppedSounds )
    {
            auto * asc = (AudioSourceComponent*)world.mEntityHandler->GetComponent( ent, AUDIO_SOURCE_COMPONENT );

            if( asc != nullptr )
            {
                if( asc->playing == false )
                {
                    world.mEntityHandler->RemoveEntity( ent );
                }
                else
                {
                    droppedSoundsNew.push_back( ent );
                }
            }
    }

    this->droppedSounds = droppedSoundsNew;

    for( auto ent : this->getEntities( world ) )
    {
        auto * pc = (PhysicsComponent*)world.mEntityHandler->GetComponent( ent, PHYSICS_COMPONENT );

        if( this->droppedSounds.size() < MAX_DROPPED_SOUNDS )
        {
            if( pc->hasCollided )
            {

                auto * wpc = (WorldPositionComponent*)world.mEntityHandler->GetComponent( ent, WORLD_POSITION_COMPONENT );
                auto * adc = (AudioDropComponent*)world.mEntityHandler->GetComponent( ent, AUDIO_DROP_COMPONENT );

                this->droppedSounds.push_back( CreateEntity( world.mEntityHandler,
                        WorldPositionComponent( *wpc ),
                        AudioSourceComponent( adc->buffer, true, false ),
                        AudioWorldPanningComponent( adc->center, adc->width ) ) );
            }
        }
    }
}
