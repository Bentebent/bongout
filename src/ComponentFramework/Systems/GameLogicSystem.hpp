#ifndef GAME_LOGIC_SYSTEM_H
#define GAME_LOGIC_SYSTEM_H

#include <ComponentFramework/System.hpp>
#include <ComponentFramework/GameState.hpp>
#include "../../GraphicsSystem/GraphicsMaterial.hpp"

class GameLogicSystem : public System
{
public:
	GameLogicSystem();
	virtual void Update(World& world) override;
private:
	float mTimeLimit;
	float mElapsedTime;
	int mRoundLimit;
	int mRoundsLeft;
	bool mSuddenDeath;
	bool mEndGame;
	GameState mOldState;

	Aspect mBrickAspect;
	Aspect mPlayerAspect;

	void ResetGameLogic(World& world);
	void SuddenDeath(World& world,  std::vector<unsigned int>& entities);
	void WinCondition(World& world, std::vector<unsigned int>& entities);
};

#endif
