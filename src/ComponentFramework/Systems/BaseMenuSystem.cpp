#include "BaseMenuSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <InputDevices.hpp>
#include <iostream>

BaseMenuSystem::BaseMenuSystem()
{
}

void BaseMenuSystem::Update(World& world) 
{
	if (mFirstEnter)
	{
		std::vector<unsigned int> entities = this->getEntities( world );

		for (auto it : entities)
		{
			WorldPositionComponent* worldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
			MenuComponent* menuComponent = (MenuComponent*)world.mEntityHandler->GetComponent(it, MENU_COMPONENT);
			ScaleComponent* scaleComponent = (ScaleComponent*)world.mEntityHandler->GetComponent(it, SCALE_COMPONENT);

			worldPos->z = menuComponent->initialZ;
			scaleComponent->x = menuComponent->defaultScale;
			scaleComponent->y = menuComponent->defaultScale;
			scaleComponent->z = menuComponent->defaultScale;
		}
		mFirstEnter = false;
	}
}

float BaseMenuSystem::WeightedAverage(float pos, float N, float W)
{
	return pos = ((pos * ( N - 1.0f )) + W ) / N;
}

float BaseMenuSystem::Pulse(float pos, float N, float W, float offset, float elapsedTime)
{
	float temp = 1.2f + sin (3.0f * elapsedTime + offset) * 0.2f;
	float output = WeightedAverage(pos, N, W * temp);
	return output;
}