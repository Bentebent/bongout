#ifndef WORLD_HPP
#define WORLD_HPP

#include <GraphicsSystem/Camera.hpp>
#include "Aspect.hpp"
#include "GameState.hpp"

class SystemHandler;
class EntityHandler;
struct InputDevices;


struct GameData
{
	GameData()
	{
		mWinner = 0;
		mPlayerOneRoundsWon = 0;
		mPlayerTwoRoundsWon = 0;
	};

	int mWinner;
	int mPlayerOneRoundsWon;
	int mPlayerTwoRoundsWon;
};

class World
{
public:
	World(InputDevices* InputDevices);
	~World();

	SystemHandler* mSystemHandler;
    EntityHandler* mEntityHandler;
	Camera mCamera;
	float mDelta;
	float mElapsedTime;
	GameState mCurrentState;
	GameState mPrevState;

	GameData mGameData;
	Aspect mGameplayAspect;
	Aspect mMainMenuAspect;
	Aspect mEndMenuAspect;
	Aspect mReadyMenuAspect;
	Aspect mRoundOverMenuAspect;

	void ResetGameData();
};
#endif
