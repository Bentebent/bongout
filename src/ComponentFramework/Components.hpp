#ifndef SRC_COMPONENTS_H
#define SRC_COMPONENTS_H

#include <Utility/Physics.hpp>
#include <GraphicsSystem/GraphicsMaterial.hpp>

#include <Audio/AudioBuffer.hpp>

enum ComponentID
{
	WORLD_POSITION_COMPONENT = 0,
	SCALE_COMPONENT,
	SCREEN_POSITION_COMPONENT,
	VELOCITY_COMPONENT,
	AABB_COMPONENT,

    TEST_COMPONENT,
	PLAYER_COMPONENT,
	INPUT_COMPONENT,
	MENU_COMPONENT,
	MAIN_MENU_COMPONENT,

	READY_MENU_COMPONENT,
	ROUND_OVER_MENU_COMPONENT,
	END_MENU_COMPONENT,
	PHYSICS_COMPONENT,
	CIRCLE_PHYSICS_COMPONENT,

	GRAPHICS_COMPONENT,
	LIGHT_COMPONENT,
	BRICK_COMPONENT,
	REMOVABLE_COMPONENT,
	BACKGROUND_COMPONENT,

    AUDIO_SOURCE_COMPONENT,
    AUDIO_WORLD_PANNING_COMPONENT,
    AUDIO_DROP_COMPONENT, 
	LIFE_COMPONENT,
	EMITTER_COMPONENT,

	TIMER_COMPONENT,

	COMPONENT_COUNT
};


struct Component
{
	static ComponentID ID;
};

struct WorldPositionComponent : Component
{
	static const ComponentID ID = WORLD_POSITION_COMPONENT;
	WorldPositionComponent();
	WorldPositionComponent(float x, float y, float z);
	float x;
	float y;
	float z;
};

struct ScaleComponent : Component
{
	static const ComponentID ID = SCALE_COMPONENT;
	ScaleComponent();
	ScaleComponent(float x, float y, float z);
	float x;
	float y;
	float z;
};

struct ScreenPositionComponent : Component
{
	static const ComponentID ID = SCREEN_POSITION_COMPONENT;
	ScreenPositionComponent();
	ScreenPositionComponent(int x, int y);
	int x;
	int y;
};

struct VelocityComponent : Component
{
	static const ComponentID ID = VELOCITY_COMPONENT;
	VelocityComponent();
	VelocityComponent(float x, float y);
	float x;
	float y;
};

struct AABBComponent: Component
{
	static const ComponentID ID = AABB_COMPONENT;
	AABB2 aabb;

	AABBComponent();
	AABBComponent(Vector2 size);
	AABBComponent(Vector2 topLeft, Vector2 bottomRight);
};

struct TestComponent : Component
{
	static const ComponentID ID = TEST_COMPONENT;
    float g;
};

struct PlayerComponent : Component
{
	static const ComponentID ID = PLAYER_COMPONENT;
	PlayerComponent();
	PlayerComponent(int id, int score);
	int playerID;
	int score;
};

struct InputComponent : Component
{
	static const ComponentID ID = INPUT_COMPONENT;
	InputComponent();
	InputComponent(int inputDevId, int keyBindUp, int keyBindDown);
	int inputDeviceID;
	int keyBindUp;
	int keyBindDown;
};

struct PhysicsComponent : Component
{
	static const ComponentID ID = PHYSICS_COMPONENT;
	PhysicsComponent();
	bool hasCollided;
	float collisionNormalx;
	float collisionNormaly;
	float collisionTime;
};

struct CirclePhysicsComponent : Component
{
	static const ComponentID ID = CIRCLE_PHYSICS_COMPONENT;
	CirclePhysicsComponent();
	CirclePhysicsComponent(float radius);
	float radius;
};


struct GraphicsComponent : Component
{
	static const ComponentID ID = GRAPHICS_COMPONENT;

	GraphicsComponent();
	GraphicsComponent( MeshType type );
	GraphicsComponent( MeshType type, MeshMaterial material );
	MeshType meshType;
	MeshMaterial meshMaterial;
	bool renderMe;
};

struct LightComponent : Component
{
	static const ComponentID ID = LIGHT_COMPONENT;
	LightComponent();
	LightComponent( LightType type, LightMaterial material );
	LightType lightType;
	LightMaterial lightMaterial;
};

struct BrickComponent : Component
{
	static const ComponentID ID = BRICK_COMPONENT;
	BrickComponent();
	BrickComponent(unsigned int owner);
	unsigned int OwnerID;
};

struct MenuComponent : Component
{
	static const ComponentID ID = MENU_COMPONENT;
	MenuComponent();
	MenuComponent( float x, float y, float z, float scale, float N, float W, float initZ );
	float defaultPosX;
	float defaultPosY;
	float defaultPosZ;
	float defaultScale;
	float popN;
	float popW;
	float initialZ;
};

struct MainMenuComponent : Component
{
	static const ComponentID ID = MAIN_MENU_COMPONENT;
	MainMenuComponent();
	MainMenuComponent(int id);
	int id;
};

struct EndMenuComponent : Component
{
	static const ComponentID ID = END_MENU_COMPONENT;
	EndMenuComponent();
	EndMenuComponent(int id);
	int id;
};

struct ReadyMenuComponent : Component
{
	static const ComponentID ID = READY_MENU_COMPONENT;
	ReadyMenuComponent();
	ReadyMenuComponent(int id, float stayTimer);
	int id;
	float stayTimer;
	float elapsedTime;
};

struct RoundOverMenuComponent : Component
{
	static const ComponentID ID = ROUND_OVER_MENU_COMPONENT;
	RoundOverMenuComponent();
	RoundOverMenuComponent(int id);
	int id;
};

struct RemovableComponent : Component
{
	static const ComponentID ID = REMOVABLE_COMPONENT;
	enum RemoveOn
	{
		ON_NONE,
		ON_COLLISION,
		ON_DEATH,
		ON_TIMER
	}removeOn;

	RemovableComponent();
	RemovableComponent(RemoveOn On);
};

struct AudioSourceComponent : Component
{
    AudioSourceComponent( int );
    AudioSourceComponent( int buffer, bool playing, bool looping );
	AudioSourceComponent( int buffer, bool playing, bool looping, long unsigned int t );
    static const ComponentID ID = AUDIO_SOURCE_COMPONENT;
    bool playing, looping;
    float panning;
    long unsigned int timer;
    float amplitude;
    int buffer;
};

struct AudioWorldPanningComponent : Component
{
    AudioWorldPanningComponent( float center, float width );
    static const ComponentID ID = AUDIO_WORLD_PANNING_COMPONENT;
    float x_center;
    float x_width;
};

struct AudioDropComponent : Component
{
    AudioDropComponent( int buffer, float center, float width );
    static const ComponentID ID = AUDIO_DROP_COMPONENT;
    int buffer;
    float center;
    float width;
};

struct LifeComponent : Component
{
	static const ComponentID ID = LIFE_COMPONENT;
	int lives;

	LifeComponent();
	LifeComponent(int lives);
};

struct BackgroundComponent : Component
{
	static const ComponentID ID = BACKGROUND_COMPONENT;
	BackgroundComponent();
};

struct EmitterComponent : Component
{
	static const ComponentID ID = EMITTER_COMPONENT;
	enum EmitterType
	{
		DEFAULT_EMITTER = 0,
		NUMBER_OF_EMITTERS			
	}emitterType;

	EmitterComponent();
	EmitterComponent(EmitterType type, int ParticlesPerSec, float Offset_x = 0.0f, float Offset_y = 0.0f, float Offset_z = 0.0f);
	int ParticlesPerSecond;
	float Offset_x;
	float Offset_y;
	float Offset_z;
};

struct TimerComponent : Component
{
	static const ComponentID ID = TIMER_COMPONENT;
	enum TimerType
	{
		NO_TIMER,
		COMPONENT_REMOVE
	}timerType;


	float Time;
	void* UserData;

	TimerComponent();
	TimerComponent(TimerType type, float time, void* userData);
};

#endif

