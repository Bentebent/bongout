#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <vector>

#include <ComponentFramework/Aspect.hpp>

class World;

class System
{
public:
	System();
	virtual ~System();

	virtual void Update(World &world) = 0;
	virtual void onComponentAdd(unsigned int entityId, ComponentID componentId, Component* cmp);
	virtual void onComponentRemove(unsigned int entityId, ComponentID componentId, Component* cmp);
	void onEntityAspectChange(unsigned int entityId, m_int AspectMask);

protected:
	friend class SystemHandler;
	std::vector<unsigned int> getEntities(World& world);
	std::vector<unsigned int> EntityList;
	Aspect Asp;
};
#endif
