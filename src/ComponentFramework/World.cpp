#include "World.hpp"
#include "SystemHandler.hpp"
#include "EntityHandler.hpp"

World::World(InputDevices* InputDevices)
{
	mSystemHandler = new SystemHandler(InputDevices);
    mEntityHandler = new EntityHandler(this);
	mCurrentState = GameState::LOAD_MENU;
	mPrevState = mCurrentState;

	mGameplayAspect.AddExclusiveAspect(MENU_COMPONENT);
	mGameplayAspect.AddInclusiveAspect(GRAPHICS_COMPONENT);

	mMainMenuAspect.AddInclusiveAspect(MAIN_MENU_COMPONENT);

	mEndMenuAspect.AddInclusiveAspect(END_MENU_COMPONENT);

	mReadyMenuAspect.AddExclusiveAspect(MAIN_MENU_COMPONENT);
	mReadyMenuAspect.AddExclusiveAspect(END_MENU_COMPONENT);
	mReadyMenuAspect.AddExclusiveAspect(ROUND_OVER_MENU_COMPONENT);
	mReadyMenuAspect.AddInclusiveAspect(GRAPHICS_COMPONENT);

	mRoundOverMenuAspect.AddExclusiveAspect(MAIN_MENU_COMPONENT);
	mRoundOverMenuAspect.AddExclusiveAspect(END_MENU_COMPONENT);
	mRoundOverMenuAspect.AddExclusiveAspect(READY_MENU_COMPONENT);
	mRoundOverMenuAspect.AddInclusiveAspect(GRAPHICS_COMPONENT);

	mElapsedTime = 0;
}

World::~World()
{
	delete mSystemHandler;
    delete mEntityHandler;
}

void World::ResetGameData()
{
		//mGameData.mWinner = 0;
		mGameData.mPlayerOneRoundsWon = 0;
		mGameData.mPlayerTwoRoundsWon = 0;
}
