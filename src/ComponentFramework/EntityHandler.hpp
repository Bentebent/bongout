#ifndef ENTITYHANDLER_HPP
#define ENTITYHANDLER_HPP

#include <vector>
#include "SharedAspectType.hpp"
#include "Components.hpp"

class ChunkVector;
class Aspect;
class World;

class EntityHandler
{
public:
	EntityHandler(World* world);
	~EntityHandler();

	unsigned int CreateEntity();
	void RemoveEntity( unsigned int entity );

	void AddComponent( unsigned int entityId, ComponentID type, Component* Component);
	void RemoveComponent( unsigned int entity_id, ComponentID type );

    Component* GetComponent( unsigned int entity_id, ComponentID type );

    std::vector<unsigned int> GetEntities( Aspect &aspect );

	 m_int GetEntityAspect( unsigned int entity_id ); 

private:
    friend class Aspect;
	World* world;
   
    m_int GetComponentAspect( ComponentID component );

	std::vector<ChunkVector*> components;
	std::vector<std::vector<unsigned int>*> removedComponents;

	std::vector<std::vector<int>*> entities;
    std::vector<m_int> entityAspects;
	std::vector<unsigned int> removedIndices;

    std::vector<int> componentSizes;
    //std::vector<void*> componentDefaults;
};
#endif
