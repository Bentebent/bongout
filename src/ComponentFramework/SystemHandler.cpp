#include "SystemHandler.hpp"
#include "System.hpp"

#include "ExampleSystem.hpp" 

#include <ComponentFramework/Systems/CollisionSystem.hpp>
#include <ComponentFramework/Systems/PhysicsSystem.hpp>
#include <ComponentFramework/Systems/PlayerInputSystem.hpp>
#include <ComponentFramework/Systems/MainMenuSystem.hpp>
#include <ComponentFramework/Systems/RemovalSystem.hpp>
#include <ComponentFramework/Systems/DamageSystem.hpp>
#include <ComponentFramework/Systems/GameLogicSystem.hpp>
#include <ComponentFramework/Systems/AudioSystem.hpp>
#include <ComponentFramework/Systems/LoadGameSystem.hpp>
#include <ComponentFramework/Systems/AudioWorldPositionPanSystem.hpp>
#include <ComponentFramework/Systems/AudioCollisionDropSystem.hpp>
#include <ComponentFramework/Systems/LoadMenuSystem.hpp>
#include <ComponentFramework/Systems/EndMenuSystem.hpp>
#include <ComponentFramework/Systems/ParticleSystem.hpp>
#include <ComponentFramework/Systems/ReadyMenuSystem.hpp>
#include <ComponentFramework/Systems/RoundOverMenuSystem.hpp>
#include <ComponentFramework/Systems/TimerSystem.hpp>
#include <ComponentFramework/Systems/ParticleCollisionSystem.hpp>
#include <ComponentFramework/Systems/BackgroundLightSystem.hpp>

SystemHandler::SystemHandler(InputDevices* InputDevices)
{
    mSystems.push_back( new ExampleSystem() );
	mSystems.push_back( new PlayerInputSystem(InputDevices) );
	mSystems.push_back( new MainMenuSystem(InputDevices) );
	mSystems.push_back( new ReadyMenuSystem() );
	mSystems.push_back( new RoundOverMenuSystem() );
	mSystems.push_back( new EndMenuSystem(InputDevices) );
	mSystems.push_back( new LoadMenuSystem() );
	mSystems.push_back( new LoadGameSystem() );
	mSystems.push_back( new GameLogicSystem() );
	mSystems.push_back( new CollisionSystem() );
	mSystems.push_back( new DamageSystem() );
	mSystems.push_back( new PhysicsSystem() );
	mSystems.push_back( new BackgroundLightSystem() );
    mSystems.push_back( new AudioCollisionDropSystem() );
    mSystems.push_back( new AudioWorldPositionPanSystem() );
    mSystems.push_back( new AudioSystem() );
	mSystems.push_back ( new ParticleCollisionSystem() );
	mSystems.push_back( new TimerSystem() );	
	this->mParticleSystem = new ParticleSystem();
	mSystems.push_back(this->mParticleSystem);
	mSystems.push_back( new RemovalSystem() ); //Add this last
}

SystemHandler::~SystemHandler()
{
}

void SystemHandler::Update(World& world)
{
	for(auto it : mSystems)
	{
		it->Update(world);
	}
}


void SystemHandler::onComponentAdd(unsigned int entityId, ComponentID componentId, Component* cmp, m_int AspectMask)
{
	for(auto it : mSystems)
	{
		if(it->Asp.Matches(AspectMask))
		{
			it->onComponentAdd(entityId, componentId, cmp);
		}
	}
}

void SystemHandler::onComponentRemove(unsigned int entityId, ComponentID componentId, Component* cmp, m_int AspectMask)
{
	for(auto it : mSystems)
	{
		if(it->Asp.Matches(AspectMask))
		{		
			it->onComponentRemove(entityId, componentId, cmp);
		}
	}
}

void SystemHandler::onEntityAspectChange(unsigned int entityId, m_int AspectMask)
{
	for(auto it : mSystems)
	{			
		it->onEntityAspectChange(entityId, AspectMask);
	}
}
