#include "AudioRawPCMData.hpp"
#include <iostream>
#include <assert.h>

AudioRawPCMData::AudioRawPCMData()
{
}

AudioRawPCMData::~AudioRawPCMData()
{
}

void AudioRawPCMData::setData( short *data, int length, int channels, int samplerate )
{
    this->data.clear(); 
    this->data.reserve( length*channels );

    for( int i = 0; i < length*channels; i++ )
    {
        this->data.push_back( data[i] ); 
    }
    
    this->channels = channels;
    this->sample_rate = samplerate;

    this->quality = INTEGER_16;
}

std::vector<short> AudioRawPCMData::getData( unsigned int from, unsigned int size ) const
{
    std::vector<short> return_data;
    for( unsigned int i = 0; i < size ; i++ )
    {
#ifdef ROTATING_SOUND
        int index = (i+from) >= this->data.size() ? (i+from) - this->data.size() : (i+from);
#else //Stick on the last sample value.
        int index = (i+from) >= this->data.size() ? this->data.size()-1 : (i+from);
#endif
        return_data.push_back( this->data[index] ); 

    }

    return return_data;
}

unsigned AudioRawPCMData::getSampleCount() const
{
    return this->data.size();
}

int AudioRawPCMData::getChannels() const
{
    return this->channels;
}

int AudioRawPCMData::getSamplerate() const
{
    return this->sample_rate;
}

const short AudioRawPCMData::getSample(unsigned int index) const
{
	return this->data[index];
}

void AudioRawPCMData::makeStereo()
{
    if( this->channels == 1 )
    {
        std::vector<short> old = this->data;  

        this->data.clear();

        for( int i = 0; i < old.size(); i++ )
        {
            this->data.push_back( old[i] );
            this->data.push_back( old[i] );
        }

        this->channels = 2;
    }
}
