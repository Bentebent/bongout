#include "AudioCore.hpp"
#include "AudioBuffer.hpp"

#include <Exception/AudioException.hpp>
#include <iostream>
#include "stb_vorbis.h"

std::vector<AudioBuffer> AudioCore::dataBuffers = std::vector<AudioBuffer>();


int AudioCore::paCallback( const void *input, void *output, unsigned long frameCount, 
const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData )
{
    float* dataOut = static_cast<float*>(output);
    AudioCore* ac = static_cast<AudioCore*>(userData);

    AudioStreamBuffer *filledStream = ac->dequeueFilledBuffer();

    if( filledStream != nullptr )
    {
        for(  int i = 0; i < filledStream->size(); i++  )
        {
            dataOut[i] = (*filledStream)[i]; 
        }

        ac->queueEmptyBuffer( filledStream );
    }
    else
    {
        for(  int i = 0; i < filledStream->size(); i++  )
        {
            dataOut[i] = 0;
        }
    }

    return paContinue;
}

AudioCore::AudioCore()
{
    this->isPlaying = false;
    int err = Pa_Initialize();
    if( err )
        std::cerr << "PortAudio initialize error:" << Pa_GetErrorText( err ) << std::endl;

    for( int i = 0; i < BUFFER_COUNT; i++ )
    {
        emptyQueue.push( &buffers[i] );
    }

    err = Pa_OpenDefaultStream( &outputStream,
                                0,          /* no input channels */
                                OUTPUT_CHANNELS,          /* stereo output */
                                PA_OUTPUT_TYPE, 
                                STREAM_RATE,
                                FRAMES_PER_BUFFER,        /* frames per buffer, i.e. the number
                                                   of sample frames that PortAudio will
                                                   request from the callback. Many apps
                                                   may want to use
                                                   paFramesPerBufferUnspecified, which
                                                   tells PortAudio to pick the best,
                                                   possibly changing, buffer size.*/
                                paCallback, /* this is your callback function */
                                this );

    if( err )
        std::cerr << "PortAudio initialize error:" << Pa_GetErrorText( err ) << std::endl;
}

AudioCore::~AudioCore()
{
    
    int err = Pa_Terminate();
    std::cerr << "PortAudio terminate error:" << Pa_GetErrorText( err ) << std::endl;
}

int AudioCore::loadFile( std::string path )
{
    std::cout << "Loading sound '"  << path << "' ";
    for( int i = 0; i < AudioCore::dataBuffers.size(); i++ )
    {
        if( AudioCore::dataBuffers[i].name == path )
        {
            std::cout << " Sound already found in cache. Done" << std::endl;
            return i;
        }
    }

	int stb_error;
    int channels;

    std::shared_ptr<AudioRawPCMData> target(new AudioRawPCMData);

	stb_vorbis * stb_vorbis_file = stb_vorbis_open_filename( path.c_str(), &stb_error, NULL );

	if( stb_vorbis_file )
	{
		stb_vorbis_info stb_vorbis_file_info = stb_vorbis_get_info( stb_vorbis_file );

		if( stb_vorbis_file_info.channels >= 3 )
		{
			stb_vorbis_close( stb_vorbis_file );

			throw AudioException( AudioException::LOAD_OGG_CHANNEL_COUNT_ERROR );
		}
        else if( stb_vorbis_file_info.channels >= 2 )
        {
            channels = 2;
        }
		else if( stb_vorbis_file_info.channels == 1 )
		{
			channels = 1;
		}
        else
        {
            //Should be impossible, unless there's a bug in stb_vorbis
            throw AudioException( AudioException::LOAD_OGG_CHANNEL_COUNT_ERROR );
        }

		int sample_rate = stb_vorbis_file_info.sample_rate;
		stb_vorbis_close( stb_vorbis_file );
		stb_vorbis_file = NULL;

		short *vorbis_data;

		int samples = stb_vorbis_decode_filename( path.c_str(), &channels, &vorbis_data );

		if( samples > 0 )
		{
            target->setData( vorbis_data, samples, channels, sample_rate );

			free( vorbis_data );
		}
		else
		{
			free( vorbis_data );
		
			throw AudioException( AudioException::LOAD_OGG_ERROR );
		}
	}
	else
	{
		if( stb_error == VORBIS_file_open_failure )
		{
			//Couldn't open file.
		}
		throw AudioException( AudioException::OPEN_OGG_ERROR );
	}

    if( target->getChannels() < 2 )
        target->makeStereo();

    AudioCore::dataBuffers.push_back( AudioBuffer( target, path ) );

    std::cout << "... Done" << std::endl;

    return  AudioCore::dataBuffers.size() - 1;

}

AudioStreamBuffer* AudioCore::dequeueFilledBuffer()
{
    AudioStreamBuffer * buffer = nullptr;
    this->filledQueueLock.lock();
    if( this->filledQueue.empty() == false )
    {
        buffer = this->filledQueue.front();
        this->filledQueue.pop();
    }
    this->filledQueueLock.unlock();
    return buffer;
}

AudioStreamBuffer* AudioCore::dequeueEmptyBuffer()
{
    AudioStreamBuffer* id = nullptr;
    this->emptyQueueLock.lock();     
    if( this->emptyQueue.empty() == false )
    {
        id = this->emptyQueue.front();
        this->emptyQueue.pop();
    }
    this->emptyQueueLock.unlock();
    return id;

}

void AudioCore::queueFilledBuffer( AudioStreamBuffer* buffer )
{
    this->filledQueueLock.lock();
    this->filledQueue.push( buffer );
    this->filledQueueLock.unlock();

}

void AudioCore::queueEmptyBuffer( AudioStreamBuffer* buffer )
{
    this->emptyQueueLock.lock();
    this->emptyQueue.push( buffer );
    this->emptyQueueLock.unlock();
}

int AudioCore::getFilledQueueCount()
{
    int ret;
    this->filledQueueLock.lock();
    ret = this->filledQueue.size();
    this->filledQueueLock.unlock();
    return ret;
}

void AudioCore::tellAreBuffersComing( bool value )
{
    if( this->isPlaying == false && value )
    {
        Pa_StartStream( this->outputStream );
        this->isPlaying = true;
        std::cout << "Starting the streaming of sound" << std::endl;
    }
    else if( value == false && this->isPlaying && this->getFilledQueueCount() == 0 )
    {
        Pa_StopStream( this->outputStream );
        this->isPlaying = false;
        std::cout << "Ending the streaming of sound" << std::endl;
    }
}
