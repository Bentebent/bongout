#ifndef RAW_PCM_DATA_H
#define RAW_PCM_DATA_H
#include <vector>

class AudioRawPCMData
{
    enum Quality
    {
        INTEGER_16
    };

    public:
        AudioRawPCMData();
        ~AudioRawPCMData();

        void setData( short *data, int length, int channels, int samplerate );

        std::vector<short> getData( unsigned int from, unsigned int size ) const;
        unsigned int getSampleCount() const;
        int getChannels() const;
        int getSamplerate() const;

		const short getSample(unsigned int index) const;

        void makeStereo();
    private:
        std::vector<short> data;
        int channels;
        int sample_rate;
        Quality quality;
};

#endif
