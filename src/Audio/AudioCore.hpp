#ifndef SRC_AUDIO_AUDIOCORE_H
#define SRC_AUDIO_AUDIOCORE_H
#include <string>
#include <portaudio.h>
#include <string>
#include <vector>
#include <memory>
#include <array>
#include <mutex>
#include <utility>
#include <queue>

class AudioBuffer;
class AudioSource;
class AudioSourceData;

#define STREAM_RATE 44100
#define FRAMES_PER_BUFFER 1024
#define OUTPUT_CHANNELS 2
#define BUFFER_COUNT 3

typedef float pa_value;
#define PA_OUTPUT_TYPE paFloat32

typedef std::array<pa_value,FRAMES_PER_BUFFER*OUTPUT_CHANNELS> AudioStreamBuffer;

class AudioCore
{
public:
    AudioCore();
    ~AudioCore(); 

    static int loadFile( std::string path );
private:

    friend class AudioSystem;
    static int paCallback( const void *input, void *output, unsigned long frameCount, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData );

    PaStream * outputStream;
    std::vector<std::shared_ptr<AudioSourceData>> sources;
    AudioStreamBuffer buffers[BUFFER_COUNT];

    static std::vector<AudioBuffer> dataBuffers;
    //True means that a buffer has been read by pa
    std::queue<AudioStreamBuffer*> emptyQueue;
    std::queue<AudioStreamBuffer*> filledQueue;

    std::mutex emptyQueueLock;
    std::mutex filledQueueLock;
    
    int currentRead;

    AudioStreamBuffer* dequeueFilledBuffer();
    AudioStreamBuffer* dequeueEmptyBuffer();

    void queueFilledBuffer( AudioStreamBuffer* buffer );
    void queueEmptyBuffer( AudioStreamBuffer* buffer );

    int getFilledQueueCount();
    
    bool isPlaying;
    
    void tellAreBuffersComing( bool value );
};


#endif

