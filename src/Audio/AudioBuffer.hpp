#ifndef SRC_AUDIO_AUDIOBUFFER_H
#define SRC_AUDIO_AUDIOBUFFER_H

#include "AudioRawPCMData.hpp"
#include <memory>
#include <string>

class AudioCore;

class AudioBuffer
{
        AudioBuffer();

    private:
        friend class AudioCore;
        friend class AudioSystem;
        AudioBuffer( std::shared_ptr<AudioRawPCMData> dataBuffer, std::string name);
        std::shared_ptr<AudioRawPCMData> dataBuffer;
        std::string name;
};
#endif
