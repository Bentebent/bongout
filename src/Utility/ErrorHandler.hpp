#ifndef ERRORHANDLER_HPP
#define ERRORHANDLER_HPP

#include <string>

class ErrorHandler
{
public:

	void Throw( std::string );
	void CheckOpenGLError();

	static ErrorHandler& Get();

private:

	ErrorHandler();
	~ErrorHandler();
};



#endif