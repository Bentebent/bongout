#include "ErrorHandler.hpp"

#include <iostream> 


ErrorHandler& ErrorHandler::Get()
{
	static ErrorHandler e;
	return e;
}


ErrorHandler::ErrorHandler()
{
}

ErrorHandler::~ErrorHandler()
{
}

void ErrorHandler::Throw( std::string error )
{ 
	std::cout << error << std::endl;
}

