#include "Physics.hpp"

#include <limits>
#include <cmath>

// DEBUG
#include <ComponentFramework/Systems/PlayerInputSystem.hpp>

#define FLT_EPSILON 0.0000001f
 
Physics* Physics::sInstance = nullptr;
 
Vector2::Vector2() : x(0.0f), y(0.0f)
{}
 
Vector2::Vector2(float xVal, float yVal) : x(xVal), y(yVal)
{}

void Vector2::Normalise()
{
	float length = GetLength();
	x /= length;
	y /= length;
}

float Vector2::GetLength() const
{
	return sqrt(GetLengthSquared());
}

float Vector2::GetLengthSquared() const
{
	return (x * x + y * y);
}

Vector2 operator+(const Vector2& lhs, const Vector2& rhs)
{
	return Vector2(lhs.x + rhs.x, lhs.y + rhs.y);
}

Vector2 operator-(const Vector2& lhs, const Vector2& rhs)
{
	return Vector2(lhs.x - rhs.x, lhs.y - rhs.y);
}

Vector2 operator*(const Vector2& lhs, const float& rhs)
{
	return Vector2(lhs.x * rhs, lhs.y * rhs);
}

Vector2 operator/(const Vector2& lhs, const float& rhs)
{
	return Vector2(lhs.x / rhs, lhs.y / rhs);
}

void Vector2::operator*=(const float& rhs)
{
	x *= rhs;
	y *= rhs;
}

void Vector2::operator/=(const float& rhs)
{
	x /= rhs;
	y /= rhs;
}

Vector3::Vector3() : x(0.0f), y(0.0f), z(0.0f)
{}

Vector3::Vector3(float xVal, float yVal, float zVal) : x(xVal), y(yVal), z(zVal)
{}

Ray2::Ray2() : origin(0.0f, 0.0f), direction(0.0f, 0.0f)
{}

Ray2::Ray2(Vector2 rayOrigin, Vector2 rayDirection) : origin(rayOrigin), direction(rayDirection)
{
	direction.Normalise();
}
 
AABB2::AABB2() : topLeft(0.0f, 0.0f), bottomRight(0.0f, 0.0f)
{}

AABB2::AABB2(Vector2 topLeftCorner, Vector2 bottomRightCorner) 
	: topLeft(topLeftCorner), bottomRight(bottomRightCorner)
{}

Vector2 AABB2::GetSideNormal(Side side)
{
	float sqrt2Inv = 1.0f / sqrt(2);

	switch (side)
	{
		case Top:
			return Vector2(0.0f, -1.0f);
		case Right:
			return Vector2(1.0f, 0.0f);
		case Bottom:
			return Vector2(0.0f, 1.0f);
		case Left:
			return Vector2(-1.0f, 0.0f);
		case Corner:
			return Vector2(sqrt2Inv, sqrt2Inv);
		default:
			return Vector2(0.0f, 0.0f);
	}
}
 
Circle::Circle() : position(), radius(0.0f)
{}

Circle::Circle(Vector2 circlePosition, float circleRadius) : position(circlePosition),
	radius (circleRadius)
{}
 
Physics& Physics::GetInstance()
{
        if(sInstance == nullptr)
			sInstance = new Physics();
 
        return *sInstance;
}
 
Physics::Physics()
{
}

Physics::~Physics()
{
	sInstance = nullptr;
}

AABB2::Side Physics::CollisionMovingCircleVsAABB2(const Circle& circle, const Vector2& circleVelocity,
	const AABB2& aabb, const float maxTOC, float& outTOC, Vector2& outCollisionPoint) const
{
	Vector2 circleRadius = Vector2(circle.radius, circle.radius);
	AABB2 extendedAABB = AABB2(aabb.topLeft - circleRadius, aabb.bottomRight + circleRadius);
	outTOC = -1.0f;

	if (!CollisionRayVsAABB2(Ray2(circle.position, circleVelocity), extendedAABB, outTOC, outCollisionPoint))
		return AABB2::SideNone;

	outTOC /= circleVelocity.GetLength();

	if (outTOC > maxTOC)
		return AABB2::SideNone;

	// DEBUG
	if (PlayerInputSystem::sShowDebugText)
		std::cout << "CollisionPoint: (" << outCollisionPoint.x << ", " << outCollisionPoint.y << ")" << std::endl;

	int mins = 0;
	int maxes = 0;

	if (outCollisionPoint.x < aabb.topLeft.x) mins |= 1;
	if (outCollisionPoint.x > aabb.bottomRight.x) maxes |= 1;
	if (outCollisionPoint.y < aabb.topLeft.y) mins |= 2;
	if (outCollisionPoint.y > aabb.bottomRight.y) maxes |= 2;

	int bitmask = mins + maxes;

	if (bitmask == 3)
	{
		// Save box corners.
		// 0             1
		// *-------------*
		// |             |
		// *-------------*
		// 3             2

		Vector2* boxCorners = new Vector2[4];
		boxCorners[0] = aabb.topLeft;
		boxCorners[1] = Vector2(aabb.bottomRight.x, aabb.topLeft.y);
		boxCorners[2] = aabb.bottomRight;
		boxCorners[3] = Vector2(aabb.topLeft.x, aabb.bottomRight.y);

		float minDist = std::numeric_limits<float>::max();

		for (int i = 0; i < 4; ++i)
		{
			float currDist = (boxCorners[i] - circle.position).GetLength();
			if (currDist < minDist)
				minDist = currDist;
		}

		if (minDist <= circle.radius)
			return AABB2::Corner;
		else
		{
			
			return AABB2::SideNone;
		}
	}

	if (mins == 1)
		return AABB2::Left;
	else if (mins == 2)
		return AABB2::Top;
	else if (maxes == 1)
		return AABB2::Right;
	else if (maxes == 2)
		return AABB2::Bottom;

	return AABB2::Inside;
}

bool Physics::CollisionRayVsAABB2(const Ray2& ray, const AABB2& aabb, float& outTOC,
								  Vector2& outCollisionPoint) const
{
	Vector2 aabbHalfWidths = Vector2((aabb.bottomRight.x - aabb.topLeft.x) * 0.5f,
								   (aabb.bottomRight.y - aabb.topLeft.y) * 0.5f);
	Vector2 aabbCenter = aabb.topLeft + aabbHalfWidths;
	Vector2 aabbAxes [] = { Vector2(1.0f, 1.0f), Vector2(0.0f, 1.0f) };

	float minTime = 0.0f;
	float maxTime = std::numeric_limits<float>::max();

	Vector2 rayToBox = aabbCenter - ray.origin;

	for (int i = 0; i < 2; ++i)
	{
		float e = rayToBox.m[i];
		float f = ray.direction.m[i];
		float fInv = 1.0f / f;

		if (abs(f) > FLT_EPSILON) // Make sure ray is not parallell to box side.
		{
			float t1 = (e + aabbHalfWidths.m[i]) * fInv;
			float t2 = (e - aabbHalfWidths.m[i]) * fInv;

			if (t1 > t2) // Make sure t1 is smaller than t2
			{
				float temp = t1;
				t1 = t2;
				t2 = temp;
			}

			if (t1 > minTime)
				minTime = t1;
			if (t2 < maxTime)
				maxTime = t2;

			// No interserction: a) the ray misses the aabb; b) the aabb is behind the ray.
			if (minTime > maxTime || maxTime < 0.0)
			{
				outTOC = -1.0f;
				outCollisionPoint = Vector2(0.0f, 0.0f);
				return false;
			}
		}
		else if (-e - aabbHalfWidths.m[i] > 0.0f || -e + aabbHalfWidths.m[i] < 0.0f)
		{
			// The ray is parallel to the side, so it cannot intersect it.
			outTOC = -1.0f;
			outCollisionPoint = Vector2(0.0f, 0.0f);
			return false;
		}
	}

	if (minTime >= 0.0f)
		outTOC = minTime;
	else
		outTOC = maxTime;

	outCollisionPoint = ray.origin + (ray.direction * outTOC);

	return true;
}

bool Physics::IntersectPointVsAABB2(const Vector2& point, const AABB2& aabb) const
{
	bool inIntervalX = point.x > aabb.topLeft.x && point.x < aabb.bottomRight.x;
	bool inIntervalY = point.y > aabb.topLeft.y && point.y < aabb.bottomRight.y;

	return inIntervalX && inIntervalY;
}

bool Physics::IntersectAABB2(const AABB2& rect1,  const AABB2& rect2) const
{
        if( (rect1.topLeft.y > rect2.bottomRight.y) ||
                (rect2.topLeft.y > rect2.bottomRight.y) ||
                (rect1.topLeft.x > rect2.bottomRight.x) ||
                (rect2.topLeft.x > rect2.bottomRight.x) )
                return false;
 
        return true;
}
