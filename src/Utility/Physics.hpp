#ifndef PHYSICS_HPP
#define PHYSICS_HPP
 
#include <iostream>
#include <limits>

// Holds an x and a y value. These can be accessed either through the x and y variables, or the
// m[0] and m[1] variables, respectively.
struct Vector2
{
	union
	{
		struct
		{
			float x;
			float y;
		};
		float m[2];
	};

    Vector2();
    Vector2(float xVal, float yVal);

	void Normalise();
	float GetLength() const;
	float GetLengthSquared() const;

	friend Vector2 operator+(const Vector2& lhs, const Vector2& rhs);
	friend Vector2 operator-(const Vector2& lhs, const Vector2& rhs);
	friend Vector2 operator*(const Vector2& lhs, const float& rhs);
	friend Vector2 operator/(const Vector2& lhs, const float& rhs);
	void operator*=(const float& rhs);
	void operator/=(const float& rhs);
};
 
// Holds an x, y and z value. These can be accessed either through the x, y and z variables, or the
// m[0], m[1] and m[2] variables, respectively.
struct Vector3
{
	union
	{
		struct
		{
			float x;
			float y;
			float z;
		};
		
		float m[3];
	};

	Vector3();
	Vector3(float xVal, float yVal, float zVal);
};

// Defines a ray starting at the point origin, with the normalized direction stored in the variable
// direction.
struct Ray2
{
	Vector2 origin;
	Vector2 direction;

	Ray2();
	Ray2(Vector2 rayOrigin, Vector2 rayDirection);
};

// Defines a two-dimensional Axix-Aligned Bounding Box by the position of it top left and bottom
// right vertices.
struct AABB2
{
	enum Side
	{
		Top = 0, Right, Bottom, Left, Corner, Inside, SideNone
	};

    Vector2 topLeft;
    Vector2 bottomRight;
 
    AABB2();
	AABB2(Vector2 topLeftCorner, Vector2 bottomRightCorner);

	// Returns the two-dimensional normal of the side sent in to the function. If it is not a side
	// (Top, Right, Bottom or Left) the normal (0.0f, 0.0f) is returned.
	static Vector2 GetSideNormal(Side side);
};

// Defines a circle by its position and radius.
struct Circle
{
        Vector2 position;
        float radius;
 
        Circle();
		Circle(Vector2 circlePosition, float circleRadius);
};

// Singleton that holds all physics functions used by the game, including collisions.
class Physics
{
public:
    static Physics& GetInstance();

	~Physics();

	// Finds out whether the moving circle will intersect the axis-aligned bounding box before the
	// time given by maxTOC in number of frames. The intersection side is returned from the 
	// function with the time and point of collision stored in outTOC and outCollisionPoint.
	// If no collision occurs, these will have the values -1.0 and (0.0, 0.0), respectively.
	AABB2::Side CollisionMovingCircleVsAABB2(const Circle& circle, const Vector2& circleVelocity,
		const AABB2& aabb, const float maxTOC, float& outTOC, Vector2& outCollisionPoint) const;
	
	// Given a Ray and an AABB2, find out whether the ray intersects the AABB and return true or
	// false. The time and point of collision are returned in the variables toc and collisionPoint,
	// which will contain -1.0 and (0.0, 0.0) respecitvely if no collision occurs.
	bool CollisionRayVsAABB2(const Ray2& ray, const AABB2& aabb, float& outTOC, 
		Vector2& outCollisionPoint) const;

	// Given a point (in Vector2 format) and a 2D axis-aligned bounding box, find out whether the 
	// point lies inside the AABB or not.
    bool IntersectPointVsAABB2(const Vector2& point, const AABB2& aabb) const;

	// Given two 2D axis-aligned bounding boxes, find out whether they intersect at all.
    bool IntersectAABB2(const AABB2& rect1, const AABB2& rect2) const;

 
private:
    static Physics* sInstance;
 
    Physics();
    Physics(const Physics& copy);
    Physics& operator=(const Physics& copy);

};
 
#endif
