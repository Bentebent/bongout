#ifndef GAME_EXCEPTION_H
#define GAME_EXCEPTION_H

#include <exception>

class Exception : public std::exception
{
public:
    //enum for each exception type, which can also be used to determin
    //exception class, useful for logging or other localisation methods
    //for generating a message of some sort.
    enum ExceptionType
    {
        //shouldnt ever be thrown
        UNKNOWN_EXCEPTION = 0,
        //same as above but has a string that may provide some info
        UNKNOWN_EXCEPTION_STR,
        //eg file not found
        FILE_OPEN_ERROR,
        //lexical cast type error
        TYPE_PARSE_ERROR,
        //NOTE: in many cases functions only check and throw this in debug
        INVALID_ARG,
        //an error occured while trying to parse data from a file
        FILE_PARSE_ERROR,
        //ConfigFileHandler.h 
        CONFIG_FILE_HANDLER_ERROR,
        //OpenAL related error
        AUDIO_ERROR,
        //Input error
    };

    virtual ExceptionType getExceptionType()const throw()
    {
        return UNKNOWN_EXCEPTION;
    }

    virtual const char* what()throw(){return "UNKNOWN_EXCEPTION";}
};
#endif
