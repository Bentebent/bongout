#include "AudioException.hpp"


AudioException::AudioException( Reason reason )
{
    this->reason = reason;
}

AudioException::Reason AudioException::getReason()
{
    return this->reason;
}

Exception::ExceptionType AudioException::getExceptionType() const throw()
{
    return Exception::AUDIO_ERROR;
}

const char* AudioException::what() throw()
{
    return "Audio error";
}
