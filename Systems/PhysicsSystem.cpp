#include "PhysicsSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>

// DEBUG
#include <Systems/PlayerInputSystem.hpp>

PhysicsSystem::PhysicsSystem()
{
	Asp.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	Asp.AddInclusiveAspect(VELOCITY_COMPONENT);
	Asp.AddInclusiveAspect(CIRCLE_PHYSICS_COMPONENT);
}

void PhysicsSystem::Update(World &world)
{
	if (!(world.mCurrentState == GameState::GAMEPLAY || world.mCurrentState == GameState::ROUND_OVER_MENU))
		return;

	std::vector<unsigned int> entities = this->getEntities( world );
	for (auto it : entities)
	{
		WorldPositionComponent* circleWorldPos	= (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
		VelocityComponent* circleVelocity		= (VelocityComponent*)world.mEntityHandler->GetComponent(it, VELOCITY_COMPONENT);
		PhysicsComponent* physics				= (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);
		CirclePhysicsComponent* circlePhysics	= (CirclePhysicsComponent*)world.mEntityHandler->GetComponent(it, CIRCLE_PHYSICS_COMPONENT);
		
		
		if(!physics->hasCollided)
		{
			circleWorldPos->x += circleVelocity->x * world.mDelta;
			circleWorldPos->y += circleVelocity->y * world.mDelta;
			
			if (circleWorldPos->x < -115.f)
				circleVelocity->x = abs(circleVelocity->x);
			if (circleWorldPos->x > 115.f)
				circleVelocity->x = abs(circleVelocity->x) * -1.0f;
			
			if (circleWorldPos->y < -55.5f)
				circleVelocity->y = abs(circleVelocity->y);
			if (circleWorldPos->y > 55.5f)
				circleVelocity->y = abs(circleVelocity->y) * -1.0f;

			if (PlayerInputSystem::sShowDebugText)
				std::cout << "Ball position: (" << circleWorldPos->x << ", " << circleWorldPos->y << ")" << std::endl;
		}
		else
		{			
			// Move to collision point
			circleWorldPos->x += circleVelocity->x * world.mDelta * physics->collisionTime;
			circleWorldPos->y += circleVelocity->y * world.mDelta * physics->collisionTime;

			// Change velocity vector
			float remainingTime = 1.0f - physics->collisionTime;			
			if(abs(physics->collisionNormalx) > 0.0001f)
				circleVelocity->x = -circleVelocity->x;
			if(abs(physics->collisionNormaly) > 0.0001f)
				circleVelocity->y = -circleVelocity->y;
			
			// Move to exit point
			circleWorldPos->x += circleVelocity->x * world.mDelta * remainingTime;
			circleWorldPos->y += circleVelocity->y * world.mDelta * remainingTime;
		}
	}

}