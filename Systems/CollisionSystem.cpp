#include "CollisionSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>

// DEBUG
#include <Systems/PlayerInputSystem.hpp>

CollisionSystem::CollisionSystem()
{
	Asp.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	Asp.AddInclusiveAspect(PHYSICS_COMPONENT);
	//Asp.AddInclusiveAspect(VELOCITY_COMPONENT);

	ballAspect.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	ballAspect.AddInclusiveAspect(VELOCITY_COMPONENT);
	ballAspect.AddInclusiveAspect(CIRCLE_PHYSICS_COMPONENT);

	boxAspect.AddInclusiveAspect(WORLD_POSITION_COMPONENT);
	//boxAspect.AddInclusiveAspect(VELOCITY_COMPONENT);
	boxAspect.AddInclusiveAspect(AABB_COMPONENT);
}

void CollisionSystem::Update(World& world)
{
	std::vector<unsigned int> entities = this->getEntities( world );

	// Set the hasCollided flag to false for all entities.
	for (auto it : entities)
	{
		PhysicsComponent* physics = (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);
		physics->hasCollided = false;
	}

	// Check for collisions.
	for (auto it : entities)
	{
		// Only handle balls.
		if ( ballAspect.Matches( world.mEntityHandler->GetEntityAspect( it ) ) )
		{
			WorldPositionComponent* circleWorldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
			VelocityComponent* circleVelocity		= (VelocityComponent*)world.mEntityHandler->GetComponent(it, VELOCITY_COMPONENT);
			PhysicsComponent* physics				= (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);
			CirclePhysicsComponent* circlePhysics = (CirclePhysicsComponent*)world.mEntityHandler->GetComponent(it, CIRCLE_PHYSICS_COMPONENT);
 
			Circle ball = Circle(Vector2(circleWorldPos->x, circleWorldPos->y), circlePhysics->radius);
			Vector2 ballVelocity = Vector2(circleVelocity->x * world.mDelta, circleVelocity->y * world.mDelta);

			Vector2 normal;
			Vector2 collisionPoint;
			float collisionTime = 0.0f;
			float prevCollisionTime = 1.0f;

			// For each ball, loop through all boxes and check for collisions.
			for (int i = 0; i < entities.size(); i++)
			{
				if (boxAspect.Matches(world.mEntityHandler->GetEntityAspect( entities[i] ) ))
				{
					WorldPositionComponent* boxWorldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(entities[i], WORLD_POSITION_COMPONENT);
					AABBComponent* boxAABB = (AABBComponent*)world.mEntityHandler->GetComponent(entities[i], AABB_COMPONENT);
					PhysicsComponent* boxPhysics				= (PhysicsComponent*)world.mEntityHandler->GetComponent(entities[i], PHYSICS_COMPONENT);
					//VelocityComponent* boxVelocity = (VelocityComponent*)world.mEntityHandler->GetComponent(entities[i], VELOCITY_COMPONENT);

					Vector2 aabbHalfSize = Vector2((boxAABB->aabb.bottomRight.x - boxAABB->aabb.topLeft.x) * 0.5f,
												   (boxAABB->aabb.bottomRight.y - boxAABB->aabb.topLeft.y) * 0.5f);
 
					AABB2 box = AABB2(Vector2(boxWorldPos->x - aabbHalfSize.x, boxWorldPos->y - aabbHalfSize.y),
									  Vector2(boxWorldPos->x + aabbHalfSize.x, boxWorldPos->y + aabbHalfSize.y));

					//Vector2 netVelocity = ballVelocity - Vector2(boxVelocity->x, boxVelocity->y);

					AABB2::Side collisionLocation = 
						Physics::GetInstance().CollisionMovingCircleVsAABB2(ball, 
						ballVelocity, box, 1.0f, collisionTime, collisionPoint);

					if (collisionLocation != AABB2::SideNone && collisionTime < prevCollisionTime)
					{
						if (collisionLocation == AABB2::Inside)
						{
							// TODO: Error, should not come here
							//assert(false);
						}
						else
						{
							if (PlayerInputSystem::sShowDebugText)
								std::cout << "Side: " << collisionLocation << std::endl;

							normal = AABB2::GetSideNormal(collisionLocation);
							prevCollisionTime = collisionTime;
							physics->hasCollided = true;
							boxPhysics->hasCollided = true;
						}
					}		
				}
			}

			// The ball has collided with a box saving the collision normal in the normal variable.
			if (physics->hasCollided)
			{
				physics->collisionNormalx = normal.x;
				physics->collisionNormaly = normal.y;
				physics->collisionTime = collisionTime;
			}
		}
	}
}

/*void CollisionSystem::Update(World& world)
{
	std::vector<unsigned int> entities = this->getEntities( world );
	SweptAABB circleAABB;

	// Set the hasCollided flag to false for all entities.
	for (auto it : entities)
	{
		PhysicsComponent* physics = (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);
		physics->hasCollided = false;
	}

	// Check for collisions.
	for (auto it : entities)
	{
		// Only handle balls.
		if ( ballAspect.Matches( world.mEntityHandler->GetEntityAspect( it ) ) )
		{
			WorldPositionComponent* circleWorldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
			VelocityComponent* circleVelocity		= (VelocityComponent*)world.mEntityHandler->GetComponent(it, VELOCITY_COMPONENT);
			PhysicsComponent* physics				= (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);
			CirclePhysicsComponent* circlePhysics = (CirclePhysicsComponent*)world.mEntityHandler->GetComponent(it, CIRCLE_PHYSICS_COMPONENT);

			circleAABB.topLeft = Vector2(circleWorldPos->x - circlePhysics->radius, circleWorldPos->y - circlePhysics->radius );

			circleAABB.dimensions.x = circlePhysics->radius * 2;
			circleAABB.dimensions.y = circlePhysics->radius * 2;

			circleAABB.velocity.x = circleVelocity->x * world.mDelta;
			circleAABB.velocity.y = circleVelocity->y * world.mDelta;

			SweptAABB broadPhaseBox = Physics::GetInstance().GetSweptBroadPhaseBox(circleAABB);

			float normalx, normaly;
			float collisionTime = 0.0f;
			float prevCollisionTime = 1.0f;

			// For each ball, loop through all boxes and check for collisions.
			for (int i = 0; i < entities.size(); i++)
			{
				if (boxAspect.Matches(world.mEntityHandler->GetEntityAspect( entities[i] ) ))
				{
					WorldPositionComponent* boxWorldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(entities[i], WORLD_POSITION_COMPONENT);
					SweptBoxPhysicsComponent* boxPhysics = (SweptBoxPhysicsComponent*)world.mEntityHandler->GetComponent(entities[i], SWEPT_BOX_PHYSICS_COMPONENT);

					AABB2 box = AABB2(Vector2(boxWorldPos->x - boxPhysics->aabb.dimensions.x / 2,
											  boxWorldPos->y - boxPhysics->aabb.dimensions.y / 2),
									  Vector2(boxWorldPos->x + boxPhysics->aabb.dimensions.x / 2,
											  boxWorldPos->y + boxPhysics->aabb.dimensions.y / 2));

					boxPhysics->aabb.topLeft.x = boxWorldPos->x - boxPhysics->aabb.dimensions.x / 2;
					boxPhysics->aabb.topLeft.y = boxWorldPos->y - boxPhysics->aabb.dimensions.y / 2;
					boxPhysics->aabb.velocity = Vector2(0,0);

					if (Physics::GetInstance().CollisionAABB(broadPhaseBox, boxPhysics->aabb))
					{
						float tempx;
						float tempy;

						collisionTime = Physics::GetInstance().CollisionSweptAABB(circleAABB, boxPhysics->aabb, tempx, tempy); 

						if (collisionTime < prevCollisionTime)
						{
							normalx = tempx;
							normaly = tempy;
							prevCollisionTime = collisionTime;
							physics->hasCollided = true;

							std::cout << "First:" << circleAABB.topLeft.x << " " << circleAABB.topLeft.y << ":" << circleAABB.dimensions.x << " " << circleAABB.dimensions.y;
							std::cout << "Second:" << boxPhysics->aabb.topLeft.x << " " << boxPhysics->aabb.topLeft.y << ":" << boxPhysics->aabb.dimensions.x << " " << boxPhysics->aabb.dimensions.y;
						}
					}
				}
			}

			// The ball has collided with a box saving the collision normal in the normal variable.
			if (physics->hasCollided)
			{
				physics->collisionNormalx = normalx;
				physics->collisionNormaly = normaly;
				physics->collisionTime = collisionTime;
			}
		}
	}
}*/
