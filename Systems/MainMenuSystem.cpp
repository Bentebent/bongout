#include "MainMenuSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <InputDevices.hpp>
#include <iostream>

MainMenuSystem::MainMenuSystem(InputDevices* InputDev)
{
	this->InputDev = InputDev;
	mCurrentEntity = 0;
	mUpKey = false;
	mDownKey = false;
	mEnterKey = false;
    Asp.AddInclusiveAspect( MAIN_MENU_COMPONENT );
	Asp.AddInclusiveAspect( WORLD_POSITION_COMPONENT );

	angry = 0x00;
}

void MainMenuSystem::Update(World &world)
{
	if (world.mCurrentState != GameState::MAIN_MENU)
	{
		mFirstEnter = true;
		return;
	}
	
	BaseMenuSystem::Update(world);

	InputDev->g_kb->capture();

	for(int i = 0; i < 4 ; ++i )
	{
		if(InputDev->g_joys[i] )
		{
			InputDev->g_joys[i]->capture();			
		}
	}

	if (world.mPrevState == GameState::END_GAME_MENU && InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_RETURN))
	{
		return;
	}
	else
	{
		world.mPrevState = GameState::MAIN_MENU;
	}

	world.mCamera.SetPosition( 0, 0, 20 );
	glm::vec3 lookAt( 0, 0, 0.05f);
	world.mCamera.UpdateView( lookAt );

	std::vector<unsigned int> entities = this->getEntities( world );
	

	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_DOWN) && !mDownKey)
	{
		mDownKey = true;
		mCurrentEntity += 1;
	}
	else if (!InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_DOWN) )
		mDownKey = false;

	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_UP) && !mUpKey)
	{
		mUpKey = true;
		mCurrentEntity -= 1;
	}
	else if (!InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_UP) )
		mUpKey = false;

	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_RETURN) && !mEnterKey)
	{
		mEnterKey = true;
	}
	else if (!InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_RETURN) )
		mEnterKey = false;

	//Nothing to see here ;)
	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_A)){angry|=(0x01);if(angry!=0x01){angry=0x00;}}
	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_N)){angry|=(0x02);if(angry!=0x03){angry=0x00;}}
	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_G)){angry|=(0x04);if(angry!=0x07){angry=0x00;}}
	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_R)){angry|=(0x08);if(angry!=0x0F){angry=0x00;}}
	if (InputDev->g_kb->isKeyDown(OIS::KeyCode::KC_Y)){angry|=(0x10);if(angry!=0x1F){angry=0x00;}}

	if (mCurrentEntity < 0)
		mCurrentEntity =  MainMenuEntities::SIZE - 1;

	mCurrentEntity = abs(mCurrentEntity) % MainMenuEntities::SIZE;

    for( auto it : entities )
    {
		MainMenuComponent* mainMenuComponent = (MainMenuComponent*)world.mEntityHandler->GetComponent(it, MAIN_MENU_COMPONENT);
		WorldPositionComponent* worldPos = (WorldPositionComponent*)world.mEntityHandler->GetComponent(it, WORLD_POSITION_COMPONENT);
		MenuComponent* menuComponent = (MenuComponent*)world.mEntityHandler->GetComponent(it, MENU_COMPONENT);
		ScaleComponent* scaleComponent = (ScaleComponent*)world.mEntityHandler->GetComponent(it, SCALE_COMPONENT);

		int componentID = mainMenuComponent->id;
		if(componentID == -1 && angry == 0x1F )
		{
			worldPos->x = menuComponent->defaultPosX + (float)(rand()%1000)/4000.0f;
			worldPos->y = menuComponent->defaultPosY + (float)(rand()%1000)/4000.0f;
		}
		if (mCurrentEntity == componentID)
		{
			worldPos->z = WeightedAverage(worldPos->z, menuComponent->popN, menuComponent->popW);

			float pulse = Pulse(scaleComponent->x, menuComponent->popN, menuComponent->popW, 0.0f, world.mElapsedTime);
			
			scaleComponent->x = pulse;
			scaleComponent->y = pulse;
			scaleComponent->z = pulse;

			switch (mCurrentEntity)
			{
				case MainMenuEntities::START:
					{
						if (mEnterKey)
							world.mCurrentState = GameState::LOAD_GAME;
					}
					break;

				case MainMenuEntities::QUIT:
					{
						if (mEnterKey)
							world.mCurrentState = GameState::EXIT_PROGRAM;

						//worldPos->y = WeightedAverage(worldPos->y, menuComponent->popN, 2.5f);
					}
					break;
			}
		}
		else
		{
			worldPos->z = WeightedAverage(worldPos->z, menuComponent->popN, 0.0f);
			float scale = WeightedAverage(scaleComponent->x, menuComponent->popN, menuComponent->defaultScale);
			scaleComponent->x = scale;
			scaleComponent->y = scale;
			scaleComponent->z = scale;
		}
			
    } 

}
