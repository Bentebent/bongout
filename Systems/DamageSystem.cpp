#include "DamageSystem.hpp"
#include <ComponentFramework/EntityHandler.hpp>
#include <ComponentFramework/World.hpp>
#include <InputDevices.hpp>
#include <ComponentFramework/EntityFactory.hpp>

#include <GraphicsSystem/Materials.hpp>

DamageSystem::DamageSystem()
{
	Asp.AddInclusiveAspect(LIFE_COMPONENT);
	Asp.AddInclusiveAspect(PHYSICS_COMPONENT);
}

void DamageSystem::Update(World& world)
{
	if (world.mCurrentState != GameState::GAMEPLAY)
		return;

	std::vector<unsigned int> entities = this->getEntities( world );

	for (auto it : entities)
	{
		PhysicsComponent* pc = (PhysicsComponent*)world.mEntityHandler->GetComponent(it, PHYSICS_COMPONENT);

		if (pc->hasCollided)
		{
			LifeComponent* lc = (LifeComponent*)world.mEntityHandler->GetComponent(it, LIFE_COMPONENT);
			GraphicsComponent* grc = (GraphicsComponent*)world.mEntityHandler->GetComponent(it, GRAPHICS_COMPONENT);
			
			lc->lives -= 1;

			switch (lc->lives)
			{
				case 1:
					grc->meshMaterial = meshPalette.brickMaterialA;
				break;
			}

		}
	}
}