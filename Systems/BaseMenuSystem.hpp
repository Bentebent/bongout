#ifndef BASE_MENU_SYSTEM_H
#define BASE_MENU_SYSTEM_H

#include <ComponentFramework/System.hpp>

class BaseMenuSystem : public System
{
public:
	BaseMenuSystem();
	virtual void Update(World& world);

	float WeightedAverage(float pos, float N, float W);
	float Pulse(float pos, float N, float W, float offset, float elapsedTime);
protected:
	bool mFirstEnter;
};

#endif