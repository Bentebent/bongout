-- solution contains projects, and defines the available configurations
-- Note that this program is written in C99 this means that it won't compile using vs20xx because
-- it only has a C89 implementation, on windows mingw is recomended.
solution "breakout"
   configurations { "Debug", "Release" }
--   location "build"
 
   -- A project defines one build target
   project "breakout"
      kind "ConsoleApp"
      language "C++"
      files { "../src/**.hpp", "../src/**.cpp", "../src/**.h", "../src/**.c" }

      buildoptions{ "-std=c++0x" }
      flags{ "ExtraWarnings" }

       
      includedirs { "../src" }
    
      if os.is("linux") then
          links { "GLEW", "OIS" }
          defines { "UNIX_TERMINAL_COLOR", "OPENGL" }

          -- Linux specific build flags, requires that glfw is compiled and installed as a static lib.
          buildoptions{ "`pkg-config --cflags glfw3 portaudio-2.0`" }
          linkoptions{ "`pkg-config --static --libs glfw3 portaudio-2.0`" }
          prelinkcommands { "cp -r ../win32/VS12/assets ./" }

      elseif os.is("windows") then
	 -- MinGW build flags, you need to install MinGW with MSYS and then compile glew and glfw
	 -- which you install into the mingw folders /lib /include
	 links { "glew32", "glfw3", "opengl32" }
      end
 
      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols" }
 
      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize" }   
