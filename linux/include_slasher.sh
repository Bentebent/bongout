#!/bin/sh

for f in $(find ../src/ | grep -E ".*([.]cpp$)|([.]hpp$)")
do
sed -i '/^\(#include\).*/s/\\/\//g' $f
done
